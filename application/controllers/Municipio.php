<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Municipio extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
		
		$this->load->database();
        $this->load->model('Municipio_Model', 'municipio');
		
		if(empty($this->session->ativo)){
            redirect('DPP/acesso');
        }
    }

    public function getCoordsLayerEstados()
    {
        $uf = $this->input->post("uf", true);
        $result = $this->municipio->getCoordsLayerEstados($uf);
        echo json_encode($result);
	}
	
    public function getMunicipiosGeo()
    {
        $dados = $this->input->post(NULL, true);
        $retorno = $this->municipio->getMunicipiosGeo($dados);
        echo json_encode($retorno);
    }

    public function getMunicipiosCord()
    {
        $dados = $this->input->post(NULL, true);
        $retorno = $this->municipio->getMunicipiosCord($dados);
        echo json_encode($retorno);
    }
}
