<?php

defined('BASEPATH') or exit('No direct script access allowed');

class GestaoConflitosFaixa extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('GestaoConflitosModel', 'm');
		$this->load->helper('form');
		$this->load->helper('url');
	}

	public function index()
	{
		$dados['pessoal'] = $this->input->get('pessoal', true);

		$dados['pagina'] = 'gestaoConflitos/index';
		$dados['sidebar'] = 'sidemenu';
		$dados['header'] = 'template_cgdr/header';
		$this->load->view('template_cgdr/index_view', $dados);
	}

	function getFaixaDominioOperacional()
	{
		$dados = $this->input->post(NULL, true);
		echo json_encode($this->m->getFaixaDominioOperacional($dados));
	}

	function getFaixaConflitos()
	{
		$dados = $this->input->post(NULL, true);
		echo json_encode($this->m->getFaixaConflitos($dados));
	}

	function getFaixaImoveis()
	{
		$dados = $this->input->post(NULL, true);
		echo json_encode($this->m->getFaixaImoveis($dados));
	}

	function abrirDadosImovel()
	{
		$dados = $this->input->post(NULL, true);
		echo json_encode($this->m->abrirDadosImovel($dados));
	}

	function getQuantitativo()
	{

		$result = $this->m->getQuantitativo();
		$arr = [];
		$total = 0;

		/**
		 * total: 800,
		 * data : [{
		 *		name:'Em Aberto', 
		 *		y: data[0].qtd,
		 *		url: base_url + 'GestaoConflitosFaixa'
		 *	},{
		 *		name:'Em Andamento',
		 *		y: data[1].qtd,
		 *		url: base_url + 'GestaoConflitosEmAndamento'
		 *	},{
		 *		name:'Para Revisao',
		 *		y: 0,
		 *		url: base_url + 'GestaoConflitosParaAprovacao'
		 *	}]
		 */
		foreach ($result as $statusConflito) {

			$total += $statusConflito['qtd'];
			$url = '';

			switch ($statusConflito['status']) {
				case 'Conflito Aberto':
					$url = base_url('GestaoConflitosFaixa');
					break;
				case 'Em Andamento':
					$url = base_url('GestaoConflitosEmAndamento');
					break;
				case 'Em Homologação':
					$url = base_url('GestaoConflitosEmHomologacao');
					break;
			}


			$arr['data'][] = [
				'name' => $statusConflito['status'],
				'y' =>  $statusConflito['qtd'],
				'url' => $url
			];
		}
		$arr['total'] = $total;

		echo json_encode($arr);
	}

	function json_to_kml($json)
	{
		$geom = geoPHP::load($json, 'json');
		return $geom->out('kml');
	}

	function getKmlConflito()
	{
		$post = $this->input->post(NULL, true);
		$dadosConflito = $this->m->getFaixaConflitos($post)[0];

		$dadosFaixa = $this->m->getFaixaDominioOperacional(['CodigoFaixaDominio' => $dadosConflito['CodigoFaixaDominio']])[0];

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
		<kml xmlns="http://earth.google.com/kml/2.1">
		<Document>
		<Style id="shp2kml">
			<IconStyle>
				<color>FFFFCA95</color>
				<scale>1.0</scale>
				<Icon>
				<href>http://www.zonums.com/library/symbols/ge/shp2kml.png</href>
				</Icon>
			</IconStyle>
			<LabelStyle>
				<scale>0</scale>
			</LabelStyle>
			<BalloonStyle>
				<text>
					<![CDATA[         
						<h4>$[name]</h4>         
					<br />    
					$[description]       
					]]>	
				</text>
				<color>FFFFFFFF</color>
			</BalloonStyle>
			<LineStyle>
				<color>6c213177</color>
				<width>1</width>
			</LineStyle>
			<PolyStyle>
				<color>6c002dff</color>
			</PolyStyle>
		</Style>
			<Placemark id="shp2kml">
	  <name>Área do Conflito</name>
	  <styleUrl>#shp2kml</styleUrl>
	  <description>
		  <b>Código</b>: ' . $dadosConflito['CodigoConflito'] .
			'<br /><b>UF:</b> ' . $dadosFaixa['UF'] .
			'<br /><b>BR:</b> ' . $dadosFaixa['BR'] .
			'<br /><b>Km inicial:</b> ' . $dadosConflito['KmInicialConflito'] .
			'<br /><b>Km Final:</b> ' . $dadosConflito['KmFinalConflito'] .
			'</description>
      <styleUrl>#shp2kml</styleUrl>';

		$xml .= $this->json_to_kml($dadosConflito['Coordenada']);

		$xml .= '</Placemark>
		</Document>
		</kml>';

		ob_end_clean();
		header_remove();

		header('Content-disposition: attachment; filename=advertise.xml');
		header("Content-Type:text/xml");
		//output the XML data
		echo  $xml;
		// if you want to directly download then set expires time
		exit();
	}

	function getKmlFaixa()
	{
		$post = $this->input->post(NULL, true);
		$dadosFaixa = $this->m->getFaixaDominioOperacional($post)[0];

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
		<kml xmlns="http://earth.google.com/kml/2.1">
		<Document>
		<Style id="shp2kml">
			<IconStyle>
				<color>FFFFCA95</color>
				<scale>1.0</scale>
				<Icon>
				<href>http://www.zonums.com/library/symbols/ge/shp2kml.png</href>
				</Icon>
			</IconStyle>
			<LabelStyle>
				<scale>0</scale>
			</LabelStyle>
			<BalloonStyle>
				<text>
					<![CDATA[         
						<h4>$[name]</h4>         
					<br />    
					$[description]       
					]]>	
				</text>
				<color>FFFFFFFF</color>
			</BalloonStyle>
			<LineStyle>
				<color>ff000046</color>
				<width>1</width>
			</LineStyle>
			<PolyStyle>
				<color>6cff004f</color>
			</PolyStyle>
		</Style>
			<Placemark id="shp2kml">
	  <name>Área da Faixa de Domínio</name>
	  <styleUrl>#shp2kml</styleUrl>
	  <description>
	  <b>Código:</b> ' . $dadosFaixa['CodigoFaixaDominio'] .
			'<br /><b>UF:</b> ' . $dadosFaixa['FaixaDominioUF'] .
			'<br /><b>BR:</b> ' . $dadosFaixa['FaixaDominioBR'] .
			'<br /><b>Km inicial:</b> ' . $dadosFaixa['KmInicial'] .
			'<br /><b>Km Final:</b> ' . $dadosFaixa['KmFinal'] .
			'</description>
      <styleUrl>#shp2kml</styleUrl>';

		$xml .= $this->json_to_kml($dadosFaixa['Coordenada']);

		$xml .= '</Placemark>
		</Document>
		</kml>';

		ob_end_clean();
		header_remove();

		header('Content-disposition: attachment; filename=advertise.xml');
		header("Content-Type:text/xml");
		//output the XML data
		echo  $xml;
		// if you want to directly download then set expires time
		exit();
	}

	function getKmlImovel()
	{
		$post = $this->input->post(NULL, true);
		$dadosImovel = $this->m->getFaixaImoveis($post)[0];

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
		<kml xmlns="http://earth.google.com/kml/2.1">
		<Document>
		<Style id="shp2kml">
			<IconStyle>
				<color>FFFFCA95</color>
				<scale>1.0</scale>
				<Icon>
				<href>http://www.zonums.com/library/symbols/ge/shp2kml.png</href>
				</Icon>
			</IconStyle>
			<LabelStyle>
				<scale>0</scale>
			</LabelStyle>
			<BalloonStyle>
				<text>
					<![CDATA[         
						<h4>$[name]</h4>         
					<br />    
					$[description]       
					]]>	
				</text>
				<color>FFFFFFFF</color>
			</BalloonStyle>
			<LineStyle>
				<color>FFFF0000</color>
				<width>1</width>
			</LineStyle>
			<PolyStyle>
				<color>FFFFCA95</color>
			</PolyStyle>
		</Style>
			<Placemark id="shp2kml">
	  <name>Área do Imóvel</name>
	  <styleUrl>#shp2kml</styleUrl>
	  <description>
	  <b>Código:</b> ' . $dadosImovel['Codigo'] .
			'<br /><b>UF:</b> ' . $dadosImovel['Estado'] .
			'<br /><b>Município:</b> ' . $dadosImovel['NomeMunicpio'] .
			'<br /><b>Tipo:</b> ' . $dadosImovel['TipoImovel'] .
			'</description>
      <styleUrl>#shp2kml</styleUrl>';

		$xml .= $this->json_to_kml($dadosImovel['Coordenada']);

		$xml .= '</Placemark>
		</Document>
		</kml>';

		ob_end_clean();
		header_remove();

		header('Content-disposition: attachment; filename=advertise.xml');
		header("Content-Type:text/xml");
		//output the XML data
		echo  $xml;
		// if you want to directly download then set expires time
		exit();
	}

	function getKmlSegmento()
	{
		$post = $this->input->post(NULL, true);
		$dadosSegmento = $this->m->getFaixasAcaoTramitada($post)[0];

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
		<kml xmlns="http://earth.google.com/kml/2.1">
		<Document>
		<Style id="shp2kml">
			<IconStyle>
				<color>FFFFCA95</color>
				<scale>1.0</scale>
				<Icon>
				<href>http://www.zonums.com/library/symbols/ge/shp2kml.png</href>
				</Icon>
			</IconStyle>
			<LabelStyle>
				<scale>0</scale>
			</LabelStyle>
			<BalloonStyle>
				<text>
					<![CDATA[         
						<h4>$[name]</h4>         
					<br />    
					$[description]       
					]]>	
				</text>
				<color>FFFFFFFF</color>
			</BalloonStyle>
			<LineStyle>
				<color>FFFF0000</color>
				<width>1</width>
			</LineStyle>
			<PolyStyle>
				<color>FFFFCA95</color>
			</PolyStyle>
		</Style>
			<Placemark id="shp2kml">
	  <name>Área do Segmento</name>';

		$xml .= $this->json_to_kml($dadosSegmento['CoordenadaTrechoPublicacao']);

		$xml .= '</Placemark>
		</Document>
		</kml>';

		ob_end_clean();
		header_remove();

		header('Content-disposition: attachment; filename=advertise.xml');
		header("Content-Type:text/xml");
		//output the XML data
		echo  $xml;
		// if you want to directly download then set expires time
		exit();
	}

	public function getJsonFromKml()
	{
		$post = $this->input->post(NULL, true);
		echo json_encode($this->kml_to_json($post['Arquivo']));
		die;
	}

	public function kml_to_json($file)
	{
		$kml = file_get_contents($file);
		$geom = geoPHP::load($kml, 'kml');
		return $geom->out('json');
	}

	public function getTramitadosPara()
	{
		$dados = $this->input->post(NULL, true);
		echo json_encode($this->m->getTramitadosPara($dados['email']));
	}

	/**
	 * Atualiza as coordenadas do conflito, imovel ou faixa 
	 * de acordo com a ultima acao tramitada do conflito
	 *
	 * @return void
	 */
	public function atualizaCoordenadas()
	{
		$dados = $this->input->post(NULL, true);

		$ultimaAcao = $this->m->getUltimaAcao($dados['CodigoConflito'])[0];

		if ($ultimaAcao['ArquivoFaixa'] != null) {
			$jsonFaixa = $this->kml_to_json($ultimaAcao['ArquivoFaixa']);
			$codFaixa = $ultimaAcao['CodigoFaixa'];
			// echo $codFaixa; die;
			echo $this->m->atualizaCoordFaixa($jsonFaixa, $codFaixa);
			die;
		}


		if ($ultimaAcao['ArquivoImovel'] != null) {
			$jsonImovel = $this->kml_to_json($ultimaAcao['ArquivoImovel']);
			$codImovel = $ultimaAcao['CodigoImovel'];
			// echo $codImovel; die;
			echo $this->m->atualizaCoordImovel($jsonImovel, $codImovel);
			die;
		}


		if ($ultimaAcao['ArquivoConflito'] != null) {
			$jsonConflito = $this->kml_to_json($ultimaAcao['ArquivoConflito']);
			$codConflito = $ultimaAcao['CodigoConflito'];
			// echo $codConflito; die;
			echo $this->m->atualizaCoordConflito($jsonConflito, $codConflito);
			die;
		}
	}

	public function enviaPublicacaoAprovacao()
	{
		$dados = $this->input->post(NULL, true);
		$uf = $dados['Publicacao']['UF'];
		$br =  $dados['Publicacao']['BR'];

		$post['TramitadoPor'] = $dados['TramitadoPor'];
		$post['Data'] = date('Y-m-d H:m:s');
		$post['CodigoFaixa'] = $dados['Publicacao']['CodigoFaixaDominio'];
		$post['CoordenadaTrechoPublicacao'] = $dados['Publicacao']['poly'];
		$post['Status'] = $dados['Status'];
		$ul1 = sizeof($post['CoordenadaTrechoPublicacao']['coordinates']) - 1;
	
		$cliente = new GuzzleHttp\Client(['headers' => ['Access-Control-Allow-Origin' => '*']]);

		$paramsP1 = http_build_query([
			'br' => $br,
			'uf' => $uf,
			'longitude' => $post['CoordenadaTrechoPublicacao']['coordinates'][0][0],
			'latitude' => $post['CoordenadaTrechoPublicacao']['coordinates'][0][1],
			'tolerancia' => 250,
			'data' => (date('Y') - 1) . '-12-31'
		]);

		$paramsP2 = http_build_query([
			'br' => $br,
			'uf' => $uf,
			'longitude' => $post['CoordenadaTrechoPublicacao']['coordinates'][$ul1][0],
			'latitude' => $post['CoordenadaTrechoPublicacao']['coordinates'][$ul1][1],
			'tolerancia' => 250,
			'data' => (date('Y') - 1) . '-12-31'
		]);
		try {

			$response = $cliente->request('GET', 'http://cgplanhml.dnit.gov.br:83/webservicesnv/service/snv/RetornaKm?' . $paramsP1);
			$response2 = $cliente->request('GET', 'http://cgplanhml.dnit.gov.br:83/webservicesnv/service/snv/RetornaKm?' . $paramsP2);

			$km1 = json_decode($response->getBody()->getContents(), true);
			$km2 = json_decode($response2->getBody()->getContents(), true);

			$post['KmInicialTrechoPublicacao'] = $km1['kmReferencia'];
			$post['KmFinalTrechoPublicacao'] = $km2['kmReferencia'];

			// $post['CoordenadaTrechoPublicacao'] = json_encode($post['CoordenadaTrechoPublicacao']);

			 echo json_encode($this->m->insertAcaoFaixa($post));
			 die;

		} catch (GuzzleHttp\Exception\ClientExeption $e) {
			echo $e->getMessage();
		}
	}

	public function aprovaPublicacao(){
		$dados = $this->input->post(NULL, true);

		echo json_encode($this->m->setAndamentoPublicacaoFaixa($dados));
		die;
	}

	public function getListaFaixasPublicacao(){
		$dados = $this->input->post(NULL, true);
		echo json_encode($this->m->getListaFaixasPublicacao($dados));
	}

	public function enviaPublicacaoFaixaHomolog(){

		$params['data'] = (array) json_decode($this->input->post('Dados', false));
		
		$this->uploadFiles($params);

		echo json_encode($this->m->setAndamentoPublicacaoFaixa($params['data']));
		die;

	}

}
