<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
      
        $this->load->helper('form');
        $this->load->helper('url');
    }
        
    public function index()
    {
		
        if (ENVIRONMENT == "production") {
            if (empty($this->session->ativoem)) {
                redirect("Home/acesso");
            } 
           
        } else {
            $_SESSION["id_token"] = "asdfasdf";
            $_SESSION["nome"] = "Desenvolvedor";
			$_SESSION["email"] =  "marcos.araujo@dnit.gov.br";
			
            // $_SESSION["nome"] = "Keyth";
            // $_SESSION["email"] =  "keyth.roy.rodrigues@accenture.com";
            $_SESSION["ativo"] = true;
           
		}

		$dados['pagina'] = 'index';
        $dados['sidebar'] = 'sidemenu';
        $dados['header'] = 'template_cgdr/header';
     	$this->load->view('template_cgdr/index_view', $dados);
    
    }

    
    public function acesso()
    {
        $this->useraccess->checkTimeSession();
    }


}
