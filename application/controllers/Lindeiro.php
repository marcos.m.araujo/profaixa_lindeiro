<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Lindeiro extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
      
        $this->load->helper('form');
        $this->load->helper('url');
    }
        
    public function index()
    {
		$dados['pagina'] = 'lindeiro/index';
        // $dados['sidebar'] = 'lindeiro/sidemenu';
        $dados['header'] = 'template_cgdr/lindeiro/header';
     	$this->load->view('template_cgdr/lindeiro/index_view', $dados);
    
    }
	
    public function cadastro()
    {
		$dados['pagina'] = 'lindeiro/cadastro';
        $dados['header'] = 'template_cgdr/lindeiro/header';
     	$this->load->view('template_cgdr/lindeiro/index_view', $dados);
    
    }

    public function consulta()
    {
		$dados['pagina'] = 'lindeiro/consulta';
        $dados['header'] = 'template_cgdr/lindeiro/header';
     	$this->load->view('template_cgdr/lindeiro/index_view', $dados);
    
    }
}
