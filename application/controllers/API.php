<?php

defined('BASEPATH') or exit('No direct script access allowed');
/**
 * REST API
 * 
 * REpresetation State Transfer
 *
 * Padrao de arquitetura cliente-servidor 
 * para sistemas distribuídos de hipermedia
 * 
 * Baseado nos Verbos HTTP: GET, PUT, POST, DELETE
 * URIs: nome da funcionalidade
 * resposta HTTP: status, dados 
 * 
 * Funcionalidade: UF
 * Servico: informacões da UF
 * Representação: Nome, Sigla, 
 * 
 * @api DPP/API
 * @param HTTP
 * @return JSON
 * 
 * @author Roy Fielding, em sua tese de doutorado
 * @copyright 2000 © Roy Thomas Fielding, 2000. All rights reserved
 * @source https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm
 */
class API extends MY_Controller
{
    /**
     * Construtor
     * 
     * @uses CORE::database 
     * @uses API_Model
     */
    function __construct()
    { 
        parent::__construct();
        $this->load->database();
        $this->load->model('API_Model');
    }

    
}
