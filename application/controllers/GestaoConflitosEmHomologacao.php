<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . 'controllers/GestaoConflitosFaixa.php';
class GestaoConflitosEmHomologacao extends GestaoConflitosFaixa
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('GestaoConflitosModel', 'm');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    public function index()
    {
		$dados['pagina'] = 'GestaoConflitosEmHomologacao/index';
        $dados['sidebar'] = 'sidemenu';
        $dados['header'] = 'template_cgdr/header';
        $this->load->view('template_cgdr/index_view', $dados);
    }
}
