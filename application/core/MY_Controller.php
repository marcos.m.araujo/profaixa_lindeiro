<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  MY_Controller
* 
* Author:  Ben Edmunds
* Created:  7.21.2009 
* 
* Description:  Class to extend the CodeIgniter Controller Class.  All controllers should extend this class.
* 
*/
class MY_Controller extends CI_Controller {

	public $API_Model;

    /**
     * Funcao global get
     * 
     * 
     * @param POST
     * @example HTTP/POST params{
     *   table: 'UF',
     *   column: 'Sigla',
     *   where: {Id: 4}
     * }
     * 
     * @return JSON
     * @example {
     *    status: true,
     *    response: {0:{Sigla:'BA'}}
     * }
     * @uses API_Model get()
     */
    public function get()
    {       
        echo json_encode(
            $this->API_Model->get(
                $this->input->post(NULL, true)
            )
        );
    }

    /**
     * Funcao global insert
     * @api DPP/API/insert
     * 
     * 
     * @param POST
     * @example HTTP/POST params{
     *   table: 'UF',
     *   data: {
     *      Nome: 'Maranhão',
     *      Sigla: 'MA'
     *   }
     * }
     * 
     * @return JSON
     * @example {
     *    status: true,
     *    response: 'dados inseridos com suceeso'
     * }
     * @uses API_Model insert()
     */
    public function insert(){

		$params = (array) json_decode($this->input->post('Dados', true));
        $params['data'] = (array) $params['data'];
      
        if(sizeof($_FILES) > 0){
			$this->uploadFiles($params);
        }
    
        echo json_encode($this->API_Model->insert($params));
        die;
    }

    
    /**
     * Funcao global update
     * @api APP/API/update
     * 
     * POST Request
     * @example HTTP/POST params{
     *   table: 'UF',
     *   data: {
     *      Nome: 'Maranhão',
     *      Sigla: 'MA'
     *   }
     * }
     * 
     * @return JSON
     * @example {
     *    status: true,
     *    response: 'dados atualizados com suceeso'
     * }
     * @uses API_Model update()
     */
    public function update(){
        $params = (array) json_decode($this->input->post('Dados', true));
        $params['data'] = (array) $params['data'];
        $params['where'] = (array) $params['where'];

        if(sizeof($_FILES) > 0){
			$this->uploadFiles($params);
		}
		
        echo json_encode($this->API_Model->update($params));
        die;

    }


    /**
     * Funcao global uploadFile
     * 
     * @param Array  by reference
     * 
     * @return void
     */
    public function uploadFiles(&$params){
        

		if (!isset($params['data']['FilesRules'])) {
			echo json_encode([
				'status' => false,
				'result' => 'As regras de upload não foram definidas.'
			]);
			die;
		}

		$rules = $params['data']['FilesRules'];
		unset($params['data']['FilesRules']);

        foreach($_FILES as $index => $value){

            if (isset($_FILES[$index]['name'])) {

                $NomeArquivo = $_FILES[$index]['name'];
                $ext = @end(explode(".", $NomeArquivo));

                // verifica se o diretorio existe, se não ele cria
                if (!is_dir($rules->upload_path)){
                    mkdir($rules->upload_path, 0755, true);
                }
                $config['upload_path'] = $rules->upload_path;
                $config['allowed_types'] = $rules->allowed_types;

				if (!function_exists('random_bytes')) {
					$novoNome = $this->random_bytes(10);
				} else {

					$novoNome = random_bytes(10);
				}
                $novoNomeRandom = bin2hex($novoNome) . "." . $ext;

                $pathFile = base_url($rules->upload_path) . '/' . $novoNomeRandom;

                $config['file_name'] = $novoNomeRandom;

                $this->upload->initialize($config);
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload($index)) {
                    echo json_encode([
                        'status' => false,
                        'result' => $index . '->' . $this->upload->display_errors()
                    ]);
                    die;
                } else {
                    $params['data'][$index] = $pathFile;
                }
            }   
        }
    }

    public function deleteFile(){
        $path = $this->input->post('path', true);
        $path = str_replace(base_url(), "", $path);
        
        if( file_exists($path) ){ 
            // Remove file 
            unlink($path); 
      
            // Set status 
            echo json_encode(true); 
         }else{ 
            // Set status 
            echo json_encode(false); 
         } 
         die;

    }

	/**
	 * funcao inexistente em PHP < 7, implementando por causa da versao do servidor 5.6
	 *
	 * @param [int] $bytes
	 * @return String
	 */
	function random_bytes($bytes)
    {
        if (!is_int($bytes) || $bytes < 1) {
             throw new InvalidArgumentException('random_bytes() expects a positive integer');
        }
        $buf = '';
        // See PHP bug #55169 for why 5.3.7 is required
        if (
            function_exists('mcrypt_create_iv') &&
            version_compare(PHP_VERSION, '5.3.7') >= 0
        ) {
            $buf = mcrypt_create_iv($bytes, MCRYPT_DEV_URANDOM);
            if ($buf !== false) {
                return $buf;
            }
        }
        
        /**
         * Use /dev/urandom for random numbers
         * 
         * @ref http://sockpuppet.org/blog/2014/02/25/safely-generate-random-numbers
         */
        if (@is_readable('/dev/urandom')) {
            $fp = fopen('/dev/urandom', 'rb');
            if ($fp !== false) {
                $streamset = stream_set_read_buffer($fp, 0);
                if ($streamset === 0) {
                    $remaining = $bytes;
                    do {
                        $read = fread($fp, $remaining); 
                        if ($read === false) {
                            // We cannot safely read from urandom.
                            $buf = false;
                            break;
                        }
                        // Decrease the number of bytes returned from remaining
                        $remaining -= strlen($read);
                        $buf .= $read;
                    } while ($remaining > 0);
                    if ($buf !== false) {
                        return $buf;
                    }
                }
            }
        }
        /**
         * Windows with PHP < 5.3.0 will not have the function
         * openssl_random_pseudo_bytes() available, so let's use
         * CAPICOM to work around this deficiency.
         */
         if (class_exists('COM', false)) {
             try {
                 if ($buf === false) {
                     $buf = ''; // Make it a string, not false
                 }
                $util = new COM('CAPICOM.Utilities.1');
                $execs = 0;
                /**
                 * Let's not let it loop forever. If we run N times and fail to
                 * get N bytes of random data, then CAPICOM has failed us.
                 */
                do {
                    $buf .= base64_decode($util->GetRandom($bytes, 0));
                    if (strlen($buf) >= $bytes) { 
                        return substr($buf, 0, $bytes);
                    }
                    ++$execs;
                } while ($execs < $bytes);
            } catch (Exception $e) {
                unset($e); // Let's not let CAPICOM errors kill our app
            }
        }
        if (function_exists('openssl_random_pseudo_bytes')) {
            $secure = true;
            $buf = openssl_random_pseudo_bytes($bytes, $secure);
            if ($buf !== false && $secure) {
                return $buf;
            }
        }
        /**
         * We have reached the point of no return. Throw an exception.
         */
        throw new Exception('Better for PRNG failures to terminate than propagate');
    }

	public function delete()
    {
       echo json_encode($this->API_Model->delete($this->input->post(NULL, true)));
        die;
    }
}
