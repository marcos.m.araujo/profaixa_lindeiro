<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Controleacesso
{
    protected $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('dpp/ControleAcesso_Model', 'admin');
    }

    public function permissao($area)
    {
        return $this->CI->admin->getPerfil($area);
    }
}
