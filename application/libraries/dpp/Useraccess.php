<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Useraccess
{
    private $tokenUri = 'http://servicos.dnit.gov.br/identidade/connect/token';
    private $authorizeUri = 'http://servicos.dnit.gov.br/identidade/connect/authorize';
    private $redirectUri = 'http://servicos.dnit.gov.br/DPP';
    private $redirectEndPoint = 'http://servicos.dnit.gov.br/identidade/connect/endsession';
    private $clientId = 'atlas_prd';
    private $responseType = 'code id_token';
    private $scope ='openid email name';

    /**
     * Verifica se tem a sessao e o token
     */
    public function  makeAccess(){
        if (session_status() != 2) {//verifica se a sessao esta ativa
            session_start();
        }

        if(!isset($_SESSION['user_token'])){
            $url = $this->authorizeUri.'?'.http_build_query([
                'client_id' => $this->clientId,
                'redirect_uri' => $this->redirectUri,            
                'response_type' => $this->responseType,
                'scope' => $this->scope,
                'nonce' => hash('sha256',$this->randomString())." HTTP/1.1"
            ]);            
            header('Location: '.$url);           
        }
    }

    //veririca se o usuario esta ativo em
    public function checkTimeSession(){
        $this->makeAccess();
        $temEsgotado = (time() - $_SESSION["ativoem"]) > 7200;
        if(isset($_SESSION["ativoem"]) && $temEsgotado){
            $this->logout();
        }else{
            $_SESSION["ativoem"] = time();
        }
    }

    //tempo do cookie 3 horas
    //parametro opcional é o code
    public function setUserTokenInSession($id_token, $code = ''){
        if($code !== ''){
            $_SESSION['access_token'] = $code;
        }

        $_SESSION['user_token'] = $id_token;
        $_SESSION['ativoem'] = time();
    }

    public function logout(){
        session_destroy();

        $content = http_build_query([
            'post_logout_redirect_uri' => $this->redirectUri,
            'id_token_hint' => $_SESSION['id_token']
        ]);

        $url = $this->redirectEndPoint.'?'.$content;

        header('Location: '.$url);
    }

    // Não utilizado ate o momento 
    ///precisa do autorization code
    //coloocado na session
    public function getAccessCode(){
        $authorization = base64_encode($this->clientId);
        $header = array("Authorization: Basic {$authorization}","Content-Type: application/x-www-form-urlencoded");
        $_SESSION['teste'] = "marcos";
        $content = http_build_query([
            'client_id' => $this->clientId,
            'grant_type'=>'authorization_code',
            'code' => $_SESSION['access_token'],
            'redirect_uri' => $this->redirectUri,
        ]);
    
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->tokenUri,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $content
        ));
        $response = curl_exec($curl);
        curl_close($curl);
    
        if ($response === false) {
            echo "Failed";
            echo curl_error($curl);
            echo "Failed";
        } elseif (array_key_exists('error',json_decode($response,TRUE))) {
            echo "Error:<br />";
            echo json_decode($response,TRUE)['error'];
        }else{
            $access_token = json_decode($response,TRUE)['access_token'];
            $id_token = json_decode($response,TRUE)['id_token'];

            $_SESSION['access_token'] = $access_token;
            $_SESSION['id_token'] = $id_token;
        }
    }

    //gera uma string aleatoria
    private function randomString($bits = 256){
        $bytes = ceil($bits / 8);
        $return = '';
        for ($i = 0; $i < $bytes; $i++) {
            $return .= chr(mt_rand(0, 255));
        }
        return $return;
    }
    
}