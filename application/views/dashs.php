<div class="col-lg-3 col-6">
    <div class="small-box bg-warning">
        <div class="inner">
            <h3>2.415,26 <sup style="font-size: 20px">km</sup></h3>
            <p>Extensão de faixa de domínio definida</p>
        </div>
        <div class="icon">
            <i class="ion ion-person-add"></i>
        </div>
        <a href="#" class="small-box-footer">Mais info <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>

<div class="col-lg-3 col-6">
    <div class="small-box bg-info">
        <div class="inner">
		<h3>2.415,26 <sup style="font-size: 20px">km</sup></h3>
            <p>Extensão de faixa de domínio publicada</p>
        </div>
        <div class="icon">
            <i class="ion ion-bag"></i>
        </div>
        <a href="#" class="small-box-footer">Mais info <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>

<div class="col-lg-3 col-6">
    <div class="small-box bg-danger">
        <div class="inner">
            <h3>2.962,3 <sup style="font-size: 20px">km</sup></h3>
            <p>Extensão de conflitos</p>
        </div>
        <div class="icon">
            <i class="ion ion-pie-graph"></i>
        </div>
        <a href="#" class="small-box-footer">Mais info <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>

<div class="col-lg-3 col-6">
    <div class="small-box bg-success">
        <div class="inner">
            <h3>1.152</h3>
            <p>Imóveis cadastrados</p>
        </div>
        <div class="icon">
            <i class="ion ion-stats-bars"></i>
        </div>
        <a href="#" class="small-box-footer">Mais info <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>

<div class="col-md-3">
    <p class="text-center">
        <!-- <strong>Goal Completion</strong> -->
    </p>
    <div class="progress-group">
        DF
        <span class="float-right"><b>160</b>/200 km</span>
        <div class="progress progress-sm">
            <div class="progress-bar bg-warning" style="width: 80%"></div>
        </div>
    </div>
    <div class="progress-group">
        GO
        <span class="float-right"><b>310</b>/400 km</span>
        <div class="progress progress-sm">
            <div class="progress-bar bg-warning" style="width: 75%"></div>
        </div>
    </div>
    <div class="progress-group">
        <span class="progress-text">MT</span>
        <span class="float-right"><b>480</b>/800 km</span>
        <div class="progress progress-sm">
            <div class="progress-bar bg-warning" style="width: 60%"></div>
        </div>
    </div>
    <div class="progress-group">
        MS
        <span class="float-right"><b>250</b>/500 km</span>
        <div class="progress progress-sm">
            <div class="progress-bar bg-warning" style="width: 50%"></div>
        </div>
    </div>
</div>
<div class="col-md-3">
    <p class="text-center">
        <!-- <strong>Goal Completion</strong> -->
    </p>
    <div class="progress-group">
        DF
        <span class="float-right"><b>160</b>/200 km</span>
        <div class="progress progress-sm">
            <div class="progress-bar bg-info" style="width: 80%"></div>
        </div>
    </div>
    <div class="progress-group">
        GO
        <span class="float-right"><b>310</b>/400 km</span>
        <div class="progress progress-sm">
            <div class="progress-bar bg-info" style="width: 75%"></div>
        </div>
    </div>
    <div class="progress-group">
        <span class="progress-text">MT</span>
        <span class="float-right"><b>480</b>/800 km</span>
        <div class="progress progress-sm">
            <div class="progress-bar bg-info" style="width: 60%"></div>
        </div>
    </div>
    <div class="progress-group">
        MS
        <span class="float-right"><b>250</b>/500 km</span>
        <div class="progress progress-sm">
            <div class="progress-bar bg-info" style="width: 50%"></div>
        </div>
    </div>
</div>
<div class="col-md-3">
    <p class="text-center">
        <!-- <strong>Goal Completion</strong> -->
    </p>
    <div class="progress-group">
        DF
        <span class="float-right"><b>160</b>/200 km</span>
        <div class="progress progress-sm">
            <div class="progress-bar bg-danger" style="width: 80%"></div>
        </div>
    </div>
    <div class="progress-group">
        GO
        <span class="float-right"><b>310</b>/400 km</span>
        <div class="progress progress-sm">
            <div class="progress-bar bg-danger" style="width: 75%"></div>
        </div>
    </div>
    <div class="progress-group">
        <span class="progress-text">MT</span>
        <span class="float-right"><b>480</b>/800 km</span>
        <div class="progress progress-sm">
            <div class="progress-bar bg-danger" style="width: 60%"></div>
        </div>
    </div>
    <div class="progress-group">
        MS
        <span class="float-right"><b>250</b>/500 km</span>
        <div class="progress progress-sm">
            <div class="progress-bar bg-danger" style="width: 50%"></div>
        </div>
    </div>
</div>
