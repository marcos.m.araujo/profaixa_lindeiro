
<script src="<?= base_url('template_lte3/') ?>plugins/leaflet/leaflet.js"></script>
<script src="<?= base_url('template_lte3/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') ?>"></script>
<script src="<?= base_url('theme/leaflet/Leaflet.PolylineMeasure.js') ?>"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.css"/>

<script>
    var leafletVue = new Vue({
        data() {
            return {
				mymap: null,
				
				layerEstados: {},

				layerMunicipios: {},
				municipiosSelecionados: null,
				layerSNV: {},
				
				layerAlterarCor: '',
				botaoAlteraCor: '',
			}
		},
        methods: {
           
            // Gerador de mapa
            gerarMapa(idMapa, controlPolyMeasure = false) {

                this.mymap = L.map(idMapa).setView(['-10.6007767', '-63.6037797'], 4.5);
                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 18,
                    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
                    id: 'mapbox.streets'
				}).addTo(this.mymap);
				
                L.control.scale({
                    maxWidth: 240,
                    metric: true,
                    imperial: false,
                    position: 'bottomleft'
				}).addTo(this.mymap);
				
                if (controlPolyMeasure) {
                    this.controlPolyMeasure()
				}
				
				this.layerMunicipios = L.geoJSON().addTo(this.mymap);
				this.layerSNV = L.geoJSON().addTo(this.mymap);


				return this.mymap;
			},
			
            // Botões de cálculos de metros no mapa
            controlPolyMeasure() {
				
                L.control.polylineMeasure({
                    position: 'topleft',
                    unit: 'metres',
                    showBearings: true,
                    clearMeasurementsOnStop: true,
                    showClearControl: true,
                    showUnitControl: true
				}).addTo(this.mymap);
				
			},			
			
			// layer com SNVs
			// var params = {uf: this.params.uf, br: this.params.br};
			async plotLayerSNV(params, showLayerSNV, color) {
				this.layerSNV.clearLayers()

				if(showLayerSNV){

					var controller = 'MalhaRodoviaria/getMalhaRodovia';
					var data = await vmGlobal.getFromController(controller, params);
					
					for (var i = 0; i < data.length; i++) {
						var cord = data[i];
						if (cord.DescricaoSuperficie == 'PLA') {
							this.setMapRodovia(cord.Coordenada, cord.Concatenado, color, this.layerSNV, '3,20', 'round')
						} else {
							this.setMap(cord.Coordenada, cord.Concatenado, color, this.layerSNV)
						}
						
					}
				}
				return this.layerSNV;
			},

            // Adicionar layer line geoJoson
            addLayer(coord, objectid, color, layer) {
                var array = {
                    "type": "FeatureCollection",
                    "features": [{
                        "type": "Feature",
                        "geometry": JSON.parse(coord),
                        "properties": {
                            "OBJECTID": objectid,
                        }
                    }]
                }
                L.geoJson(array, {
                    style: function(feature) {
                        return {
                            stroke: true,
                            color: color,
                            weight: 5
                        };
                    },
                    onEachFeature: function(feature, l) {
                        l.bindPopup(feature.properties.OBJECTID);
                        l.on('click', function(e) {
                            l.setStyle({
                                weight: 12,
                                outline: 'red'
                            });
                        });

                        l.on("popupclose", function(e) {
                            l.setStyle({
                                weight: 5,
                            });
                        });
                    }
                }).addTo(layer)
			},

            // busca municipio pelo nome, monta lista com nomes parecidos            
            async getMunicipio() {

                var hsp = $("#inputMunic").val();
                if (hsp.length > 2) {
                    var controller = 'Municipio/getMunicipiosGeo';
                    var params = {municipio: hsp};
                    var data = await vmGlobal.getFromController(controller, params);

                    $('#inptmun').empty();
                    for (var i = 0; i < data.length; i++) {
                        var hosp = data[i];
                        $("#inptmun").append('<option value="' + hosp.MunicipioEstado + '">' + hosp.MunicipioEstado +"</option>");
                    }
                }

            },
            // adiciona marcador em cima do municipio selecionado
            async getMunicipioSelect() {
                var m = $("#inputMunic").val();
                if (m != "") {
                    
                    var controller = 'Municipio/getMunicipiosCord';
                    var params = {municipio: m};
                    var data = await vmGlobal.getFromController(controller, params);

                    this.addLayer(data[0].Coordenada, m, '',this.layerMunicipios);
					$(".btnClearBuscarM").css('display', 'block')
					
					return this.layerMunicipios;
                }
            },
            // retira do mapa municipios selecionados
            clearMunicipioSelect(input, clearClass) {
                this.layerMunicipios.clearLayers()
                $("#inputMunic").val('')
                $(".btnClearBuscarM").css('display', 'none')
            },
            //volta o zoom para visao nacional
            zoomHome() {
                this.mymap.setView(['-10.6007767', '-63.6037797'], 4.6);
            },            
            // altera a visualizacao do mapa para estilo satelite
			changeViewSatelite(showImagemSatelite) {
				if (!showImagemSatelite) {
					L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
						maxZoom: 18,
						attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
							'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
						id: 'mapbox.streets'
					}).addTo(this.mymap);

				} else {
					L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
						maxZoom: 20,
						subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
					}).addTo(this.mymap);
				}
			},                    
            
            setMapRodovia(coord, objectid, color, layer, dahsArray, lineJoin) {
                var array = {
                    "type": "FeatureCollection",
                    "features": [{
                        "type": "Feature",
                        "geometry": JSON.parse(coord),
                        "properties": {
                            "OBJECTID": objectid
                        }
                    }]
                }
                L.geoJson(array, {
                    style: function (feature) {
                        return {
                            stroke: true,
                            color: color,
                            weight: 3,
                            dashArray: dahsArray,
                            lineJoin: lineJoin

                        };
                    },
                    onEachFeature: function (feature, l) {
                        l.bindPopup(feature.properties.OBJECTID);
                    }
                }).addTo(layer)
            },            
            setMap(coord, objectid, color, layer) {
                this.addLayer(coord, objectid, color, layer)
			},
			setMapCircle(coord, objectid, color, weight, layer) {
				var array = {
					"type": "FeatureCollection",
					"features": [{
						"type": "Feature",
						"geometry": JSON.parse(coord),
						"properties": {
							"OBJECTID": objectid
						}
					}]
				}
				L.geoJson(array, {
					style: function (feature) {
						return {
							stroke: true,
							color: color,
							opacity: 1,
							fillColor: color,
							weight: weight,
							fillOpacity: 1
						};
					},
					onEachFeature: function (feature, l) {
						l.bindPopup(feature.properties.OBJECTID);

					}
				}).addTo(layer)
			},
			//abrir modal de cores define layer q ira trocar a cor
			setLayerTrocaCor(layerSelecionada, botao, corInicial) {
				
				this.layerAlterarCor = layerSelecionada;
				this.botaoAlteraCor = botao;

				setTimeout(() => {
					$("#corLinhaSelecionada").focus();
					$("#corLinhaSelecionada").val(corInicial);
				}, 1000);
				
			},
			//trocar cor linhas
			trocaCor() {
				this.layerAlterarCor.setStyle({
					color: $("#corLinhaSelecionada").val()
				});

				$("#" + this.botaoAlteraCor).css('border', '3px solid ' + $("#corLinhaSelecionada").val());
				
			}, 

        }
    })
</script>
