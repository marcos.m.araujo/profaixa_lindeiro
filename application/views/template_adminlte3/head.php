
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<title>DPP</title>
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSSs -->
<link rel="shortcut icon" href="<?= base_url() ?>/js/dpp/menu/img/favicon.ico">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!--Bootstrap-->
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/plugins/bootstrap/css/bootstrap.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/plugins/fontawesome-free/css/all.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/plugins/datatables/jquery.dataTables.css">
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/plugins/datatables/dataTables.bootstrap4.css">
<link rel="stylesheet" href="<?php print base_url('theme/') ?>datatable/buttons.dataTables.min.css">
<!-- overlayScrollbars -->
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/dist/css/adminlte.min.css">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/plugins/fontawesome-free/css/all.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/plugins/datatables/dataTables.bootstrap4.css">
<link rel="stylesheet" href="<?php print base_url('theme/') ?>datatable/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
<!-- overlayScrollbars -->
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/dist/css/adminlte.min.css">
<!-- CSS Pai para alterações do lteadmin-->
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/dist/css/comum.css">
<!--<link data-prerender="keep" rel="stylesheet" href="--><?php //print base_url('new_theme/fontawesome/css/all.css') ?><!--">-->
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('new_theme/fontawesome/css/brands.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('new_theme/fontawesome/css/regular.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('new_theme/fontawesome/css/solid.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('new_theme/fontawesome/css/svg-with-js.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('new_theme/fontawesome/css/v4-shims.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('new_theme/fontawesome/css/v4-shims.min.css') ?>">


<!-- COLOR PIKER-->
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">

