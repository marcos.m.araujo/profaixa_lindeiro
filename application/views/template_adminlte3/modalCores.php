<!-- Modal -->
<div class="modal fade" id="modalCores" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Selecione uma Cor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body h-250">
                <div class="row" id="paleta-cores">
                    <div class="input-group my-colorpicker2 colorpicker-element" data-colorpicker-id="2">
                        
                    <input type="text" class="form-control" data-original-title="" id="corLinhaSelecionada">

                        <div class="input-group-append">
                            <span class="input-group-text" id="btn-setar-cor"><i class="fas fa-check"></i></span>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>