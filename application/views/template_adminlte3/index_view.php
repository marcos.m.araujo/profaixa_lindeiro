<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php $this->load->view('template_adminlte3/head') ?>
</head>
<style>
  @import url('https://fonts.googleapis.com/css?family=Livvic&display=swap');
  h1,
  h2,
  h3,
  span,
  label,
  hr,
  td,
  a,
  table {
    font-family: 'Livvic', sans-serif;
  }
  .content-wrapper {
    background-color: #E9F1F4
  }
</style>
<body id="body" class="sidebar-collapse">
	<?php $this->load->view('template_adminlte3/script') ?>
	<?php $this->load->view('template_adminlte3/script_interno') ?>
  <div class="wrapper">
		<nav style="background-color: #F4F6F9;" class="main-header navbar navbar-expand border-bottom navbar-dark" id="menuPrincipalAtlas">
				<?php $this->load->view('template_adminlte3/header') ?>
			</nav>
			<aside class="main-sidebar elevation-4 sidebar-light-primary">
				<a href="<?= base_url('DPP') ?>" class="brand-link bg-dark">
					<!-- <img src="<?= base_url('theme/img_dafault/balaoLogo.png') ?>" style="box-shadow: none" class="brand-image img-circle elevation-3">-->
					<span class="brand-text font-weight-light"><img src="<?= base_url() ?>/js/dpp/menu/img/logo-atlas.png" align="center" width="55%" height="auto"></span>
				</a>
				<?php $this->load->view($sidebar) ?>
			</aside>
				<div class="content-wrapper">
					
					<?php $this->load->view($pagina) ?>
				</div>
				<footer class="main-footer">
					<?php $this->load->view('template_adminlte3/footer') ?>
				</footer>
			</div>
		</body>
		
		</html>
<script>
  $('.content-wrapper').click(function() {
    var classe = $('#body').attr("class");
    if (classe == 'sidebar-open') {
      $('#body').removeClass("sidebar-open");
      $('#body').addClass("sidebar-collapse");
    }
	});
	

	 // =================== Início funcionalidade trocar cor

     /* pegar controller
* $this->router->fetch_class()
* Pegar Action
* $this->router->fetch_method();
*/

var controllerMetodo = '<?= $this->uri->uri_string()?>';
if(controllerMetodo === 'ProjecaoOrcamentaria/index' ||controllerMetodo === 'AtlasDPP/index'){

        //color picker with addon
        $('.my-colorpicker2').colorpicker();
        var layerGlobalAlterarCor = '';
        var botaoGlobalAlteraCor = '';


        //abrir modal de cores
        function abrirModalCores(layerSelecionada, botao, corInicial, controller) {
            botaoGlobalAlteraCor = botao;
            if (controller === 'Carteira') {
                if (layerSelecionada === 'estudo')
                    layerGlobalAlterarCor = layer1;
                if (layerSelecionada === 'projetos')
                    layerGlobalAlterarCor = layer2;
                if (layerSelecionada === 'desapropriacoes')
                    layerGlobalAlterarCor = layer3;
                if (layerSelecionada === 'getao-ambiental')
                    layerGlobalAlterarCor = layer4;
                if (layerSelecionada === 'licenca-ambiental')
                    layerGlobalAlterarCor = layer5;
                if (layerSelecionada === 'obra-direta')
                    layerGlobalAlterarCor = layer7;
                if (layerSelecionada === 'obra-delegada')
                    layerGlobalAlterarCor = layer8;
                if (layerSelecionada === 'concessao-federal')
                    layerGlobalAlterarCor = layer9;
                if (layerSelecionada === 'estudos-concessao')
                    layerGlobalAlterarCor = layerEstudoConcessao;
            } else {

                if (layerSelecionada === 'estudo')
                    layerGlobalAlterarCor = layer;
                if (layerSelecionada === 'projetos')
                    layerGlobalAlterarCor = layer1;
                if (layerSelecionada === 'concessao-federal')
                    layerGlobalAlterarCor = layer2;
                if (layerSelecionada === 'obra-direta')
                    layerGlobalAlterarCor = layer3;
                if (layerSelecionada === 'obra-delegada')
                    layerGlobalAlterarCor = layer4;
                if (layerSelecionada === 'estudos-concessao')
                    layerGlobalAlterarCor = layer6;
            }
            //abrir modal
            $("#modalCores").modal();
            setTimeout(() => {
                $("#corLinhaSelecionada").focus();
                $("#corLinhaSelecionada").val(corInicial);
            }, 300);


        }

        //trocar cor linhas
        function trocarCor(cor) {
            layerGlobalAlterarCor.setStyle({
                color: cor
            });

            $("#" + botaoGlobalAlteraCor).css('border', '3px solid ' + cor);
        }

        //Alterar cor de linhas

        $("#btn-setar-cor").click(function() {
            var cor = $("#corLinhaSelecionada").val();
            trocarCor(cor);
            $("#modalCores").modal('hide');
        });

        $("#" + botaoGlobalAlteraCor).css('border', '3px solid ' + cor);
    }

</script>
