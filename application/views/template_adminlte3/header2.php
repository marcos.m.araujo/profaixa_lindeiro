<style>
	.bars {
		font-size: 24px;
		color: #17a2b8;
	}

	.bg-light {
		background-color: rgba(248, 249, 250, 0) !important;
	}

	/*menu mobile*/
	.dropdown-submenu {
		position: relative;
	}

	.dropdown-submenu > .dropdown-menu {
		top: 0;
		right: 100%;
		margin-top: -6px;
		margin-left: -1px;
		-webkit-border-radius: 0 6px 6px 6px;
		-moz-border-radius: 0 6px 6px;
		border-radius: 0 6px 6px 6px;
	}

	.dropdown-submenu:hover > .dropdown-menu {
		display: block;
	}

	.dropdown-submenu > a:after {
		display: block;
		content: " ";
		float: right;
		width: 0;
		height: 0;
		border-color: transparent;
		border-style: solid;
		border-width: 5px 0 5px 5px;
		border-left-color: #ccc;
		margin-top: 5px;
		margin-right: -10px;
	}

	.dropdown-submenu:hover > a:after {
		border-left-color: #fff;
	}

	.dropdown-submenu.pull-right {
		float: none;
	}

	.dropdown-submenu.pull-right > .dropdown-menu {
		right: -100%;
		margin-right: 10px;
		-webkit-border-radius: 6px 0 6px 6px;
		-moz-border-radius: 6px 0 6px 6px;
		border-radius: 6px 0 6px 6px;
	}

	/*menu mobile*/
	.icon-color {
		color: #17a2b8;
	}

	a {
		color: black !important;
	}

	@media (min-width: 350px) and (max-width: 992px) {
		.mobile-hide {
			display: none;
		}

		/*    menu mobile*/
		.dropdown-submenu {
			position: relative;
		}

		.dropdown-submenu > .dropdown-menu {
			top: 0;
			left: 100%;
			margin-top: -6px;
			margin-left: -1px;
			-webkit-border-radius: 0 6px 6px 6px;
			-moz-border-radius: 0 6px 6px;
			border-radius: 0 6px 6px 6px;
		}

		i
		.dropdown-submenu:hover > .dropdown-menu {
			display: block;
		}

		.dropdown-submenu > a:after {
			display: block;
			content: " ";
			float: right;
			width: 0;
			height: 0;
			border-color: transparent;
			border-style: solid;
			border-width: 5px 0 5px 5px;
			border-left-color: #ccc;
			margin-top: 5px;
			margin-right: -10px;
		}

		.dropdown-submenu:hover > a:after {
			border-left-color: #fff;
		}

		.dropdown-submenu.pull-left {
			float: none;
		}

		.dropdown-submenu.pull-left > .dropdown-menu {
			left: -100%;
			margin-left: 10px;
			-webkit-border-radius: 6px 0 6px 6px;
			-moz-border-radius: 6px 0 6px 6px;
			border-radius: 6px 0 6px 6px;
		}

		/*menu mobile*/
		.dropdown-menu li:hover .dropdown-menu {
			position: absolute;
			width: 188px;
		}
	}

	@media (min-width: 992px) and (max-width: 2900px) {
		.menu-mobile {
			display: none;
		}

		/*    menu mobile*/
		.dropdown-submenu {
			position: relative !important;
		}

		.dropdown-submenu > .dropdown-menu {
			top: 0;
			left: 100%;
			margin-top: -6px;
			margin-left: -1px;
			-webkit-border-radius: 0 6px 6px 6px;
			-moz-border-radius: 0 6px 6px;
			border-radius: 0 6px 6px 6px;
		}

		.dropdown-submenu:hover > .dropdown-menu {
			display: block;
		}

		.dropdown-submenu > a:after {
			display: block;
			content: " ";
			float: right;
			width: 0;
			height: 0;
			border-color: transparent;
			border-style: solid;
			border-width: 5px 0 5px 5px;
			border-left-color: #ccc;
			margin-top: 5px;
			margin-right: -10px;
		}

		.dropdown-submenu:hover > a:after {
			border-left-color: #fff;
		}

		.dropdown-submenu.pull-left {
			float: none;
		}

		.dropdown-submenu.pull-left > .dropdown-menu {
			left: -100%;
			margin-left: 10px;
			-webkit-border-radius: 6px 0 6px 6px;
			-moz-border-radius: 6px 0 6px 6px;
			border-radius: 6px 0 6px 6px;
		}

		/*menu mobile*/
		.dropdown-menu li:hover .dropdown-menu {
			position: absolute;
		}
	}

	/**/

</style>
<ul id="menu" class="navbar-nav mobile-hide">
	<li class="nav-item">
		<a style="color: silver" class="nav-link" data-widget="pushmenu" href="#"><i style="color: #17a2b8"
																																								 class="fas fa-filter"></i></a>
	</li>
	<li class="nav-item" style="margin-left: 10px">
		<a title="Voltar para página inicial" style="color: black" class="nav-link" href="<?= base_url('DPP') ?>"><i
				class="fas fa-home icon-color"></i> Home</a>
	</li>
	<li class="nav-item dropdown show">
		<a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
			 class="nav-link dropdown-toggle"><i class="fas fa-chart-area icon-color"></i> Módulos</a>
		<ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow ">
			<li><a href="<?= base_url('ProjecaoOrcamentaria') ?>" class="dropdown-item">Planejamento</a></li>
			<li><a href="<?= base_url('ControleAceite') ?>" class="dropdown-item">Termos de Aceite</a></li>
			<li><a href="<?= base_url('AtlasDPP') ?>" class="dropdown-item">Carteira</a></li>
			<li><a target="_black" href="http://servicos.dnit.gov.br/sgplan/home" class="dropdown-item">Estudos</a></li>
			<li><a target="_black" href="http://servicos.dnit.gov.br/sgplanpainel/Projeto" class="dropdown-item">Projetos</a>
			</li>
			<li><a href="<?= base_url("BibliotecaDPP") ?>" class="dropdown-item">Biblioteca</a></li>
			<?php if (
				$this->session->email === 'welther.melo@dnit.gov.br' ||
				$this->session->email === 'marcos.araujo@dnit.gov.br' ||
				$this->session->email === 'allan.ribeiro@dnit.gov.br' ||
				$this->session->email === 'luiz.mello@dnit.gov.br' ||
				$this->session->email === 'thiago.rosa@dnit.gov.br' ||
				$this->session->email === 'carlos.kawamura@accenture.com' ||
				$this->session->email === 'thiago.duraes@dnit.gov.br' ||
				$this->session->email === 'bruno.vendramini@dnit.gov.br' ||
				$this->session->email === 'leonardo.perim@dnit.gov.br'
			) { ?>
				<li><a target="_black" href="http://10.100.13.116/radar" class="dropdown-item">Radar</a></li>
			<?php } ?>
			<li class="dropdown-divider"></li>
			<li><a href="<?= base_url('DPP') ?>" class="dropdown-item"><strong>Home</strong></a></li>
		</ul>
	</li>
	<li v-if="adminiValido" class="nav-item dropdown show">
		<a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
			 class="nav-link dropdown-toggle"> <i class="fas fa-cog"></i> Adminstrativo</a>
		<ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow ">
			<li v-for="i in table"><a :href="i.Link" class="dropdown-item"> {{i.Area}}</a></li>
		</ul>
	</li>
	<li v-if="sistemaValido" class="nav-item dropdown show">
		<a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
			 class="nav-link dropdown-toggle"> <i class="fas fa-cog icon-color"></i> Sistema</a>
		<ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow ">
			<li v-for="i in sistema"><a :href="i.Link" class="dropdown-item"> {{i.Area}}
					<strong class="badge-warning" style="border-radius: 25px; padding:2px"
									v-if="i.Area == 'Fale conosco'"> {{totalDemandaAberta}}</strong>
				</a>
			</li>
		</ul>
	</li>
</ul>

<nav class="navbar navbar-expand-lg navbar-light bg-light menu-mobile">

	<div class="collapse navbar-collapse " id="navbarNavDropdown">
		<ul class="navbar-nav">

			<li class="nav-item dropdown">
				<a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
					<i class="fas fa-bars bars"></i>
				</a>
				<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					<a title="Voltar para página inicial"
						 href="<?= base_url('DPP') ?>"
						 class="dropdown-item"><i
							class="fa fa-home icon-color"></i> Home</a>
					<div class="dropdown-divider"></div>
					<li class="dropdown-submenu">
						<a href="#" data-toggle="dropdown" aria-haspopup="true"
							 aria-expanded="true" class="dropdown-item dropdown-toggle"><i
								class="fas fa-chart-area icon-color"></i> Módulos</a>
						<div class="dropdown-divider"></div>
						<ul class="dropdown-menu">
							<li><a href="<?= base_url('ProjecaoOrcamentaria') ?>" class="dropdown-item">Planejamento</a></li>
							<li><a href="<?= base_url('ControleAceite') ?>" class="dropdown-item">Termos de Aceite</a></li>
							<li><a href="<?= base_url('AtlasDPP') ?>" class="dropdown-item">Carteira</a></li>
							<li><a target="_black" href="http://servicos.dnit.gov.br/sgplan/home"
										 class="dropdown-item">Estudos</a></li>
							<li><a target="_black" href="http://servicos.dnit.gov.br/sgplanpainel/Projeto"
										 class="dropdown-item">Projetos</a></li>
							<li><a href="<?= base_url("BibliotecaDPP") ?>" class="dropdown-item">Biblioteca</a></li>
							<?php if (
								$this->session->email === 'welther.melo@dnit.gov.br' ||
								$this->session->email === 'marcos.araujo@dnit.gov.br' ||
								$this->session->email === 'allan.ribeiro@dnit.gov.br' ||
								$this->session->email === 'luiz.mello@dnit.gov.br' ||
								$this->session->email === 'thiago.rosa@dnit.gov.br' ||
								$this->session->email === 'carlos.kawamura@accenture.com' ||
								$this->session->email === 'thiago.duraes@dnit.gov.br' ||
								$this->session->email === 'bruno.vendramini@dnit.gov.br' ||
								$this->session->email === 'leonardo.perim@dnit.gov.br'
							) { ?>
								<li><a target="_black" href="http://10.100.13.116/radar" class="dropdown-item">Radar</a>
								</li>
							<?php } ?>
							<li class="dropdown-divider"></li>
							<li><a href="<?= base_url('DPP') ?>" class="dropdown-item"><strong>Home</strong></a></li>

						</ul>
					</li>
					<li class="dropdown-submenu">
						<a href="#" data-toggle="dropdown" aria-haspopup="true"
							 aria-expanded="true" class="dropdown-item dropdown-toggle"><i
								class="fas fa-cog icon-color"></i>
							Sistema</a>
						<ul class="dropdown-menu">
							<li><a href="AcessosDPP" class="dropdown-item"> Acessos DPP
								</a></li>
							<li><a href="<?= base_url('FaleConoscoDPP')?>" class="dropdown-item"> Fale conosco
									<strong class="badge-warning" style="border-radius: 25px; padding: 2px;">
										1</strong></a>
							</li>
							<li><a href="<?= base_url('GerenciamentoAdmin');?>" class="dropdown-item"> Gestão Administrativa
								</a></li>


						</ul>
					</li>

				</ul>
			</li>
		</ul>
	</div>
</nav>


<ul id="msg" class="navbar-nav ml-auto ">
	<li class="nav-item dropdown show">
		<a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
			 class="nav-link dropdown-toggle"><?= $this->session->nome ?></a>
		<ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow ">
			<li><a href="<?= base_url('DPP/logout') ?>" class="dropdown-item"><strong>Sair</strong></a></li>
		</ul>
	</li>

	<li class="nav-item dropdown">
		<a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="true">
			<i class="far fa-bell"></i>
			<span class="badge badge-warning navbar-badge">{{msg.length}}</span>
		</a>
		<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
			<span class="dropdown-item dropdown-header">Notificações</span>
			<div class="dropdown-divider"></div>
			<a v-for="i in msg" href="<?= base_url('Notificacao') ?>" class="dropdown-item">
				<i class="fas fa-bell"></i> {{i.TipoDemanda}} <br>De:
				<strong>{{i.EmailOrigem}} </strong><br>
				<span>{{i.DataF}}</span>
			</a>

			<div class="dropdown-divider"></div>
			<a href="<?= base_url('Notificacao') ?>" class="dropdown-item dropdown-footer">Ver Todas as Notificações</a>
			<div class="dropdown-divider"></div>

			<div data-toggle="modal" data-target="#myModal"
					 style="text-align: center; background-color: #E9F1F4; cursor: pointer; padding:5px; font-weight: bold">
				Criar novo fale conosco <img width="15%" src="<?= base_url('img_dpp/fale_conosco.png') ?>">
			</div>
		</div>
	</li>
	<li>
	<a href="javascritp:;" onclick="toggleFullScreen()" class="nav-link"><i class="fas fa-expand"></i></a>
	</li>


</ul>


<script>
	var base_url = '<?=base_url()?>';
    new Vue({
        el: "#msg",
        data() {
            return {
                msg: [],
                balde: [],
            }
        },
        mounted() {
            this.get()
            setInterval(() => {
                this.get()
            }, 60000);
        },
        methods: {
            get() {
                axios.get('<?= base_url("FaleConoscoDPP/getFaleConoscoTramiteAlerta") ?>')
                    .then((result) => {
                        this.msg = result.data
                    })
                    .catch((e) => {
                        console.log(e)
                    })
            },

        }
    })
    vmHeader = new Vue({
        el: "#menu",
        data() {
            return {
                msg: [],
                balde: [],
                table: [],
                sistema: [],
                sistemaValido: false,
                adminiValido: false,
                totalDemandaAberta: 0
            }
        },
        mounted() {
            this.getAreaAdmin()
            this.getFaleConosco()
            this.getAreaSistema()
            setInterval(() => {
                this.getFaleConosco()
            }, 60000);
        },
        methods: {
            getFaleConosco() {
                axios.get('<?= base_url("FaleConoscoDPP/getFaleConosco") ?>')
                    .then((result) => {
                        this.balde = result.data
                    })
                    .catch((e) => {
                        console.log(e)
                    })
                    .finally(() => {
                        this.totalDemandaAberta = 0;
                        for (var i = 0; i < this.balde.length; i++) {
                            if (this.balde[i].Situacao == 'Demanda Aberta') {
                                this.totalDemandaAberta++
                            }
                        }
                    })
            },
            getAreaAdmin() {
                axios.get('<?= base_url("GerenciamentoAdmin/getAreasMenuAdmin") ?>')
                    .then((result) => {
                        this.table = result.data
                    })
                    .catch((e) => {
                        console.log(e)
                    })
                    .finally(() => {
                        if (this.table.length > 0)
                            this.adminiValido = true
                    })
            },
            getAreaSistema() {
                axios.get('<?= base_url("GerenciamentoAdmin/getAreasMenuSistema") ?>')
                    .then((result) => {
						console.log(result);
						var retorno = result.data;
						
						$.each(retorno, function(i, v){
							retorno[i].Link = window.base_url + retorno[i].Link; 
						});

						this.sistema = retorno;
                    })
                    .catch((e) => {
                        console.log(e)
                    })
                    .finally(() => {
                        if (this.sistema.length > 0)
                            this.sistemaValido = true
                    })
            },
        }
    })
</script>
<script>
    $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
        }
        var $subMenu = $(this).next('.dropdown-menu');
        $subMenu.toggleClass('show');


        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
            $('.dropdown-submenu .show').removeClass('show');
        });


        return false;
    });
</script>
