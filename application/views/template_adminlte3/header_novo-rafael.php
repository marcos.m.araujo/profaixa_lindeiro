<style>
    /*    a {
            color: black !important;
        }*/
</style>
<ul id="menu" class="navbar-nav text-center">
    <li class="nav-item">
        <a style="color: black" class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
    </li>




</ul>


<ul class="navbar-nav ml-auto">
    <!-- Messages Dropdown Menu -->
    <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
            <i class="far fa-user"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="#" class="dropdown-item">
                <!-- Message Start -->
                <div class="media">
                    <img src="<?= base_url('template_lte3/dist/img/user1-128x128.jpg') ?>" alt="Avatar" class="img-size-50 mr-3 img-circle">
                    <div class="media-body">
                        <h3 class="dropdown-item-title">
                            <?= $this->session->nome ?>
                            <span class="float-right text-sm text-danger"><i class="fas fa-edit"></i></span>
                        </h3>

                    </div>
                </div>
                <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>


            <a href="#" class="dropdown-item dropdown-footer">Sair</a>
        </div>
    </li>
    <!-- Notifications Dropdown Menu -->
     <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fa fa-bell"></i>
            <span class="badge badge-warning navbar-badge">0</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">Notificações</span>
            <div class="dropdown-divider"></div>
            <a style="border-bottom: 1px solid silver" v-for="i in msg" href="<?= base_url('Notificacao') ?>" class="dropdown-item">
                <i style="color: green" class="fas fa-bell"></i> {{i.TipoDemanda}} <br>De: <strong>{{i.EmailOrigem}} </strong><br>
                <span class=" ">{{i.DataF}}</span>
            </a>
            <div style="text-align: center; padding:10px">
                <a href="<?= base_url('Notificacao') ?>">Ver todas</a>

            </div>
            <div  data-toggle="modal" data-target="#exampleModal" style="text-align: center; background-color: #E9F1F4; cursor: pointer; padding:5px; font-weight: bold">      
                Criar novo fale conosco <img width="15%" src="<?= base_url('img_dpp/fale_conosco.png') ?>">
            </div>

        </div>
    </li>
    
</ul>

<!--
<ul id="msg" class="navbar-nav ml-auto">
    <li class="nav-item dropdown show">
        <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="nav-link dropdown-toggle">
            <i class="fa fa-user"></i> </a>


        <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow ">
            <li class=<?= $this->session->nome ?>"dropdown-item">
                <?= $this->session->nome ?>
            </li>
            <li><a href="<?= base_url('DPP/logout') ?>" class="dropdown-item"><strong>Sair</strong></a></li>
        </ul>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
            <i style="color: black" class="fa fa-bell-o"></i>
            <span class="badge badge-warning navbar-badge">{{msg.length}}</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">Notificações</span>
            <div class="dropdown-divider"></div>
            <a style="border-bottom: 1px solid silver" v-for="i in msg" href="<?= base_url('Notificacao') ?>" class="dropdown-item">
                <i style="color: green" class="fas fa-bell"></i> {{i.TipoDemanda}} <br>De: <strong>{{i.EmailOrigem}} </strong><br>
                <span class=" ">{{i.DataF}}</span>
            </a>
            <div style="text-align: center; padding:10px">
                <a href="<?= base_url('Notificacao') ?>">Ver todas</a>

            </div>
            <div  data-toggle="modal" data-target="#exampleModal" style="text-align: center; background-color: #E9F1F4; cursor: pointer; padding:5px; font-weight: bold">      
                Criar novo fale conosco <img width="15%" src="<?= base_url('img_dpp/fale_conosco.png') ?>">
            </div>

        </div>
    </li>
</ul>
-->
<script>
    new Vue({
        el: "#msg",
        data() {
            return {
                msg: [],
                balde: [],
            }
        },
        mounted() {
            this.get()
            setInterval(() => {
                this.get()
            }, 60000);
        },
        methods: {
            get() {
                axios.get('<?= base_url("FaleConoscoDPP/getFaleConoscoTramiteAlerta") ?>')
                        .then((result) => {
                            this.msg = result.data
                        })
                        .catch((e) => {
                            console.log(e)
                        })
            },

        }
    })
    new Vue({
        el: "#menu",
        data() {
            return {
                msg: [],
                balde: [],
                table: [],
                sistema: [],
                sistemaValido: false,
                adminiValido: false,
                totalDemandaAberta: 0
            }
        },
        mounted() {
            this.getAreaAdmin()
            this.getFaleConosco()
            this.getAreaSistema()
            setInterval(() => {
                this.getFaleConosco()
            }, 60000);
        },
        methods: {
            getFaleConosco() {
                axios.get('<?= base_url("FaleConoscoDPP/getFaleConosco") ?>')
                        .then((result) => {
                            this.balde = result.data
                        })
                        .catch((e) => {
                            console.log(e)
                        })
                        .finally(() => {
                            this.totalDemandaAberta = 0;
                            for (var i = 0; i < this.balde.length; i++) {
                                if (this.balde[i].Situacao == 'Demanda Aberta') {
                                    this.totalDemandaAberta++
                                }
                            }
                        })
            },
            getAreaAdmin() {
                axios.get('<?= base_url("GerenciamentoAdmin/getAreasMenuAdmin") ?>')
                        .then((result) => {
                            this.table = result.data
                        })
                        .catch((e) => {
                            console.log(e)
                        })
                        .finally(() => {
                            if (this.table.length > 0)
                                this.adminiValido = true
                        })
            },
            getAreaSistema() {
                axios.get('<?= base_url("GerenciamentoAdmin/getAreasMenuSistema") ?>')
                        .then((result) => {
                            this.sistema = result.data
                        })
                        .catch((e) => {
                            console.log(e)
                        })
                        .finally(() => {
                            if (this.sistema.length > 0)
                                this.sistemaValido = true
                        })
            },
        }
    })
</script>