<script>

    const vmGlobal = new Vue({
		methods: {
            async getFromController(controller, params){
                var output = '';
                await axios.post('<?=base_url();?>'+controller, $.param(params))
                .then((response) => {
                    output = response.data;
                })
                .catch((e) => {
					console.log(e)
					this.showMessage('error', 'Erro!', "Erro ao consultar dados. "+e);
                });
                return output;
            },
            async getFromAPI(params, projeto = null){
				
				var output = '';
				
                await axios.post(base_url + '/API/get', $.param(params))
                .then((response) => {
                    if(response.data.status == false)
                        throw new Error(response.data.result);

                    output = response.data.result;
                })
                .catch((e) => {
                    console.log(e)
					this.showMessage('error', 'Erro!', "Erro ao consultar dados. "+e);
                });
                return output;
            },
            async updateFromAPI(params, file = null, projeto = null, msg = true){
                
                var formData = new FormData();
                formData.append('Dados', JSON.stringify(params));

                if(file !== null){
					if(file instanceof Array){
						for(i=0; i<file.length; i++){
							formData.append(file[i].name, file[i].value);
						};
					} else {
						formData.append(file.name, file.value);
					}
				}
				
                var url = base_url + '/API/update';

				var result;

                await axios.post(url, formData)
                .then((response) => {

                    if(response.data.status == false)
                        throw new Error(response.data.result);

					
					if(msg === true)
						msg = {
							tipo: 'sucess',
							titulo: 'Salvo!', 
							texto: 'Alteração executada com sucesso.'
						};

					if(msg != false)
						this.showMessage(msg.tipo, msg.titulo, msg.texto);
                    
                    result = true;
                })
                .catch((e) => {
                    console.log(e)
					this.showMessage('error', 'Erro!', "Erro ao atualizar dados. "+e);

					result = false;
				});
				return result;
            },
            async insertFromAPI(params, file = null, projeto = null, msg = true){
				
				var url = base_url + '/API/insert';
                
                var formData = new FormData();
                formData.append('Dados', JSON.stringify(params));

                if(file !== null){
					if(file instanceof Array){
						for(i=0; i<file.length; i++){
							formData.append(file[i].name, file[i].value);
						};
					} else {
						formData.append(file.name, file.value);
					}
				}
                await axios.post(url, formData)
                .then((response) => {

                    if(response.data.status == false) {
                        throw new Error(response.data.result);
					}
						
					if(msg === true)
						msg = {
							tipo: 'success',
							titulo: 'Salvo!', 
							texto: 'Dados inseridos com sucesso.'
						};

					if(msg !== false)
						this.showMessage(msg.tipo, msg.titulo, msg.texto);
					
                    result = true;
                })
                .catch((e) => {
                    console.log(e)
					this.showMessage('error', 'Erro!', "Erro ao tentar inserir dados. "+e);					
					result = false;
				});
				
				return result;
                
            },
			async deleteFromAPI(params, projeto = null, msg = true) {

				var url = base_url + '/API/delete';

				await axios.post(url, $.param(params))
				.then((response) => {
					if (response.data.status == false)
						throw new Error(response.data.result);

					if(msg === true)
						msg = {
							tipo: 'sucess',
							titulo: 'Salvo!', 
							texto: 'Dados deletados com sucesso.'
						};

					if(msg != false)
						this.showMessage(msg.tipo, msg.titulo, msg.texto);
					
					return true;
				})
				.catch((e) => {
					console.log(e)
					this.showMessage('error', 'Erro!', "Erro ao tentar deletar os dados. "+e);					
				});
			},
            async deleteFile(path){
				
				var params = {
                    path: path
				};
				
                await axios.post(base_url + 'API/deleteFile',  $.param(params))
                .then((response) => {

                    if(response == false) 
                        throw new Error("Arquivo não encontrado");

                    return true;
                })
                .catch((e) => {
					console.log(e)
					this.showMessage('error', 'Erro!', "Erro ao tentar deletar arquivo. "+e);					
                    return false;
                });
            },
            montaDatatable(id, filters = null, sums = null) {
                try {
                    this.$nextTick(function () {
                        $(id).DataTable({
                            "destroy": true,
                            "scrollX": true,
                            dom: 'Bfrtip',
							"orderCellsTop": true,
        					"fixedHeader": true,
                            buttons:  [
								{ 
									extend: 'excel', 
									text: '<i class="fas fa-file-excel"></i> Excel', 
									className: 'btn btn-sm ml-2 btn-success',
									// title: 'Acidentes',
									attr:{
										id: 'btnExportExcel'
									},
									// customize: function( xlsx ) {
									// 	var source = xlsx.xl['workbook.xml'].getElementsByTagName('sheet')[0];
									// 	source.setAttribute('name','Acidentes');
									// }
								}
							],
                            "bSort": false,
                            "bSortable": true,
                            "autoWidth": false,
                            "oLanguage": {
                                "sEmptyTable": "Nenhum registro encontrado na tabela",
                                "sInfo": "Mostrar _START_ até _END_ do _TOTAL_ registros",
                                "sInfoEmpty": "Mostrar 0 até 0 de 0 Registros",
                                "sInfoFiltered": "(Filtrar de _MAX_ total registros)",
                                "sInfoPostFix": "",
                                "sInfoThousands": ".",
                                "sLengthMenu": "Mostrando _MENU_ registros por pagina",
                                "sLoadingRecords": "Carregando...",
                                "sProcessing": "Processando...",
                                "sZeroRecords": "Nenhum registro encontrado",
                                "sSearch": "Busca: ",
                                "oPaginate": {
                                    "sNext": "<i class=\"fas fa-chevron-right\" ></i>",
                                    "sPrevious": "<i class=\"fas fa-chevron-left\" ></i>",
                                    "sFirst": "Primeiro",
                                    "sLast": "Ultimo"
                                },
                                "oAria": {
                                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                                    "sSortDescending": ": Ordenar colunas de forma descendente"
                                }
                            },
							"initComplete": function () {
								let cont = 0;
								this.api().columns().every( function () {
									if(filters != null && filters.includes(cont)){
										var column = this;
										// console.log(this);
										
										var select = $('<br><select><option value=""></option></select>')
											.appendTo( $(column.header() ) )
											.on( 'change', function () {
												var val = $.fn.dataTable.util.escapeRegex(
													$(this).val()
												);
						
												column
													.search( val ? '^'+val+'$' : '', true, false )
													.draw();
											} );
						
										column.data().unique().sort().each( function ( d, j ) {
											select.append( '<option value="'+d+'">'+d+'</option>' )
										} );
									}
									cont++;
								} );
							}, 
							"footerCallback": function ( row, data, start, end, display ) {
								if(sums != null) {

									var api = this.api(), data;

									// Remove the formatting to get integer data for summation
									var intVal = function ( i ) {
										return typeof i === 'string' ?
											i.replace(/[\$,]/g, '')*1 :
											typeof i === 'number' ?
												i : 0;
									};

									$( api.column( 0 ).footer() ).html('Total');

									$.each(sums, (index, val) => {
									
										var sumCol4Filtered = display.map(el => data[el][val]).reduce((a, b) => intVal(a) + intVal(b), 0 );

										$( api.column(val).footer() ).html(sumCol4Filtered);

									});

								}
							}
                        });
                    });
                } catch (err) {
                    console.error(err);
                }
            },
			detroyDataTable(id) {
                $(id).DataTable().destroy();
            },
            //função global para formatar data eng -> pt
            frontEndDateFormat: function(date, tipo = null) {
                if (tipo === 1) {
                    var dataFormatada = moment(date, 'YYYY-MM-DD').format('MMM/YYYY').split("/");
                    if(dataFormatada[0] === 'Feb'){
                        return 'Fev'+'/'+dataFormatada[1];
                    }
                    if(dataFormatada[0] === 'Apr'){
                        return 'Abr'+'/'+dataFormatada[1];
                    }
                    if(dataFormatada[0] === 'May'){
                        return 'Mai'+'/'+dataFormatada[1];
                    }
                    if(dataFormatada[0] === 'Aug'){
                        return 'Ago'+'/'+dataFormatada[1];
                    }
                    if(dataFormatada[0] === 'Sep'){
                        return 'Set'+'/'+dataFormatada[1];
                    }
                    if(dataFormatada[0] === 'Oct'){
                        return 'Out'+'/'+dataFormatada[1];
                    }
                    if(dataFormatada[0] === 'Dec'){
                        return 'Dez'+'/'+dataFormatada[1];
                    } 
                    else{
                        return dataFormatada[0]+"/"+dataFormatada[1]
                    }

                } 
                if (tipo === 2) {
                    return moment(date).format('DD/MM/YYYY HH:mm');
                }else {
                    return moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY');
                }
            },
            //função global para formatar data pt -> eng
            backEndDateFormat: function(date) {
                return moment(date, 'DD/MM/YYYY').format('YYYY-MM-DD');
            },
			async listarUF(br = ''){

				var params = {
					columns: 'DISTINCT (uf) as UF',
					table: 'ufRodovia',
				};
				if(br != '')
					params.where = {br: br};

				var data = await this.getFromAPI(params);
				return data;
			},
			async listarBR(uf = ''){

				var params = {
					columns: 'DISTINCT (br) as BR',
					table: 'ufRodovia',
				};
				if(uf != '')
					params.where = {UF: uf};

				var data = await this.getFromAPI(params);
				return data;
			},
			showMessage(type, title, text){ 
				Swal.fire({
					position: 'center',
					type: type,
					title: title,
					text: text,
					showConfirmButton: true,
				});
			},
			showLoading(){
				$('#preloader').show();
			},
			closeLoading(){
				$('#preloader').hide();
			}
        }
	});
	
	function showLoading(){
		vmGlobal.showLoading();
	}

	function closeLoading(){
		vmGlobal.closeLoading();
	}

	function showMessage(type, title, text){
		vmGlobal.showMessage(type, title, text);
	}
</script>
