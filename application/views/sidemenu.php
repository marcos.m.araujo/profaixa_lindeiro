
<aside class="main-sidebar sidebar-dark-primary elevation-4">

	<a href="<?= base_url('Home') ?>" class="brand-link" style="color: white">
		<div class="d-flex justify-content-center">

			<img src="<?= base_url() ?>/img_dpp/profaixa-logo2.png" alt="ProFaixa" width="80px">
		</div>
	</a>

	<div class="sidebar ">
		<nav class="mt-2">
		
			<ul id="listaMenu" class="nav nav-pills nav-sidebar flex-column " data-widget="treeview" role="menu" data-accordion="false">
			<!-- Add icons to the links using the .nav-icon class
				with font-awesome or any other icon font library -->
				<li class="nav-item" has-treeview >
					<a href="<?=base_url()?>Home" class="nav-link">
						<i class="nav-icon fas fa-home"></i>
						<p>Home</p>
					</a>
				</li>
				<li class="nav-item" has-treeview >
					<a href="<?=base_url()?>GestaoConflitosFaixa" class="nav-link">
						<i class="nav-icon fas fa-road"></i>
						<p>Gestão Faixas de Domínio</p>
					</a>
				</li>
				<li class="nav-item" has-treeview >
					<a href="<?=base_url()?>GestaoConflitosEmAndamento" class="nav-link">
						<i class="nav-icon fas fa-tasks"></i>
						<p>Conflitos em Análise</p>
					</a>
				</li>
				<li class="nav-item" has-treeview >
					<a href="<?=base_url()?>GestaoConflitosEmHomologacao" class="nav-link">
						<i class="nav-icon fas fa-check-double"></i>
						<p>Conflitos em Homologação</p>
					</a>
				</li>
				<!-- <li class="nav-item" has-treeview >

				<li class="nav-item has-treeview menu-open">
				
				<ul class="nav nav-treeview" style="display: block;">
				
					<li class="nav-item">
						
					</li>
					<li class="nav-item">
						
					</li>
					<li class="nav-item">
						
					</li>
					<li class="nav-item">
						<a href="<?=base_url()?>" class="nav-link">
						<i class="nav-icon fas fa-road"></i>
						<p>Faixas para Publicação</p>
						</a>
					</li> -->
				</ul>
			</li>
			</ul>
		<nav>
		
		<br>

		<nav class="mt-2">
		
			<div class="filtros" id="vmFiltros">
				<ul id="listaFiltros" class="nav nav-pills nav-sidebar flex-column " data-widget="treeview" 
					role="menu" data-accordion="false" style="border:0px !important"
				>
				<li class="nav-item ml-3">
					<i class="nav-icon">UF</i>
					<p class="ml-4">
						<select v-model="params.uf" class="form-control-sm ml-2" class="selectFiltro">
							<option value="">Brasil</option>
							<option v-for="item in listaUF" :value="item.UF">
								{{item.UF}}
							</options>
						</select>
					</p>
				</li>
				<li class="nav-item ml-3">
					<i class="nav-icon">BR</i>
					<p class="ml-4">
						<select v-model="params.br" class="form-control-sm ml-2" class="selectFiltro">
							<option value="">Todas</option>
							<option v-for="item in listaBR" :value="item.BR">{{item.BR}}</options>
						</select>
					</p>
				</li>
				<li class="nav-item ml-3">
					<i class="nav-icon">Município</i>
					<p class="ml-4">
						<select v-model="params.Municipio" class="form-control-sm ml-2" class="selectFiltro" style="width: 150px">
							<option value="">Todos</option>
							<option v-for="item in listaMunicipio" :value="item.Municipio">{{item.Municipio}}</options>
						</select>
					</p>
				</li>
				</ul>
			</div>
		</nav>
	<!-- /.sidebar-menu -->
	</div>
</aside>
<style>
.selectFiltro{
	width: 150px;
}
</style>
