<div id="conflitosEmHomologacao">
	
   <gestao-conflito-faixa
		ref="gestaoConflitosEmHomologacao"
		tipo="Em Homologação"
   >
   </gestao-conflito-faixa>
</div>

<?php $this->load->view("/componentes/mapa_conflito_faixa") ?>
<?php $this->load->view("/componentes/mapa_from_kml") ?>
<?php $this->load->view("/componentes/gestao_conflito_faixa") ?>

<script>

var vmFiltros = new Vue({
    el: '#vmFiltros',
    data(){
        return {
            params: {
                uf: '',
                br: '',
				municipio: '',
            },
            listaUF: null,
            listaBR: null,
            listaMunicipio: null,            
        }
    },
    async mounted(){
        await this.listarUF();
        await this.listarBR();
    },	
	watch:{
		'params.uf': function(val){
			this.listarBR();
			this.listarMunicipio();
			this.consulta();
		},
		'params.br': function(val){
			this.listarUF();
			this.listarMunicipio();
			this.consulta();
		},
		'params.Municipio': function(val){
			this.consulta();
		},
	},
    methods: {
		
        async consulta(){
            vmConflitosEmHomologacao.$refs.gestaoConflitosEmHomologacao.params = this.params;
            await vmConflitosEmHomologacao.$refs.gestaoConflitosEmHomologacao.reloadMapa('','');            
        },
        async listarUF(){
			var params = {
				columns: 'DISTINCT (UF)',
				table: 'ufRodovia',
			};
			params.whereCast = [{index: 'uf!=', value: "''"}];
			
			if(this.params.br != '')
				params.where = {br: this.br};
			

			var data = await vmGlobal.getFromAPI(params, 'cgdr');
			this.listaUF = data;
            
        },
        async listarBR(){
			
			var params = {
					columns: 'DISTINCT (BR)',
					table: 'ufRodovia',
				};
				if(this.params.uf != '')
					params.where = {UF: this.params.uf};

			var data = await vmGlobal.getFromAPI(params, 'cgdr');
            this.listaBR = data;
        },
        async listarMunicipio(){
            var params = {
				columns: 'DISTINCT (Municipio)',
				table: 'ufRodovia',
			};
			if(this.params.uf != '')
				params.where = {UF: this.params.uf};
			
			if(this.params.br != '')
				params.where = {BR: this.params.br};

			var data = await vmGlobal.getFromAPI(params, 'cgdr');
			this.listaMunicipio = data;
        },
        limpar(){
            window.location.reload()
        }, 

    },
});
var vmConflitosEmHomologacao= new Vue({
	el: "#conflitosEmHomologacao",
	data() {
		return {
		}
	},
	methods: {
		
        async consulta(){
			vmConflitosEmHomologacao.$refs.gestaoConflitosEmHomologacao.params = this.params;
			await vmConflitosEmHomologacao.$refs.gestaoConflitosEmHomologacao.reloadMapa('','');
		},
		pointMap(a, b) {
			this.$refs.gestaoConflitosEmHomologacao.pointMap(a,b);
		},

		abrirDadosImovel(a, b) {
			this.$refs.gestaoConflitosEmHomologacao.abrirDadosImovel(a, b)
		}
	}
});


async function pointMap(a, b) {
	vmConflitosEmHomologacao.pointMap(a,b);
}

function abrirDadosImovel(a, b) {
	vmConflitosEmHomologacao.abrirDadosImovel(a, b)
}
</script>
