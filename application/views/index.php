
<section class="content-header">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-sm-6">
                <h3></h3>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section id="conflitos" class="content">
    <div class="container-fluid">
		
		<div class="row">
			<!-- <div class="col-md-12 d-flex justify-content-center" style="top: 50px">
				<img src="<?=base_url('img_dpp/profaixa-logo2.png')?>" alt="Programa de Faixa de Domínio" width="220px">
			</div>
			<div class="col-md-12 d-flex justify-content-center" style="top: 50px"	>
				<h3>Programa Federal de Faixas de Domínio</h3>
			</div> -->

			<div class="col-md-12 d-flex justify-content-center mb-2" style="top: 100px">
				<div class="row">
					
					<?php $this->load->view("dashs") ?>

				</div>
				
			</div>
			<div class="row"  style="top: 150px">
				
				<div class="col-md-3">
				</div>
				<div class="col-md-4 d-flex justify-content-center">
					<div id="containerFaixas" style="min-width: 610px; height: 400px; max-width: 600px; margin: 0 auto; "></div>
				</div>
				
				<div class="col-md-1">
				</div>
				<div class="col-md-3 d-flex justify-content-center">
					<div id="containerConflitos" style="min-width: 610px; height: 400px; max-width: 600px; margin: 0 auto; "></div>
				</div>
				
				<!-- <div class="col-md-3">
				</div> -->
			</div>
			
		</div>
	</div>
</div>
<script src="<?=base_url('template_lte3/plugins/chart.js/Chart.min.js')?>"></script>
<script>
	vmHome = new Vue({
		el: "#conflitosEmAndamento",
		data() {
			return {
			}
		},
		async mounted(){
			await this.montaGraficoTotalDeConflitos();
			await this.montaGraficoTotalDeFaixas();
		},
		methods: {
			async montaGraficoTotalDeConflitos() {

				var controller = 'GestaoConflitosFaixa/getQuantitativo';
				var result = await vmGlobal.getFromController(controller, {});
			

				Highcharts.chart('containerConflitos', {
					credits: {
						enabled: false
					},
					chart: {
						plotBackgroundColor: null,
						plotBorderWidth: 0,
						plotShadow: false,
						backgroundColor: 'rgba(0,0,0,0)',
						
					},
					title: {
						text: result.total + '<br>Conflitos',
						align: 'center',
						verticalAlign: 'middle',
						y: 120,
						x: -30,
						style: {
							fontWeight: 'bold',
							color: 'black',
							fontSize: '18px'
						},
					},
					tooltip: {
						pointFormat: '{point.percentage:.1f}%',
						style: {
							fontWeight: 'bold',
							color: 'black',
							fontSize: '12px'
						},
					},
					plotOptions: {
						pie: {
							dataLabels: {
								connectorColor: 'grey',
								color:'black',
								y:-10,
								softConnector: false,
								connectorWidth:1,
								verticalAlign:'bottom',
								distance: 10,
								style: {
									fontWeight: 'bold',
									color: 'black',
									fontSize: '12px'
								},
								formatter: function () {
									if(this.percentage!=0)  return this.point.name + ': ' + this.point.y;

								}
							},
							startAngle: -90,
							endAngle: 90,
							center: ['45%', '95%'],
							size: '80%'
						}
					},
					series: [{
						name: 'Conflitos',
						type: 'pie',
						innerSize: '60%',
						data: result.data,
						animation: false,
						events:{
							click: function (event) {
								location.href = event.point.options.url;
							}
						}
					}],
					exporting: {
						enabled: false
					}
				});
			},

			async montaGraficoTotalDeFaixas() {


				Highcharts.chart('containerFaixas', {
					credits: {
						enabled: false
					},
					chart: {
						plotBackgroundColor: null,
						plotBorderWidth: 0,
						plotShadow: false,
						backgroundColor: 'rgba(0,0,0,0)',
						
					},
					title: {
						text: 1330 + '<br>Faixas',
						align: 'center',
						verticalAlign: 'middle',
						y: 120,
						x: -30,
						style: {
							fontWeight: 'bold',
							color: 'black',
							fontSize: '18px'
						},
					},
					tooltip: {
						pointFormat: '{point.percentage:.1f}%',
						style: {
							fontWeight: 'bold',
							color: 'black',
							fontSize: '12px'
						},
					},
					plotOptions: {
						pie: {
							dataLabels: {
								connectorColor: 'grey',
								color:'black',
								y:-10,
								softConnector: false,
								connectorWidth:1,
								verticalAlign:'bottom',
								distance: 10,
								style: {
									fontWeight: 'bold',
									color: 'black',
									fontSize: '12px'
								},
								formatter: function () {
									if(this.percentage!=0)  return this.point.name + ': ' + this.point.y;

								}
							},
							startAngle: -90,
							endAngle: 90,
							center: ['45%', '95%'],
							size: '80%'
						}
					},
					series: [{
						name: 'Conflitos',
						type: 'pie',
						innerSize: '50%',
						data: [{
							name: 'Publicadas',
							y: 30
						},{
							name: 'Com Conflito',
							y: 30
						},{
							name: 'Em Análise',
							y: 60
						},{
							name: 'Pendentes de<br> Publicação',
							y: 30
						}],
						animation: false,
						events:{
							click: function (event) {
								location.href = event.point.options.url;
							}
						}
					}],
					exporting: {
						enabled: false
					}
				});
				}

		}
				
	});

	

</script>
<style>
.highcharts-data-labels span {
  width: 100px; 
  word-break: break-word !important;
  white-space: normal !important;
}
</style>
