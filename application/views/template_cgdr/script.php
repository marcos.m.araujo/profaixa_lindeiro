
<!-- javascripts -->
<script src="<?= base_url('template_lte3/plugins/jquery/jquery.min.js') ?>"></script>
<script src="<?= base_url('template_lte3/plugins/jquery-ui-1.12.0/jquery-ui.min.js') ?>"></script>
<script src="<?= base_url("template_lte3/plugins/jquery-mask/dist/jquery.mask.min.js") ?>"></script>
<script src="<?=base_url("template_lte3/plugins/inputmask/inputmask/jquery.inputmask.js")?>"></script>

<script src="<?= base_url('new_theme/vue/vue.js') ?>"></script>
<script src="<?= base_url('new_theme/vue/axios.min.js') ?>"></script>
<!-- DataTables -->
<script src="<?= base_url('template_lte3/plugins/datatables/jquery_dataTables.js') ?>"></script>
<script src="<?= base_url('template_lte3/plugins/datatables/Buttons-1.6.1/js/dataTables.buttons.min.js') ?>"></script>
<script src="<?= base_url('template_lte3/plugins/datatables/Buttons-1.6.1/js/buttons.bootstrap4.js') ?>"></script>

<script src="<?= base_url('template_lte3/plugins/datatables/Buttons-1.6.1/js/buttons.html5.min.js')?>"></script>
<script src="<?= base_url('template_lte3/plugins/datatables/Buttons-1.6.1/js/buttons.print.min.js')?>"></script>
<script src="<?= base_url('template_lte3/plugins/datatables/Buttons-1.6.1/js/buttons.flash.min.js')?>"></script>

<script src="<?= base_url('template_lte3/dist/js/comum.js') ?>"></script>
<script src="<?= base_url('new_theme/Highcharts/') ?>code/highcharts.js"></script>
<script src="<?= base_url('new_theme/Highcharts/') ?>code/highcharts-3d.js"></script>
<script src="<?= base_url('new_theme/Highcharts/') ?>code/modules/exporting.js"></script>
<script src="<?= base_url('template_lte3/plugins/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
<!-- DataTables -->

<!-- FastClick -->
<script src="<?= base_url('template_lte3/plugins/fastclick/fastclick.js') ?>"></script>
<!-- Moment -->
<script src="<?= base_url('template_lte3/plugins/moment/moment.js') ?>"></script>

<!-- AdminLTE App -->
<script src="<?= base_url('template_lte3/dist/js/adminlte.min.js') ?>"></script>

<script src="<?= base_url('template_lte3/plugins/datatables/pdfmake-0.1.36/pdfmake.min.js') ?>"></script>
<script src="<?= base_url('template_lte3/plugins/datatables/pdfmake-0.1.36/vfs_fonts.js') ?>"></script>

<script src="<?= base_url('template_lte3/plugins/datatables/JSZip-2.5.0/jszip.min.js') ?>"></script>

<script src="<?= base_url('template_lte3/plugins/gstatic/loader.js') ?>"></script>

<script src="<?= base_url('template_lte3/plugins/sweetalert2/sweetalert2.all.min.js') ?>"></script>


<script>
	
	var base_ul = '<?=base_url()?>';
	
	
	setTimeout(() => {
		<?php

/* pegar controller
* $this->router->fetch_class()
* Pegar Action
* $this->router->fetch_method();
*/

//verifica se o controller é do faleconosco para personalizar datatable (item povisório)

if ($this->router->fetch_class() == "FaleConoscoDPP") {
	echo 'personalizarDataTable("#tables", true)';
}
?>

}, 1500);
   
       
        //Função para Personalizar DataTalbes
        function personalizarDataTable(idTabela, bSortProp = null) {
            $(idTabela).DataTable({
                "scrollX": true,
                dom: 'Bfrtip',
                "destroy": true,
                buttons: [
                    'excel'
                ],
                "bSort": bSortProp,
                "bSortable": true,
                'autoWidth': true,
                "oLanguage": {
                    "sEmptyTable": "Nenhum registro encontrado na tabela",
                    "sInfo": "Mostrar _START_ até _END_ do _TOTAL_ registros",
                    "sInfoEmpty": "Mostrar 0 até 0 de 0 Registros",
                    "sInfoFiltered": "(Filtrar de _MAX_ total registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "Mostrando _MENU_ registros por pagina",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Busca: ",
                    "oPaginate": {
                        "sNext": "<i class=\"fas fa-chevron-right\" ></i>",
                        "sPrevious": "<i class=\"fas fa-chevron-left\" ></i>",
                        "sFirst": "Primeiro",
                        "sLast": "Ultimo"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                },
            });
			
        }
		</script>
