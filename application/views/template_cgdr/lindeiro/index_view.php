<!DOCTYPE html>
<html lang="pt-br">

	<head>
		<?php $this->load->view('template_cgdr/head') ?>

		<style>
			.content-wrapper {
				width: 100% !important;
			}

			.sidebar-mini.sidebar-collapse .content-wrapper {
				margin-left: 0px !important;
			}
			

		.required label {
			font-weight: bold;
		}

		.required label:after {
			color: #e32;
			content: ' *';
			display: inline;
		}
		</style>
	</head>

	<body id="body" class="sidebar-mini sidebar-collapse">

		<?php $this->load->view('template_adminlte3/script') ?>
		<?php $this->load->view('template_adminlte3/script_interno') ?>

		<div class="wrapper">

			<?php $this->uri->uri_string() != 'Lindeiro/index' ? $this->load->view('template_cgdr/lindeiro/header') : '' ?>

			<div class="content-wrapper">
				<?php $this->load->view($pagina) ?>
			</div>
			
			<footer class="main-footer">
				<?php $this->load->view('template_cgdr/footer') ?>
			</footer>
		</div>
	</body>

</html>
