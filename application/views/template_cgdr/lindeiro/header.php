<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
	
		<a href="<?= base_url('Lindeiro/index') ?>" class="navbar-brand">
			<img src="<?= base_url('img_dpp/profaixa-logo2.png') ?>" alt="ProFaixa Logo" class="" width="70px">
		</a>

		<button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse order-3" id="navbarCollapse">
			<!-- Left navbar links -->
			<ul class="navbar-nav">
				<li class="nav-item">
					<a href="<?= base_url('Lindeiro/index') ?>" class="nav-link">
						<!-- <i class="fas fa-home icon-color"></i> -->
						Home
					</a>
				</li>
				<li class="nav-item">
					<a href="<?= base_url('Lindeiro/cadastro') ?>" class="nav-link">Cadastro</a>
				</li>
				<li class="nav-item">
					<a href="<?= base_url('Lindeiro/consulta') ?>" class="nav-link">Consulta</a>
				</li>

			</ul>

		</div>
</nav>
