<style>

	/*menu mobile*/
	.icon-color {
		color: #17a2b8;
	}

	@media (min-width: 350px) and (max-width: 992px) {
		.mobile-hide {
			display: none;
		}

		/*    menu mobile*/
		.dropdown-submenu {
			position: relative;
		}

		.dropdown-submenu>.dropdown-menu {
			top: 0;
			left: 100%;
			margin-top: -6px;
			margin-left: -1px;
			-webkit-border-radius: 0 6px 6px 6px;
			-moz-border-radius: 0 6px 6px;
			border-radius: 0 6px 6px 6px;
		}

		i .dropdown-submenu:hover>.dropdown-menu {
			display: block;
		}

		.dropdown-submenu>a:after {
			display: block;
			content: " ";
			float: right;
			width: 0;
			height: 0;
			border-color: transparent;
			border-style: solid;
			border-width: 5px 0 5px 5px;
			border-left-color: #ccc;
			margin-top: 5px;
			margin-right: -10px;
		}

		.dropdown-submenu:hover>a:after {
			border-left-color: #fff;
		}

		.dropdown-submenu.pull-left {
			float: none;
		}

		.dropdown-submenu.pull-left>.dropdown-menu {
			left: -100%;
			margin-left: 10px;
			-webkit-border-radius: 6px 0 6px 6px;
			-moz-border-radius: 6px 0 6px 6px;
			border-radius: 6px 0 6px 6px;
		}

		/*menu mobile*/
		.dropdown-menu li:hover .dropdown-menu {
			position: absolute;
			width: 188px;
		}
	}

	@media (min-width: 992px) and (max-width: 2900px) {
		.menu-mobile {
			display: none;
		}

		/*    menu mobile*/
		.dropdown-submenu {
			position: relative !important;
		}

		.dropdown-submenu>.dropdown-menu {
			top: 0;
			left: 100%;
			margin-top: -6px;
			margin-left: -1px;
			-webkit-border-radius: 0 6px 6px 6px;
			-moz-border-radius: 0 6px 6px;
			border-radius: 0 6px 6px 6px;
		}

		.dropdown-submenu:hover>.dropdown-menu {
			display: block;
		}

		.dropdown-submenu>a:after {
			display: block;
			content: " ";
			float: right;
			width: 0;
			height: 0;
			border-color: transparent;
			border-style: solid;
			border-width: 5px 0 5px 5px;
			border-left-color: #ccc;
			margin-top: 5px;
			margin-right: -10px;
		}

		.dropdown-submenu:hover>a:after {
			border-left-color: #fff;
		}

		.dropdown-submenu.pull-left {
			float: none;
		}

		.dropdown-submenu.pull-left>.dropdown-menu {
			left: -100%;
			margin-left: 10px;
			-webkit-border-radius: 6px 0 6px 6px;
			-moz-border-radius: 6px 0 6px 6px;
			border-radius: 6px 0 6px 6px;
		}

		/*menu mobile*/
		.dropdown-menu li:hover .dropdown-menu {
			position: absolute;
		}
	}

	/**/
</style>
<ul id="menu" class="navbar-nav mobile-hide">
	<li class="nav-item btnSidemenu">
		<a style="color: silver" class="nav-link" data-widget="pushmenu" href="#"><i style="color: #17a2b8" class="fas fa-filter"></i></a>
	</li>
	<li class="nav-item" style="margin-left: 10px">
		<a title="Voltar para página inicial" style="color: black" class="nav-link" href="<?= base_url('Home') ?>"><i class="fas fa-home icon-color"></i> Home</a>
	</li>
	<li class="nav-item dropdown show">
		<a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="nav-link dropdown-toggle"><i class="fas fa-chart-area icon-color"></i> Módulos</a>
		<ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow ">
			<li><a href="<?= base_url('GestaoConflitosFaixa') ?>" class="dropdown-item">Gestão de Faixas</a></li>
			<li><a href="<?= base_url('GestaoConflitosEmAndamento') ?>" class="dropdown-item">Conflitos Em Análise</a></li>
			<li><a href="<?= base_url('GestaoConflitosEmHomologacao') ?>" class="dropdown-item">Conflitos Em Homologação</a></li>
			<!-- <li><a href="<?= base_url('GestaoFaixasApublicar') ?>" class="dropdown-item">Faixas Para Publicação</a></li> -->
			<li class="dropdown-divider"></li>
			<li><a href="<?= base_url('Home') ?>" class="dropdown-item"><strong>Home</strong></a></li>
		</ul>
	</li>
	
</ul>

<nav class="navbar navbar-expand-lg  menu-mobile">

	<div class="collapse navbar-collapse " id="navbarNavDropdown">
		<ul class="navbar-nav">

			<li class="nav-item dropdown">
				<a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
					<i class="fas fa-bars bars"></i>
				</a>
				<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					<a title="Voltar para página inicial" href="<?= base_url('Home') ?>" class="dropdown-item"><i class="fa fa-home icon-color"></i> Home</a>
					<div class="dropdown-divider"></div>
					<li class="dropdown-submenu">
						<a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="dropdown-item dropdown-toggle"><i class="fas fa-chart-area icon-color"></i> Módulos</a>
						<div class="dropdown-divider"></div>
						<ul class="dropdown-menu">
							<li><a href="<?= base_url('GestaoConflitosFaixa') ?>" class="dropdown-item">Conflitos Abertos</a></li>
							<li><a href="<?= base_url('GestaoConflitosEmAndamento') ?>" class="dropdown-item">Conflitos Em Andamento</a></li>
							<li><a href="<?= base_url('GestaoConflitosEmHomologacao') ?>" class="dropdown-item">Conflitos Em Homologação</a></li>
						</ul>
					</li>

				</ul>
			</li>
		</ul>
	</div>
</nav>


<ul id="msg" class="navbar-nav ml-auto ">

	<li class="nav-item dropdown show">
		<a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="nav-link dropdown-toggle"><?= $this->session->nome ?></a>
		<ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow ">
			<li><a href="<?= base_url('DPP/logout') ?>" class="dropdown-item"><strong>Sair</strong></a></li>
		</ul>
	</li>

	<li class="nav-item dropdown">
		<a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="true">
			<i class="far fa-bell"></i>
			<span class="badge badge-warning navbar-badge">{{msg.length}}</span>
		</a>
		<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
			<span class="dropdown-item dropdown-header">Notificações</span>
			<div class="dropdown-divider"></div>
			<a v-for="i in msg" :href="(i.StatusConflito == 'Em Homologação') ? base_url + 'GestaoConflitosEmHomologacao' : base_url + 'GestaoConflitosEmAndamento'" class="dropdown-item">
				<p>Análise de Conflito (BR-{{i.FaixaDominioBR}}/{{i.FaixaDominioUF}})</p>
				<i class="fas fa-bell"></i> {{i.StatusConflito}} <br>De:
				<strong>{{i.TramitadoPor}} </strong><br>
				<span>{{moment(String(i.Data)).format('DD/MM/YYYY HH:mm:ss')}}</span>
			</a>

			<div class="dropdown-divider"></div>
			<!-- <a href="<?= base_url('Notificacao') ?>" class="dropdown-item dropdown-footer">Ver Todas as Notificações</a>
			<div class="dropdown-divider"></div> -->
			<!-- <div data-toggle="modal" data-target="#myModal" style="text-align: center; background-color: #E9F1F4; cursor: pointer; padding:5px; font-weight: bold">
				Criar novo fale conosco <img width="15%" src="<?= base_url('img_dpp/fale_conosco.png') ?>">
			</div> -->
		</div>
	</li>
	<li>
		<a href="javascritp:;" onclick="toggleFullScreen()" class="nav-link"><i class="fas fa-expand"></i></a>
	</li>


</ul>


<script>
	var base_url = '<?= base_url() ?>';
	
	$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
		if (!$(this).next().hasClass('show')) {
			$(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
		}
		var $subMenu = $(this).next('.dropdown-menu');
		$subMenu.toggleClass('show');


		$(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
			$('.dropdown-submenu .show').removeClass('show');
		});

		return false;
	});

	vmMensagens = new Vue({
		el: "#msg",
		data() {
			return {
				msg: [],
				balde: [],
			}
		},
		mounted() {
			this.get()
			setInterval(() => {
				this.get()
			}, 60000);
		},
		methods: {
			async get() {
				var controller = "GestaoConflitosFaixa/getTramitadosPara";
				var params = {email: localStorage.getItem('email')}
				this.msg = await vmGlobal.getFromController(controller, params);
			},

		}
	});
</script>
