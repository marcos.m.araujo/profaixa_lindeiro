<!DOCTYPE html>
<html lang="pt-br">

<head>
	<?php $this->load->view('template_cgdr/head') ?>

	<style>
		.sidebar>.nav-treeview>.nav-item>.nav-link {
			color: #c2c7d0;
		}

		.sidebar a {
			color: #c2c7d0;
		}

		.filtros {
			color: #c2c7d0;
			width: 80%;
		}

		#menu {
			color: #000;
		}

		#menu>li>a {
			color: #000;
		}

		#msg>li>a {
			color: #000;
		}

		#menu>nav,
		#listaMenu {
			border: 0px !important;
		}

		.navbar-nav>.nav-item>.nav-link {
			color: #000;
		}

		.navbar-nav>.nav-item>.nav-link :hover {
			color: #2a4d73;
		}

		.required label {
			font-weight: bold;
		}

		.required label:after {
			color: #e32;
			content: ' *';
			display: inline;
		}
	</style>
</head>

<body id="body" class="sidebar-mini sidebar-collapse">
	<?php $this->load->view('template_adminlte3/script') ?>
	<?php $this->load->view('template_adminlte3/script_interno') ?>
	<div class="wrapper">
		<nav style="background-color: #F4F6F9;" class="main-header navbar navbar-expand border-bottom navbar-dark" id="menuPrincipalAtlas">
			<?php $this->load->view('template_cgdr/header') ?>
		</nav>
		<?php $this->load->view($sidebar) ?>

		<div class="content-wrapper">

			<?php $this->load->view($pagina) ?>
		</div>
		<footer class="main-footer">
			<?php $this->load->view('template_cgdr/footer') ?>
		</footer>
	</div>
</body>

</html>
<script>
	// abre e fecha menu
	$('.content-wrapper').click(function() {
		var classe = $('#body').attr("class");
		if (classe == 'sidebar-open') {
			$('#body').removeClass("sidebar-open");
			$('#body').addClass("sidebar-collapse");
		}
	});


	// Ativa(fica azul) botao menu
	var uri = '<?= $this->uri->uri_string(); ?>';
	$(document).ready(function() {

		$('#listaMenu > li > a').each(function(index, node) {
			// console.log(2, node);
			link = node.getAttribute('href');
			link = link.replace('<?= base_url() ?>', '');
			if (link === uri) {
				$(this).addClass('active');
			}
		});
	});
</script>
