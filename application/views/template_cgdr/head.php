
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<title>CGDR / ProFaixa</title>
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSSs -->
<link rel="shortcut icon" href="<?= base_url() ?>/js/dpp/menu/img/favicon.ico">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/plugins/fontawesome-free/css/all.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/plugins/datatables/jquery.dataTables.css">
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/plugins/datatables/dataTables.bootstrap4.css">
<link rel="stylesheet" href="<?php print base_url('theme/') ?>datatable/buttons.dataTables.min.css">
<!-- overlayScrollbars -->
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/dist/css/adminlte.min.css">
<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/plugins/fontawesome-free/css/all.min.css">

<!-- COLOR PIKER-->
<link rel="stylesheet" href="<?= base_url() ?>template_lte3/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">

