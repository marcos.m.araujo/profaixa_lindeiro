<div class="modal fade" id="modalImovel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">

		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<i style="margin-left: 10px; color: green" class="fas fa-home"></i>
					<span class="ml-3">Dados Imóvel
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="card-footer p-0">
				
				<ul v-for="i in detalheImovel" class="nav flex-column">
					<li class="nav-item" v-for="(index, key) in i">
						<span class="nav-link">
							{{key}}: {{index}} <span class="float-right">{{i.key}}</span>
						</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
