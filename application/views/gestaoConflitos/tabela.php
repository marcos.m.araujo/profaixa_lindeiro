<table id="tables" class="dataTables_wrapper dt-bootstrap4 no-footer" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Abrir mapa</th>
            <th>CodigoConflito</th>
            <th>KmInicialConflito</th>
            <th>KmFinalConflito</th>
            <th>ExtensaoConflito</th>
            <th>AreaConflitoKm2</th>
            <th>PerimetroConflitoKm</th>
            <th>CodigoEmpreendimento</th>
            <th>Fonte</th>
            <th>StatusConflito</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="i in listaConflito">
            <td style="text-align: center;"><a style="cursor: pointer;" @click="pointMap(i.CodigoConflito,i.CodigoFaixaDominio)"><i class="fas fa-map-marker-alt"></i></a></td>
            <td>{{i.CodigoConflito}}</td>
            <td>{{i.KmInicialConflito}}</td>
            <td>{{i.KmFinalConflito}}</td>
            <td>{{formatNumeric(i.ExtensaoConflito)}}</td>
            <td>{{formatNumeric(i.AreaConflitoKm2)}}</td>
            <td>{{i.PerimetroConflitoKm}}</td>
            <td>{{i.CodigoEmpreendimento}}</td>
            <td>{{i.Fonte}}</td>
            <td>{{i.StatusConflito}}</td>
        </tr>
    </tbody>
</table>
