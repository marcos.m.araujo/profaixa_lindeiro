<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin="" />
<link rel="stylesheet" href="<?= base_url('theme/leaflet/Leaflet.PolylineMeasure.css') ?>" />
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA==" crossorigin=""></script>
<script src="<?= base_url('theme/leaflet/Leaflet.PolylineMeasure.js') ?>"></script>


<style>
    .map-height {
        width: 100%;
        height: 700px;
        z-index: 1
    }

    .card-body {
        min-height: 750px;
    }

    .timelines {
        max-height: 700px;
        overflow: auto
    }

    .acao {
        color: red
    }

    .flex-column {
        border: 1px solid silver !important;
    }

    .btnMapa {
        position: absolute;
        left: 10px;
        padding: 8px;
        text-align: center;
        z-index: 1000;
        width: 34px;
        background-color: #FFFF;
        border-radius: 4px;
    }

    .baseMapa {
        top: 185px;
    }

    .baseConflito {
        top: 230px;
    }

    .baseFaixa {
        top: 270px;
    }

    .baseImovel {
        top: 310px;
    }

    .baseRodovia {
        top: 360px;
        padding: 0px;
        font-size: 7pt;
        height: 35px;
        font-weight: bold;
    }
</style>

<section class="content-header">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-sm-6">
                <h3>Gestão de Conflitos de Faixa</h3>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Blank Page</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section id="conflitos" class="content">
    <div class="row">

        <div class="card " :class="[codigoConflito != ''? 'col-md-8':'col-md-12']">
            <div class="card-header">
                <div class="mailbox-controls">
                    <button @click="telaMapa = true, telaGrafico = false, telaTabela = false" type="button" :class="[telaMapa ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']"><i class="fas fa-map"></i> Mapa de conflitos</button>
                    <button @click="telaMapa = false, telaGrafico = true, telaTabela = false" type="button" :class="[telaGrafico ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']"><i class="fas fa-chart-bar"></i> Sala de situação</button>
                    <button @click="telaMapa = false, telaGrafico = false, telaTabela = true" type="button" :class="[telaTabela ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']"><i class="fas fa-table"></i> Lista de conflitos</button>
                    <button @click="location.reload()" type="button" class="btn btn-default btn-sm"><i class="fas fa-sync-alt"></i> Limpar</button>
                </div>
            </div>
            <div class="card-body ">
                <div id="mapid" v-show="telaMapa" class="map-height">
                    <button @click="mudaVisualizacaoMapa()" class="btnMapa baseMapa"><i class="fas fa-map"></i></button>
                    <button @click="layerConflito()" class="btnMapa baseConflito"><i style="color: red" class="fab fa-confluence"></i></button>
                    <button @click="layerFaixa()" class="btnMapa baseFaixa"><i style="color: blue" class="fas fa-road"></i></button>
                    <button @click="layerImovel()" class="btnMapa baseImovel"><i style="color: green" class="fas fa-home"></i></button>
                    <button @click="layerGetRodovias()" title="Rodovias" id="baseRodovia" class="btnMapa baseRodovia">SNV</button>
                </div>
                <div v-show="telaGrafico" class="row">
                    <?php $this->load->view("gestaoConflitos/dashs") ?>
                </div>
                <div v-if="telaTabela">
                    <?php $this->load->view("gestaoConflitos/tabela") ?>
                </div>
            </div>
        </div>

        <div class="card col-md-4" v-if="codigoConflito != ''">
            <div class="card-header">
                <div class="mailbox-controls">
                    <button @click="dadosConflitoDetalhe = true, dadosDetalheEixo = false, timeLineDetalhe = false" type="button" :class="[dadosConflitoDetalhe ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']"><i class="fab fa-confluence"></i> Dados do conflito</button>
                    <button @click="dadosDetalheEixo = true, dadosConflitoDetalhe = false, timeLineDetalhe = false" type="button" :class="[dadosDetalheEixo ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']"><i class="fas fa-cogs"></i> Ações</button>
                    <button @click="timeLineDetalhe = true, dadosDetalheEixo = false, dadosConflitoDetalhe = false " type="button" :class="[timeLineDetalhe ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']"><i class="fas fa-history"></i> Histórico</button>
                </div>
            </div>
            <div style="background-color: #F7F7F7!important;" class="card-body ">
                <div v-show="dadosConflitoDetalhe">
                    <div class="card-footer p-0">
                        <i style="margin-left: 10px; color: red" class="fab fa-confluence"></i> Conflito
                        <ul v-for="i in conflitos" class="nav flex-column">
                            <li class="nav-item">
                                <span class="nav-link">
                                    Situação do conflito <span class="float-right badge bg-danger">{{i.StatusConflito}}</span>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    Área do conflito <span class="float-right">{{formatNumeric(i.AreaConflitoKm2,2)}} km²</span>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    Km inicial <span class="float-right">{{formatNumeric(i.KmInicialConflito, 1)}}</span>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    Km final <span class="float-right">{{formatNumeric(i.KmFinalConflito, 1)}}</span>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    Extensão aproximada do conflito <span class="float-right">{{formatNumeric(i.ExtensaoConflito,1)}} km</span>
                                </span>
                            </li>
                        </ul>
                    </div>

                    <div style="margin-top: 10px" class="card-footer p-0">
                        <i style="margin-left: 10px; color: green" class="fas fa-home"></i> Imóvel
                        <ul v-for="i in imoveis" class="nav flex-column">
                            <li class="nav-item">
                                <span class="nav-link">
                                    Munícipio do imóvel <span class="float-right">{{i.Municipio}}</span>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    Área do imóvel <span class="float-right">{{formatNumeric(i.AreaKm2,2)}} km²</span>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    Perimêtro do imóvel <span class="float-right">{{formatNumeric(i.PerimetroKm,2)}} km</span>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    Ver mais detalhes do imóvel <span class="float-right"><button type="button" @click="abrirDadosImovel(i.Fonte,i.CodigoTabelaFonte)" class="btn btn-block btn-success btn-sm">Abrir </button></span>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    Fonte <span class="float-right">{{i.Fonte}} </span>
                                </span>
                            </li>
                        </ul>
                    </div>

                    <div style="margin-top: 10px" class="card-footer p-0">
                        <i style="margin-left: 10px;color: blue" class="fas fa-road"></i> Faixa
                        <ul v-for="i in faixadominio" class="nav flex-column">
                            <li class="nav-item">
                                <span class="nav-link">
                                    Trecho <span class="float-right ">{{i.KmInicial}} ao {{i.KmFinal}}</span>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    Extensão <span class="float-right">{{i.ExtensaoKm}} km</span>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    Largura da faixa lado direito <span class="float-right">{{i.FaixaDominioLadoDireito}} m</span>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    Largura da faixa lado esquerdo <span class="float-right">{{i.FaixaDominioLadoEsquerdo}} m</span>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    Largura da faixa <span class="float-right">{{i.FaixaDominioLargura}} m</span>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    Checagem <span class="float-right badge">{{i.Checagem}} </span>
                                </span>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    Fonte <span class="float-right badge">{{i.Fonte}}</span>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div v-show="dadosDetalheEixo">
                    <div class="card-body p-0  " style="margin-top: 10px;">
                        <div class="form-group">
                            <label>Ação</label>
                            <select class="form-control">
                                <option>Reajustar Faixa de Domínio</option>
                                <option>Desapropriar</option>
                                <option>Reintegrar posse ou Firmar acordo de uso</option>
                                <option>Realizar chamamento individual</option>
                                <option>Solicitar explicações</option>
                                <option>Aguardar informações adicionais lindeiro</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tramitar para</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                        </div>

                        <div class="form-group">
                            <label>Observações</label>
                            <textarea class="form-control" rows="10" placeholder="Enter ..."></textarea>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info float-right">Salvar</button>
                        </div>
                    </div>
                </div>

                <div class="timelines" v-show="timeLineDetalhe">
                    <div class="callout callout-success">
                        <h5><i class="far fa-clock"></i> 23/10/2019</h5>
                        <strong>Marcos de Matos Araujo</strong>
                        <strong class="acao">Análise de eixo</strong>

                        <p>This is a green callout.</p>
                    </div>
                    <div class="callout callout-secondary">
                        <h5><i class="far fa-clock"></i> 22/10/2019</h5>
                        <strong>Marcos de Matos Araujo</strong>
                        <strong class="acao">Análise de eixo</strong>

                        <p>This is a green callout.</p>
                    </div>
                    <div class="callout callout-secondary">
                        <h5><i class="far fa-clock"></i> 21/10/2019</h5>
                        <strong>Marcos de Matos Araujo</strong>
                        <strong class="acao">Análise de eixo</strong>

                        <p>This is a green callout.</p>
                    </div>
                    <div class="callout callout-secondary">
                        <h5><i class="far fa-clock"></i> 20/10/2019</h5>
                        <strong>Marcos de Matos Araujo</strong>
                        <strong class="acao">Análise de eixo</strong>

                        <p>This is a green callout.</p>
                    </div>
                    <div class="callout callout-secondary">
                        <h5><i class="far fa-clock"></i> 19/10/2019</h5>
                        <strong>Marcos de Matos Araujo</strong>
                        <strong class="acao">Análise de eixo</strong>

                        <p>This is a green callout.</p>
                    </div>
                    <div class="callout callout-secondary">
                        <h5><i class="far fa-clock"></i> 18/10/2019</h5>
                        <strong>Marcos de Matos Araujo</strong>
                        <strong class="acao">Análise de eixo</strong>

                        <p>This is a green callout.</p>
                    </div>
                    <div class="callout callout-warning">
                        <h5><i class="far fa-clock"></i> 17/10/2019</h5>
                        <strong>Marcos de Matos Araujo</strong>
                        <strong class="acao">Análise de eixo</strong>

                        <p>This is a green callout.</p>
                    </div>

                </div>
            </div>


        </div>


        <div class="modal fade bd-example-modal-lg" id="modalImovel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">

                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Dados Imóvel</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div style="width: 100%;" class="modal-body">
                        <p style="text-align: justify; width: 50%;">{{formatJson(detalheImovel)}} </p>
                    </div>
                </div>
            </div>
        </div>
</section>

<?php $this->load->view("gestaoConflitos/script_old") ?>