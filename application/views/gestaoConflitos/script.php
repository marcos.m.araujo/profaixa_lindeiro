<script>
    
	var base_url = '<?= base_url() ?>';
    
	var vmGestaoConflitos = new Vue({
        el: "#conflitos",
    	// components: { 'mapa-conflito-faixa': mapaConflitoFaixa },
        data() {
            return {
                mymap: null,
                tabela: [],

				params: {
					uf: '',
					br: '',
					CodigoConflito: '',
                	CodigoFaixaDominio: '',
				},

				listaImovel: null,
				listaFaixa: null,
				listaConflito: null,

                detalheImovel: [],
                
				// telaSalaSituacao: false,
                telaTabela: false,
                telaMapa: true,
                
				acaoModel: {},
                
				dadosConflitoDetalhe: true,
                timeLineDetalhe: false,
				detalheAcao: false,

				listaAcao: null,

            }
        },
        async mounted() {
            
            
        },
        watch: {
            telaTabela() {
				this.listaConflito = this.$refs.mapaConflitoEmAberto.listaConflito
				vmGlobal.montaDatatable("#tables");
            },
        },
        methods: {
			
			recebeuf(val){
				vmFiltros.params.uf = val;
			},
            async pointMap(codigoConflito, codigoFaixaDominio) {

                this.telaTabela = false
                this.telaMapa = true
                
                this.$refs.mapaConflitoEmAberto.params.CodigoConflito = codigoConflito;
                this.$refs.mapaConflitoEmAberto.params.CodigoFaixaDominio = codigoFaixaDominio;
				await this.$refs.mapaConflitoEmAberto.consultaLayers();

				this.listaImovel = this.$refs.mapaConflitoEmAberto.listaImovel
				this.listaFaixa = this.$refs.mapaConflitoEmAberto.listaFaixa
				this.listaConflito = this.$refs.mapaConflitoEmAberto.listaConflito
			
				
				var params = {table: 'ConflitoAcao'};
				var data = await vmGlobal.getFromAPI(params, 'cgdr');
				this.listaAcao = data;
				
				this.params.CodigoConflito = this.$refs.mapaConflitoEmAberto.params.CodigoConflito;



            },
			async abrirDadosImovel(fonte, codigo) {

				var controller = 'GestaoConflitosFaixa/abrirDadosImovel';
				var params = {Fonte: fonte, Codigo: codigo};
				var data = await vmGlobal.getFromController(controller, params);

				this.detalheImovel = data;
				delete this.detalheImovel[0]['Coordenada']
				
				$("#modalImovel").modal()
            },
			async salvarAcao(){

				this.acaoModel.TramitadoPor = localStorage.getItem('email');
				this.acaoModel.CodigoConflito = this.listaConflito[0].CodigoConflito;
				this.acaoModel.CodigoFaixa = this.listaFaixa[0].CodigoFaixa;
				this.acaoModel.CodigoImovel = this.listaConflito[0].CodigoImovel;

				var params = {
					table: 'cgdrConflitoProposta',
					data: this.acaoModel
				}
				var response = await vmGlobal.insertFromAPI(params);

					if(response.status == true){

					this.acaoModel = {};

					var paramConflito = {
						table: 'cgdrConflitos',
						data: {StatusConflito: 'Em análise'},
						where: {CodigoConflito: this.params.CodigoConflito}
					}
					var response2 = await vmGlobal.updateFromAPI(params);

					if(response2.status == true)
						location.reload()
				}
			},
            formatData(param, tipo) {
                if (tipo === 1) {
                    return moment(String(param)).format('DD/MM/YYYY');
                } else {
                    return moment(String(param)).format('MM/YYYY');
                }
            },
            formatNumeric(value, tofix) {
                let val = (value / 1).toFixed(tofix)
                let val2 = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                return val2.toString().replace(".", ",")
            },

        },
    });

    async function pointMap(a, b) {
		vmGestaoConflitos.pointMap(a,b);
    }

    function abrirDadosImovel(a, b) {
        vmGestaoConflitos.abrirDadosImovel(a, b)
    }
</script>
