<template id="template-mapa-conflito-faixa">
	
			
		
		<!--End input search-->
		<div class="row">
			
			<div style="text-align: center; align-items: center;" class="col-md-12">
				
				<div id="mapid" class="map-height">

							
					<!-- Pesquisa por municipio -->
					<div id="buscaM" class="buscaM">
						<div class="col-lg-4 col-10" id="pesquisaMunicipio">
							<div class="input-group md-form form-sm form-2 pl-0">
								<!-- campo de pesquisa -->
								<input class="form-control" type="text" size="140"
									placeholder="Digite o nome do Município" id="inputMunic" list="inptmun" name="inptmun"
									@keypress="leafletVue.getMunicipio()">
								<!-- lista com municipios pesquisados -->
								<datalist id="inptmun">
									
								</datalist>
								<!-- botoes -->
								<div class="input-group-append">
									<!-- incluir municipio -->
									<button class="btn btn-info btn-sm btnBuscarM" @click="leafletVue.getMunicipioSelect()" title="Consultar município">
										<i class="fas fa-search text-grey" aria-hidden="true"></i>
									</button>
									<!-- excluir municipio -->
									<button title="Remover município do mapa" @click="leafletVue.clearMunicipioSelect()"
										style='display: none' class="btn btn-sm btn-danger btnClearBuscarM">
										<i class="fas fa-trash-alt"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
					<!-- FIM Pesquisa por municipio -->
					
					<button @click="leafletVue.zoomHome()" class="btnMapa btnHome"><i class="fas fa-home"></i>
					</button>

					<button @click="showImagemSatelite = !showImagemSatelite" 
						:style="[showImagemSatelite ? {'font-weight': 'bold', 'background-color': '#fff'} : {'font-weight': 'bold', 'background-color': '#BDBDBD'}]" 
						class="btnMapa baseMapa"
					>
						<i class="fas fa-map"></i>
					</button>

					<button @click="showLayerSNV = !showLayerSNV" title="Rodovias" id="baseRodovia" 
						:style="[showLayerSNV ? {'background-color':'#fff','font-weight':'bold'} : {'background-color':'#BDBDBD','font-weight':'normal'}]" 
						class="btnMapa baseRodovia"
					>
						SNV
					</button>
					
					<button @click="showLayerConflito = !showLayerConflito" 
						:style="[showLayerConflito ? {'background-color':'#fff','font-weight':'bold'} : {'background-color':'#BDBDBD','font-weight':'normal'}]" 
						class="btnMapa baseConflito"
					>
						<i style="color: red" class="fab fa-confluence"></i>
					</button>
					<button @click="showLayerFaixa = !showLayerFaixa" 
						:style="[showLayerFaixa ? {'background-color':'#fff','font-weight':'bold'} : {'background-color':'#BDBDBD','font-weight':'normal'}]" 
						class="btnMapa baseFaixa"
					>
						<i style="color: blue" class="fas fa-road"></i>
					</button>
					<button @click="showLayerImovel = !showLayerImovel" 
						:style="[showLayerImovel ? {'background-color':'#fff','font-weight':'bold'} : {'background-color':'#BDBDBD','font-weight':'normal'}]" 
						class="btnMapa baseImovel"
					>
						<i style="color: green" class="fas fa-home"></i>
					</button>
					
				</div>
			</div>
		</div>

<!-- 		
		<modal-component ref="modalcor" >
			<h5 slot="header">Selecione uma Cor</h5>
			<div slot="body">
				<div class="row" id="paleta-cores">
					<div class="input-group my-colorpicker2 colorpicker-element" data-colorpicker-id="2">
						
					<input type="text" class="form-control" data-original-title="" id="corLinhaSelecionada">

						<div class="input-group-append">
							<span class="input-group-text" id="btn-setar-cor" @click="trocaCor()"><i class="fas fa-check"></i></span>
						</div>
					</div>

				</div>
			</div>        
		</modal-component> -->
	</div>
	
	
</template>

<?php // $this->load->view('dpp/componentes/modal.php'); ?>
<?php $this->load->view('template_adminlte3/script_leaflet'); ?>

<script>
var mapaConflitoFaixa = Vue.component('mapa-conflito-faixa',{
    template: '#template-mapa-conflito-faixa',
	data() {
		return {
			
			params: {
				uf: '',
				br: '', 
				kminicial: '', 
				kmfinal: '',
				CodigoConflito: '',
				CodigoFaixaDominio: '',
				CodigoImovel: '',
			},

			mymap: null,
			
			layerEstados: {},
			layerMunicipios: {},
			
			showImagemSatelite: false,
			layerSNV: {},
			showLayerSNV: false,

			layerAlterarCor: '',
			botaoAlteraCor: '',
			carregando: null, 

			imoveis: null,
			showLayerConflito: true,
			layerConflito: {},

			showLayerFaixa: true,
			layerFaixa: {},

			showLayerImovel: true,
			layerImovel:{},

			listaImovel: null,
			listaFaixa: null,
			listaConflito: null,
		}
	},
	computed: {
		isMobile: function(){
			var check = false;
			(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
			return check;
		},
	},
	watch:{
		showImagemSatelite: function(val){
			leafletVue.changeViewSatelite(val);
		},
		showLayerSNV: function(val){
			this.layerSNV = leafletVue.plotLayerSNV(this.params, val, '#38f404');				
		},
		showLayerConflito: async function(val){
			await this.plotLayerConflito();
		},
		showLayerFaixa: async function(val){
			await this.plotLayerFaixa();
		},
		showLayerImovel: async function(val){
			await this.plotLayerImovel();
		},
	},
	async mounted(){
		
		await this.gerarMapa('mapid');
		
		$('.modal-dialog').draggable({" handle": ".modal-header"});
		
		$('.my-colorpicker2').colorpicker();
	},
    methods: {
		// Gerador de mapa
		async gerarMapa(idMapa) {
			
			this.mymap = await leafletVue.gerarMapa(idMapa, true);
			
			$('#polyline-measure-control').parent().css('top','230px');

			// cria os layers necessarios
			this.layerMunicipios = L.geoJSON().addTo(this.mymap);
			this.layerEstados = L.geoJSON().addTo(this.mymap);
			this.layerSNV = L.geoJSON().addTo(this.mymap);

			this.layerConflito = L.geoJSON().addTo(this.mymap);
			this.layerFaixa = L.geoJSON().addTo(this.mymap);
			this.layerImovel = L.geoJSON().addTo(this.mymap);
			
			await this.consultaLayers();
		},
		// recupera dados e monta layer selecionados
		async consultaLayers() {
			
			var controller = 'Municipio/getCoordsLayerEstados';
			var params = {};
			var data = await vmGlobal.getFromController(controller, params);

			this.layerEstados.clearLayers()

			for (var i = 0; i < data.length; i++) {
				var cord = data[i];

				// this.layerEstados.addData(JSON.parse(cord.coordenada)).addTo(this.mymap).setStyle(myStyle1)
				
				if(cord.UF == this.params.uf || this.params.uf == '')
					this.addLayerEstados(cord.coordenada, cord.UF, .0, this.layerEstados, 'black')
				else 
					this.addLayerEstados(cord.coordenada, cord.UF, .1, this.layerEstados, 'trasparente')

			}
			
			// adiciona os dados dos layers
			this.layerSNV = leafletVue.plotLayerSNV(this.params, this.showLayerSNV, '#38f404');
			
			await this.plotLayerFaixa();
			await this.plotLayerImovel();
			await this.plotLayerConflito();
		},
		// monta layer dos estados
		addLayerEstados(cord, uf, fillOpacity, layer, color) {
			
			var array = {
				"type": "FeatureCollection",
				"features": [{
					"type": "Feature",
					"geometry": JSON.parse(cord),
					"properties": {
						"OBJECTID": uf,
						"DADOS" : 'TEste'
					}
				}]
			}

			L.geoJson(array, {
				style: function (feature) {
					return {
						stroke: true,
						color: color,
						weight: 1,
						fillOpacity: fillOpacity,
					};
				},
				/**
				 * todo -> precisa emitir UF para filtro
				 */
				onEachFeature: (feature, l) => {
					var vm = this;
					l.on('contextmenu', function (e) {
						vm.$emit('emituf', feature.properties.OBJECTID);
					});
					l.on("dblclick", function (e) {
						vm.$emit('emituf', '');							
					});
				}
			}).addTo(layer)
			
		},		
		formatNumeric(value, tofix) {
			let val = (value / 1).toFixed(tofix)
			let val2 = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
			return val2.toString().replace(".", ",")
		},
		async plotLayerConflito(){

			this.layerConflito.clearLayers();
			
			if(this.showLayerConflito){	
				
				var controller = 'GestaoConflitosFaixa/getFaixaConflitos';
				var params = {CodigoConflito: this.params.CodigoConflito};
				var data = await vmGlobal.getFromController(controller, params);

				this.listaConflito = data; 

				for (i = 0; i < data.length; i++) {
					var extensaoConflito = this.formatNumeric(data[i].ExtensaoConflito, 1);
					var areaConflitoKm2 = this.formatNumeric(data[i].AreaConflitoKm2, 2);
					var codigoConflito = data[i].CodigoConflito
					var codigoFaixaDominio = data[i].CodigoFaixaDominio
					var concatenado = '<div><strong>INFORMAÇÕES DO CONFLITO</strong> <br>' +
						'<br> <strong>Código conflito:</strong> ' + data[i].CodigoConflito +
						'<br> <strong>Extensão aproximada do conflito:</strong> ' + extensaoConflito + ' km' +
						'<br> <strong>Área do conflito:</strong> ' + areaConflitoKm2 + ' km²' +
						'<br> <strong>Situação do conflito:</strong> ' + data[i].StatusConflito +
						'<br> <strong>Fonte:</strong> ' + data[i].Fonte +
						"<br><br><br> <button type='button' onclick='pointMap(" + codigoConflito + "," + codigoFaixaDominio + ")' class='btn btn-block btn-outline-danger btn-sm'><i class='fas fa-filter'></i> Filtrar este conflito</button>" +
						'</div>';
					leafletVue.addLayer(data[i].Coordenada, concatenado, 'red', this.layerConflito);
				}
				this.mymap.setView(this.layerConflito.getBounds().getCenter())
				this.mymap.fitBounds(this.layerConflito.getBounds())
			}	
		},
		async plotLayerFaixa(){
			
			this.layerFaixa.clearLayers();
			
			if(this.showLayerFaixa){	
				
				var controller = 'GestaoConflitosFaixa/getFaixaDominioOperacional';
				var params = {CodigoFaixaDominio: this.params.CodigoFaixaDominio};
				var data = await vmGlobal.getFromController(controller, params);

				this.listaFaixa = data;
				
				for (i = 0; i < data.length; i++) {
					var concatenado = '<div><strong>INFORMAÇÕES DA FAIXA DE DOMÍNIO</strong> <br>' +
						'<br> <strong>BR-' + data[i].FaixaDominioBR + '/' + data[i].UF + '</strong>' +
						'<br> <strong>Trecho:</strong> ' + data[i].KmInicial + ' ao ' + data[i].KmFinal +
						'<br> <strong>Extensão:</strong> ' + data[i].ExtensaoKm + ' km <br>' +
						'<br> <strong>Largura da faixa lado direito:</strong> ' + data[i].FaixaDominioLadoDireito + ' m' +
						'<br> <strong>Largura da faixa lado esquerdo:</strong> ' + data[i].FaixaDominioLadoEsquerdo + ' m' +
						'<br> <strong>Largura da faixa:</strong> ' + data[i].FaixaDominioLargura + ' m <br>' +
						'<br> <strong>Checagem:</strong> ' + data[i].Checagem +
						'<br> <strong>Fonte:</strong> ' + data[i].Fonte +
						'</div>';
					leafletVue.addLayer(data[i].Coordenada, concatenado, '#0000FF', this.layerFaixa);
				}
				this.mymap.setView(this.layerFaixa.getBounds().getCenter())
				this.mymap.fitBounds(this.layerFaixa.getBounds())
			}
		},
		async plotLayerImovel(){
			this.layerImovel.clearLayers();
			
			if(this.showLayerImovel){	
				
				var controller = 'GestaoConflitosFaixa/getFaixaImoveis';
				var params = {CodigoConflito: this.params.CodigoConflito};
				var data = await vmGlobal.getFromController(controller, params);
				
				this.listaImovel = data;
				this.params.CodigoImovel = data[0].CodigoConflito;
				
				for (i = 0; i < data.length; i++) {
					var concatenado = '<div><strong>INFORMAÇÕES DO IMÓVEL</strong> <br>' +
						'<br> <strong>Codigo imóvel:</strong> ' + data[i].CodigoEmpreendimento +
						'<br> <strong>Área do imóvel:</strong> ' + this.formatNumeric(data[i].AreaKm2, 2) + ' km²' +
						'<br> <strong>Perimêtro do imóvel:</strong> ' + this.formatNumeric(data[i].PerimetroKm, 2) + ' km' +
						'<br> <strong>Fonte:</strong> ' + data[i].Fonte +
						"<br><br><br> <button type='button' onclick='abrirDadosImovel(" + JSON.stringify(data[i].Fonte) + "," + data[i].CodigoTabelaFonte + ")' class='btn btn-block btn-outline-success btn-sm'><i class='fas fa-home'></i> Detalhes Imóvel</button>" +
						'</div>';
					leafletVue.addLayer(data[i].Coordenada, concatenado, 'green', this.layerImovel);
				}
				this.mymap.setView(this.layerImovel.getBounds().getCenter())
				this.mymap.fitBounds(this.layerImovel.getBounds())
			}
		},
		
    }
});
</script>


<style scoped>
	.map-height {
        width: 100%;
        height: 700px;
        z-index: 1
    }
	
	.btnMapa {
		position: absolute;
		left: 10px;
		padding: 8px;
		text-align: center;
		z-index: 1000;
		width: 34px;
		background-color: #FFFF;
		border-radius: 4px;
		cursor: pointer;
	}
	.btnHome {
		top: 85px;
		left: 10px;
	}
	
	.baseMapa {
		top: 121px;
		left: 10px;
		height: 35px;
	}

	.baseRodovia {
		font-size: 5pt;
		top: 155px;
		left: 10px;
		height: 33px;

	}

    .baseConflito {
        top: 197px;
    }

    .baseFaixa {
        top: 233px;
    }

    .baseImovel {
        top: 269px;
    }

	
    .buscaM {
        position: absolute;
        top: 4px;
        left: 60px;
        padding: 8px;
        text-align: center;
        z-index: 1000;
    }

    .buscaM input {
        width: 350px;
        height: 30px;
        border-radius: 4px;
        z-index: 1000;
    }

    .buscaM .btnBuscarM {
        width: 40px;
        height: 30px;
        border-radius: 4px;
        z-index: 1000;
    }

	
    .buscaM .btnClearBuscarM {
        width: 40px;
        height: 30px;
        border-radius: 4px;
        z-index: 1000;
    }


</style>

<link rel="stylesheet" href="<?= base_url('template_lte3/plugins/leaflet/leaflet.css') ?>">
<link rel="stylesheet" href="<?= base_url('template_lte3/plugins/leaflet/draw/src/leaflet.draw.css') ?>">
<link rel="stylesheet" href="<?= base_url('template_lte3/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') ?>">
