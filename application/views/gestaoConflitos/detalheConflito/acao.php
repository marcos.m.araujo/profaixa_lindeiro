<div class="card-body p-0" style="margin-top: 10px;">
	<div class="form-group">
		<label>Ação</label>
		<select v-model="acaoModel.Acao" class="form-control">
			<option v-for="i in listaAcao" value="{{i.CodigoAcao}}">{{i.NomeAcao}}</option>
		</select>
	</div>
	<div class="form-group">
		<label for="exampleInputEmail1">Tramitar para</label>
		<input type="email" v-model="acaoModel.TramitadoPara" class="form-control"  placeholder="Email">
	</div>

	<div class="form-group">
		<label>Observações</label>
		<textarea v-model="acaoModel.ObservacaoTramitacao" class="form-control" rows="10" placeholder="Escreva uma observação ..."></textarea>
	</div>
	<div class="card-footer">
		<button type="button" @click="salvarAcao" class="btn btn-info float-right">Salvar</button>
	</div>
</div>
