<div class="card-header">
	<div class="mailbox-controls">
		<button @click="dadosConflitoDetalhe = true, detalheAcao = false, timeLineDetalhe = false" type="button" :class="[dadosConflitoDetalhe ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']"><i class="fab fa-confluence"></i> Dados do conflito</button>
		<button @click="detalheAcao = true, dadosConflitoDetalhe = false, timeLineDetalhe = false" type="button" :class="[detalheAcao ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']"><i class="fas fa-cogs"></i> Ações</button>
		<button @click="timeLineDetalhe = true, detalheAcao = false, dadosConflitoDetalhe = false " type="button" :class="[timeLineDetalhe ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']"><i class="fas fa-history"></i> Histórico</button>
	</div>
</div>
<div style="background-color: #F7F7F7!important;" class="card-body ">
	<div v-show="dadosConflitoDetalhe">
		<?php $this->load->view("gestaoConflitos/detalheConflito/dados") ?>
	</div>
	<div v-show="detalheAcao">
		<?php $this->load->view("gestaoConflitos/detalheConflito/acao") ?>
	</div>

	<div class="timelines" v-show="timeLineDetalhe">	
		<?php $this->load->view("gestaoConflitos/detalheConflito/timeline") ?>
	</div>
</div>
