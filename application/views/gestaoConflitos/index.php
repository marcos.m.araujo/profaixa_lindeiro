<div id="conflitosAbertos">
	<div id="preloader">
		<div class="inner">
			<img id="spinner" width="50px" src="http://localhost:8080/dpp/theme/spinner2.gif">
		</div>
	</div>
   <gestao-conflito-faixa
		ref="gestaoConflitosAbertos"
		tipo="Conflito Aberto"
   >
   </gestao-conflito-faixa>
</div>

<?php $this->load->view("/componentes/mapa_conflito_faixa") ?>
<?php $this->load->view("/componentes/mapaFaixa_from_kml") ?>
<?php $this->load->view("/componentes/gestao_conflito_faixa") ?>

<script>

var vmFiltros = new Vue({
    el: '#vmFiltros',
    data(){
        return {
            params: {
                uf: '',
                br: '',
				municipio: '',
            },
            listaUF: null,
            listaBR: null,
            listaMunicipio: null,            
        }
    },
    async mounted(){
        await this.listarUF();
        await this.listarBR();
    },	
	watch:{
		'params.uf': function(val){
			this.listarBR();
			this.listarMunicipio();
			this.consulta();
		},
		'params.br': function(val){
			this.listarUF();
			this.listarMunicipio();
			this.consulta();
		},
		'params.Municipio': function(val){
			this.consulta();
		},
	},
    methods: {
		
        async consulta(){
            vmConflitosAbertos.$refs.gestaoConflitosAbertos.params = this.params;
            await vmConflitosAbertos.$refs.gestaoConflitosAbertos.reloadMapa('','');            
        },
        async listarUF(){
			var params = {
				columns: 'DISTINCT (UF)',
				table: 'ufRodovia',
			};
			params.whereCast = [{index: 'uf!=', value: "''"}];
			
			if(this.params.br != '')
				params.where = {br: this.br};
			

			var data = await vmGlobal.getFromAPI(params, 'cgdr');
			this.listaUF = data;
            
        },
        async listarBR(){
			
			var params = {
					columns: 'DISTINCT (BR)',
					table: 'ufRodovia',
				};
				if(this.params.uf != '')
					params.where = {UF: this.params.uf};

			var data = await vmGlobal.getFromAPI(params, 'cgdr');
            this.listaBR = data;
        },
        async listarMunicipio(){
            var params = {
				columns: 'DISTINCT (Municipio)',
				table: 'ufRodovia',
			};
			if(this.params.uf != '')
				params.where = {UF: this.params.uf};
			
			if(this.params.br != '')
				params.where = {BR: this.params.br};

			var data = await vmGlobal.getFromAPI(params, 'cgdr');
			this.listaMunicipio = data;
        },
        limpar(){
            window.location.reload()
		}, 
    },
});


var vmConflitosAbertos= new Vue({
	el: "#conflitosAbertos",
	data() {
	},
	methods: {
		pointMap(a, b) {
			this.$refs.gestaoConflitosAbertos.pointMap(a,b);
		},
		abrirDadosImovel(a, b) {
			this.$refs.gestaoConflitosAbertos.abrirDadosImovel(a, b)
		},
		filtrarFaixa(faixa, BR, UF){

			this.$refs.gestaoConflitosAbertos.$refs.mapaConflito.publicacao.CodigoFaixaDominio = faixa;
			
			this.$refs.gestaoConflitosAbertos.$refs.mapaConflito.publicacao.UF = UF;
			this.$refs.gestaoConflitosAbertos.$refs.mapaConflito.publicacao.BR = BR;

			this.$refs.gestaoConflitosAbertos.showDetalheConflito = false;
			this.$refs.gestaoConflitosAbertos.showDetalheImovel = false;

			this.$refs.gestaoConflitosAbertos.reloadMapa(null, faixa);
		},
		getKmlFaixa(cod){
			this.$refs.gestaoConflitosAbertos.getKmlFaixa(cod);
		},
		getKmlSegmento(cod){
			this.$refs.gestaoConflitosAbertos.getKmlSegmento(cod);
		},
		reloadLayersSugestoes(){
			this.$refs.gestaoConflitosAbertos.getListaFaixasPublicacao();
			this.$refs.gestaoConflitosAbertos.$refs.mapaConflito.plotLayerPublicar();
			this.$refs.gestaoConflitosAbertos.$refs.mapaConflito.plotLayerSugestaoFaixaAprovada();
			this.$refs.gestaoConflitosAbertos.$refs.mapaConflito.plotLayerSugestaoFaixaHomolog();
		},		
		visualizarFaixaAPublicar(cod, kml){
			this.$refs.gestaoConflitosAbertos.CodigoFaixaApublicar = cod;
			this.$refs.gestaoConflitosAbertos.KmlTrecho = kml;
			this.$refs.gestaoConflitosAbertos.existeArquivoFaixa = true;


		},
		showLoading(){
			$('#preloader .inner').fadeOut();
			$('#preloader').delay(350).fadeOut('slow');
			$('body').delay(350).css({
				'overflow': 'visible'
			});
		},
		closeLoading(){
			$('#preloader').hide();
		},
	}
});

async function pointMap(a, b) {
	vmConflitosAbertos.pointMap(a,b);
}

function abrirDadosImovel(a, b) {
	vmConflitosAbertos.abrirDadosImovel(a, b)
}

function selecionarFaixaParaPublicacao(faixa, br, uf){
	
	$('.leaflet-draw').show();
	vmConflitosAbertos.filtrarFaixa(faixa, br, uf);
}

async function enviarPublicacaoFaixa(e){

	e.preventDefault();
	
	vmGlobal.showLoading();

	const acaoModel = {};
	acaoModel.TramitadoPor = localStorage.getItem('email');
	acaoModel.Status = 'A Publicar'; // enviar publicacao para aprovacao
	acaoModel.Publicacao = vmConflitosAbertos.$refs.gestaoConflitosAbertos.$refs.mapaConflito.publicacao;
	acaoModel.Observacao =  $('#obsPublicarFaixa').val();

	const controller = 'GestaoConflitosFaixa/enviaPublicacaoAprovacao';
	const response = await vmGlobal.getFromController(controller, acaoModel);
	
	if(response.status == true){	
		showMessage('success', 'Salvo!', 'Trecho enviado para análise');
		vmConflitosAbertos.$refs.gestaoConflitosAbertos.reloadMapa('','');	
	}

	$('leaflet-popup').hide();

	vmConflitosAbertos.reloadLayersSugestoes();

	vmGlobal.closeLoading();

}

function getKmlFaixa(cod){
	vmConflitosAbertos.getKmlFaixa(cod);
}

function getKmlSegmento(cod){
	vmConflitosAbertos.getKmlSegmento(cod);
}

async function aprovarSugestaoPublicacao(cod){

	vmGlobal.showLoading();
	
	const acaoModel = {};
	acaoModel.TramitadoPor = localStorage.getItem('email');
	acaoModel.Status = 'Sugestão Aprovada'; // aprova sugestao
	acaoModel.CodigoFaixaApublicar = cod;

	const controller = 'GestaoConflitosFaixa/aprovaPublicacao';
	const response = await vmGlobal.getFromController(controller, acaoModel);
	
	if(response.status == true)
		vmGlobal.showMessage('success', 'Salvo!','Sugestão aprovada com sucesso.');	
	else
		vmGlobal.showMessage('error', 'Erro!', "Erro ao inserir dados. "+e);					

	vmConflitosAbertos.reloadLayersSugestoes();
	vmGlobal.closeLoading();

}

async function deletaSugestaoPublicacao(cod){

	vmGlobal.showLoading();

	const params = {
		table: 'FaixasAcaoTramitada',
		where: {CodigoFaixaApublicar: cod}
	}

	vmGlobal.deleteFromAPI(params, 'CGDR', {
		tipo: 'success', 
		titulo:'Salvo', 
		texto: 'Sugestão de publicação deletada com sucesso.'
	});

	vmConflitosAbertos.reloadLayersSugestoes();
	vmGlobal.closeLoading();

}

async function enviaTrechoHomolog(e){

	e.preventDefault();
	
	vmConflitosAbertos.showLoading();

	const model = {
		CodigoFaixaApublicar: $('#CodigoFaixaApublicar').val(),
		Status: 'Sugestão em Homologação',
		TramitadoPara: $('#fxTramitadoPara').val(),
		ObservacaoTramitacao: $('#obsPublicarFaixa').val(),
		FilesRules: {
			allowed_types: 'kml|xml',
			upload_path: 'arquivos_dpp/ProFaixa/faixasPublicadas',
		},
	};
	
	var formData = new FormData();

	if(typeof $('#pubArquivoTrecho')[0].files != 'undefined'){
		model.NomeArquivoTrecho = $('#pubArquivoTrecho')[0].files[0].name;
		formData.append('ArquivoTrecho', $('#pubArquivoTrecho')[0].files[0]);
	} 

	if(typeof ('#pubArquivoFaixa')[0].files != 'undefined'){
		model.NomeArquivoFaixa = $('#pubArquivoFaixa')[0].files[0].name;
		formData.append('ArquivoFaixa', $('#pubArquivoFaixa')[0].files[0]);
	}

	formData.append('Dados', JSON.stringify(model));

	const controller = 'GestaoConflitosFaixa/enviaPublicacaoFaixaHomolog';

	await axios.post(base_url + controller, formData)
	.then((response) => {

		if(response.data.status == false)
			throw new Error(response.data.result);

		this.showMessage('success', 'Salvo!', 'Alteração executada com sucesso.');
		
		vmConflitosAbertos.reloadLayersSugestoes();
		vmConflitosAbertos.closeLoading();
	})
	.catch((e) => {
		console.log(e)
		this.showMessage('error', 'Erro!', "Erro ao atualizar dados. "+e);

	});
	
}

async function findEmail(val, id){
	if(val.length > 2){
		var params = {
			table: 'acessos',
			columns: 'DISTINCT (Email) as Email',
			like: {Email: val}
		};
		var data = await vmGlobal.getFromAPI(params);
		$(id).empty();
		for (var i = 0; i < data.length; i++) {
			var item = data[i];
			$(id).append('<option value="' + item.Email + '">' + item.Email +"</option>");
		}
	
	}
}

function showFormRecusa(){
	$('#formRecusa').show();
}

async function recusaHomologacao(e){
	
	e.preventDefault();

	vmGlobal.showLoading();
	
	const model = {};
	model.TramitadoPor = localStorage.getItem('email');
	model.Status = 'Sugestão Aprovada'; // aprova sugestao
	model.TramitadoPara = $('#fxHomTramitadoPara').val();
	model.ObservacaoTramitacao = $('#obsHomologPublicarFaixa').val();
	model.CodigoFaixaApublicar = $('#CodigoFaixaApublicarHom').val();

	const controller = 'GestaoConflitosFaixa/aprovaPublicacao';
	const response = await vmGlobal.getFromController(controller, model);
	
	if(response.status == true)
		vmGlobal.showMessage('success', 'Salvo!','Sugestão aprovada com sucesso.');	
	else
		vmGlobal.showMessage('error', 'Erro!', "Erro ao inserir dados. "+e);					

	vmConflitosAbertos.reloadLayersSugestoes();
	vmGlobal.closeLoading();

}

async function aprovaPublicacao(cod){

	vmGlobal.showLoading();
	
	const model = {};
	model.TramitadoPor = localStorage.getItem('email');
	model.Status = 'Sugestão Homologada'; // aprova sugestao
	model.CodigoFaixaApublicar = cod;

	const controller = 'GestaoConflitosFaixa/aprovaPublicacao';
	const response = await vmGlobal.getFromController(controller, model);
	
	if(response.status == true)
		vmGlobal.showMessage('success', 'Salvo!','Sugestão aprovada com sucesso.');	
	else
		vmGlobal.showMessage('error', 'Erro!', "Erro ao inserir dados. ");					

	vmConflitosAbertos.reloadLayersSugestoes();

	vmGlobal.closeLoading();

	location.reload();

}

function visualizarHomologFaixa(cod, arquivo){

	vmConflitosAbertos.visualizarFaixaAPublicar(cod, arquivo);
}
</script>
