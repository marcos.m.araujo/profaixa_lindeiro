<script>
    var base_url = '<?= base_url() ?>';
    conflitos = new Vue({
        el: "#conflitos",
        data() {
            return {
                tabela: [],
                uf: '',
                br: '',
                mymap: null,
                layerfaixadominio: null,
                layerEstados: null,
                layerSNV: null,
                estados: [],
                snv: [],
                faixadominio: [],
                conflitos: [],
                layerConflitos: null,
                imoveis: [],
                detalheImovel: [],
                layerImoveis: null,
                telaGrafico: false,
                telaTabela: false,
                telaMapa: true,
                codigoConflito: '',
                codigoFaixaDominio: '',
                dadosConflitoDetalhe: true,
                dadosDetalheEixo: false,
                timeLineDetalhe: false,
                indentificadorTipoMapa: 0,
                indentificadorImovies: 0,
                indentificadorFaixa: 0,
                indentificadorConflitos: 0,
                indentificadorSNV: 1,


            }
        },
        async mounted() {
            $(".leaflet-control").css("display", "none");
            this.gerarMapa()
            this.layerEstados = L.geoJSON().addTo(this.mymap)
            this.layerfaixadominio = L.geoJSON().addTo(this.mymap);
            this.layerConflitos = L.geoJSON().addTo(this.mymap);
            this.layerImoveis = L.geoJSON().addTo(this.mymap);
            this.layerSNV = L.geoJSON().addTo(this.mymap);
            // await this.getEstados()
            await this.getFaixaDominioOperacional()
            await this.getFaixaImoveis()
            await this.getFaixaConflitos()
        },
        watch: {
            telaTabela() {
                setTimeout(() => {
                    personalizarDataTable("#tables")
                }, 200);
            },
        },
        methods: {
            gerarMapa() {
                this.mymap = L.map('mapid').setView(['-10.6007767', '-63.6037797'], 4.5);
                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 18,
                    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
                    id: 'mapbox.streets'
                }).addTo(this.mymap);
                L.control.scale({
                    maxWidth: 240,
                    metric: true,
                    imperial: false,
                    position: 'bottomleft'
                }).addTo(this.mymap);
                L.control.polylineMeasure({
                    position: 'topleft',
                    unit: 'metres',
                    showBearings: true,
                    clearMeasurementsOnStop: true,
                    showClearControl: true,
                    showUnitControl: true
                }).addTo(this.mymap);
            },
            mudaVisualizacaoMapa() {
                var valor = this.indentificadorTipoMapa++;
                if (valor & 1) {
                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                        maxZoom: 18,
                        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
                        id: 'mapbox.streets'
                    }).addTo(this.mymap);
                    $('.baseMapa').css('background-color', '#FFFF')

                } else {
                    L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                        maxZoom: 20,
                        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
                    }).addTo(this.mymap);
                    $('.baseMapa').css('background-color', '#218838')
                }
            },

            layerFaixa() {
                var valor = this.indentificadorFaixa++;
                if (valor & 1)
                    this.getFaixaDominioOperacional()
                else
                    this.layerfaixadominio.clearLayers();

            },
            layerImovel() {
                var valor = this.indentificadorImovies++;
                if (valor & 1)
                    this.getFaixaImoveis()
                else
                    this.layerImoveis.clearLayers();

            },
            layerConflito() {
                var valor = this.indentificadorConflitos++;
                if (valor & 1)
                    this.getFaixaConflitos()
                else
                    this.layerConflitos.clearLayers();

            },
            async getFaixaDominioOperacional() {
                var form = new FormData()
                form.append("CodigoFaixaDominio", this.codigoFaixaDominio);
                await axios.post('<?= base_url('GestaoConflitosFaixa/getFaixaDominioOperacional') ?>', form)
                    .then((resp) => {
                        this.faixadominio = resp.data
                    })
                    .catch((e) => {
                        console.log(e)
                    })
                    .finally(() => {
                        this.layerfaixadominio.clearLayers();
                        for (i = 0; i < this.faixadominio.length; i++) {
                            var concatenado = '<div><strong>INFORMAÇÕES DA FAIXA DE DOMÍNIO</strong> <br>' +
                                '<br> <strong>BR-' + this.faixadominio[i].FaixaDominioBR + '/' + this.faixadominio[i].UF + '</strong>' +
                                '<br> <strong>Trecho:</strong> ' + this.faixadominio[i].KmInicial + ' ao ' + this.faixadominio[i].KmFinal +
                                '<br> <strong>Extensão:</strong> ' + this.faixadominio[i].ExtensaoKm + ' km <br>' +
                                '<br> <strong>Largura da faixa lado direito:</strong> ' + this.faixadominio[i].FaixaDominioLadoDireito + ' m' +
                                '<br> <strong>Largura da faixa lado esquerdo:</strong> ' + this.faixadominio[i].FaixaDominioLadoEsquerdo + ' m' +
                                '<br> <strong>Largura da faixa:</strong> ' + this.faixadominio[i].FaixaDominioLargura + ' m <br>' +
                                '<br> <strong>Checagem:</strong> ' + this.faixadominio[i].Checagem +
                                '<br> <strong>Fonte:</strong> ' + this.faixadominio[i].Fonte +
                                '</div>';
                            this.addLayer(this.faixadominio[i].Coordenada, concatenado, '#0000FF', this.layerfaixadominio);
                        }
                        return true
                    })
            },

            // Adicionar layer line geoJoson
            addLayer(coord, objectid, color, layer) {
                var array = {
                    "type": "FeatureCollection",
                    "features": [{
                        "type": "Feature",
                        "geometry": JSON.parse(coord),
                        "properties": {
                            "OBJECTID": objectid,
                        }
                    }]
                }
                L.geoJson(array, {
                    style: function(feature) {
                        return {
                            stroke: true,
                            color: color,
                            weight: 5
                        };
                    },
                    onEachFeature: function(feature, l) {
                        l.bindPopup(feature.properties.OBJECTID);
                        l.on('click', function(e) {
                            l.setStyle({
                                weight: 12,
                                outline: 'red'
                            });
                        });

                        l.on("popupclose", function(e) {
                            l.setStyle({
                                weight: 5,
                            });
                        });
                    }
                }).addTo(layer)
            },
            async getFaixaConflitos() {
                var form = new FormData()
                form.append("CodigoConflito", this.codigoConflito);
                await axios.post('<?= base_url('GestaoConflitosFaixa/getFaixaConflitos') ?>', form)
                    .then((resp) => {
                        this.conflitos = resp.data
                    })
                    .catch((e) => {
                        console.log(e)
                    })
                    .finally(() => {
                        this.layerConflitos.clearLayers();
                        for (i = 0; i < this.conflitos.length; i++) {
                            var extensaoConflito = this.formatNumeric(this.conflitos[i].ExtensaoConflito, 1);
                            var areaConflitoKm2 = this.formatNumeric(this.conflitos[i].AreaConflitoKm2, 2);
                            var codigoConflito = this.conflitos[i].CodigoConflito
                            var codigoFaixaDominio = this.conflitos[i].CodigoFaixaDominio
                            var concatenado = '<div><strong>INFORMAÇÕES DO CONFLITO</strong> <br>' +
                                '<br> <strong>Código conflito:</strong> ' + this.conflitos[i].CodigoConflito +
                                '<br> <strong>Extensão aproximada do conflito:</strong> ' + extensaoConflito + ' km' +
                                '<br> <strong>Área do conflito:</strong> ' + areaConflitoKm2 + ' km²' +
                                '<br> <strong>Situação do conflito:</strong> ' + this.conflitos[i].StatusConflito +
                                '<br> <strong>Fonte:</strong> ' + this.conflitos[i].Fonte +
                                "<br><br><br> <button type='button' onclick='pointMap(" + codigoConflito + "," + codigoFaixaDominio + ")' class='btn btn-block btn-outline-danger btn-sm'><i class='fas fa-filter'></i> Filtrar este conflito</button>" +
                                '</div>';
                            this.addLayer(this.conflitos[i].Coordenada, concatenado, 'red', this.layerConflitos);
                        }
                        this.mymap.setView(this.layerConflitos.getBounds().getCenter())
                        this.mymap.fitBounds(this.layerConflitos.getBounds())
                        return true
                    })

            },
            async getFaixaImoveis() {
                var form = new FormData()
                form.append("CodigoConflito", this.codigoConflito);
                await axios.post('<?= base_url('GestaoConflitosFaixa/getFaixaImoveis') ?>', form)
                    .then((resp) => {
                        this.imoveis = resp.data
                    })
                    .catch((e) => {
                        console.log(e)
                    })
                    .finally(() => {
                        this.layerImoveis.clearLayers();
                        for (i = 0; i < this.imoveis.length; i++) {
                            var concatenado = '<div><strong>INFORMAÇÕES DO IMÓVEL</strong> <br>' +
                                '<br> <strong>Codigo imóvel:</strong> ' + this.imoveis[i].CodigoEmpreendimento +
                                '<br> <strong>Área do imóvel:</strong> ' + this.formatNumeric(this.imoveis[i].AreaKm2, 2) + ' km²' +
                                '<br> <strong>Perimêtro do imóvel:</strong> ' + this.formatNumeric(this.imoveis[i].PerimetroKm, 2) + ' km' +
                                '<br> <strong>Fonte:</strong> ' + this.imoveis[i].Fonte +
                                "<br><br><br> <button type='button' onclick='abrirDadosImovel(" + JSON.stringify(this.imoveis[i].Fonte) + "," + this.imoveis[i].CodigoTabelaFonte + ")' class='btn btn-block btn-outline-success btn-sm'><i class='fas fa-home'></i> Detalhes Imóvel</button>" +
                                '</div>';
                            this.addLayer(this.imoveis[i].Coordenada, concatenado, 'green', this.layerImoveis);
                        }
                        return true
                    })
            },

            async getEstados() {
                await axios.get('<?= base_url('ProjecaoOrcamentaria/getCoordsLayerEstados') ?>')
                    .then((resp) => {
                        this.estados = resp.data
                    })
                    .catch((e) => {
                        console.log(e)
                    })
                    .finally(() => {
                        this.layerEstados.clearLayers();
                        for (i = 0; i < this.estados.length; i++) {
                            if (this.estados[i].UF == this.uf || this.uf == '') {
                                this.addLayerEstados(this.estados[i].coordenada, this.estados[i].UF, .0, this.layerEstados, 'black')
                            } else {
                                this.addLayerEstados(this.estados[i].coordenada, this.estados[i].UF, .1, this.layerEstados, 'trasparente')
                            }
                        }
                        return true;
                    })
            },

            abrirDadosImovel(fonte, codigo) {
                var form = new FormData()
                form.append("Fonte", fonte);
                form.append("Codigo", codigo);
                axios.post('<?= base_url('GestaoConflitosFaixa/abrirDadosImovel') ?>', form)
                    .then((resp) => {
                        this.detalheImovel = resp.data
                        delete this.detalheImovel[0]['Coordenada']
                    })
                    .catch((e) => {
                        console.log(e)
                    })
                    .finally(() => {
                        $("#modalImovel").modal()
                    })
            },
            async pointMap(codigoConflito, codigoFaixaDominio) {
                this.telaTabela = false
                this.telaMapa = true
                this.telaGrafico = false
                this.codigoConflito = codigoConflito
                this.codigoFaixaDominio = codigoFaixaDominio
                await this.getFaixaDominioOperacional()
                await this.getFaixaImoveis()
                await this.getFaixaConflitos()
            },
            addLayerEstados(cord, uf, fillOpacity, layer, color) {
                var array = {
                    "type": "FeatureCollection",
                    "features": [{
                        "type": "Feature",
                        "geometry": JSON.parse(cord),
                        "properties": {
                            "OBJECTID": uf,
                            "DADOS": 'TEste'
                        }
                    }]
                }
                L.geoJson(array, {
                    style: function(feature) {
                        return {
                            stroke: true,
                            color: color,
                            weight: 1,
                            fillOpacity: fillOpacity,
                        };
                    },
                    onEachFeature: function(feature, l) {
                        l.on('contextmenu', function(e) {
                            conflitos.uf = feature.properties.OBJECTID
                            conflitos.getEstados();
                        });
                        l.on("dblclick", function(e) {
                            location.reload()
                        });
                    }
                }).addTo(layer)
            },
            formatData(param, tipo) {
                if (tipo === 1) {
                    return moment(String(param)).format('DD/MM/YYYY');
                } else {
                    return moment(String(param)).format('MM/YYYY');
                }
            },
            formatNumeric(value, tofix) {
                let val = (value / 1).toFixed(tofix)
                let val2 = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                return val2.toString().replace(".", ",")
            },
            formatJson(value) {
                let val = JSON.stringify(value);
                val = val.replace(/[ÀÁÂÃÄÅ]/g, "A");
                val = val.replace(/[àáâãäå]/g, "a");
                val = val.replace(/[ÈÉÊË]/g, "E");
                val = val.replace('[{', '');
                val = val.replace('}]', '');
                val = val.replace(/"/g, '');
                return val
            },


            layerGetRodovias() {
                var valor = this.indentificadorSNV++;
                if (valor & 1) {
                    this.getRodovias()
                    $('#baseRodovia').css('background-color', '#218838')
                    $('#baseRodovia').css('font-weight', 'bold')
                } else {
                    this.layerSNV.clearLayers()
                    $('#baseRodovia').css('background-color', '#fff')
                    $('#baseRodovia').css('font-weight', 'normal')
                }
            },

            async getRodovias() {
                var form = new FormData()
                form.append("uf", this.uf);
                form.append("br", this.br);
                await axios.post('<?= base_url('MalhaRodoviaria/getMalhaRodovia') ?>', form)
                    .then((resp) => {
                        this.snv = resp.data
                    })
                    .catch((e) => {
                        console.log(e)
                    })
                    .finally(() => {
                        this.layerSNV.clearLayers();
                        for (i = 0; i < this.snv.length; i++) {
                            if (this.snv[i].DescricaoSuperficie == 'PLA') {
                                this.addLayer(this.snv[i].Coordenada, this.snv[i].Concatenado, "#38f404", this.layerSNV, '3,20', 'round')
                            } else {
                                this.addLayer(this.snv[i].Coordenada, this.snv[i].Concatenado, "#38f404", this.layerSNV)
                            }
                        }
                        return true
                    })
            },
        },
    });

    function pointMap(a, b) {
        conflitos.pointMap(a, b)
    }

    function abrirDadosImovel(a, b) {
        conflitos.abrirDadosImovel(a, b)
    }
</script>