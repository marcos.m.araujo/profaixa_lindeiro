
<template id="template-gestao-conflito-faixa">
	<div v-cloak>
		<section class="content-header">
			<div class="container-fluid">
				<div class="row mt-2">
					<div class="col-sm-4">
						<h3>{{titulo}}</h3>
					</div>
					<div class="col-sm-5">
						<div class="mailbox-controls">
							<button @click="showTela('Mapa')" type="button" :class="[telaMapa ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']">
								<i class="fas fa-map"></i> Mapa
							</button>

							<button @click="location.reload()" type="button" class="btn btn-default btn-sm">
								<i class="fas fa-sync-alt"></i> 
								Limpar
							</button>

							<!-- <button @click="telaMapa = false, telaSalaSituacao = true, telaConflitos = false" type="button" :class="[telaSalaSituacao ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']"><i class="fas fa-chart-bar"></i> Sala de situação</button> -->
							<button @click="showTela('Conflitos')" type="button" class="ml-3" :class="[telaConflitos ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']">
								<i class="fas fa-table"></i> 
								Conflitos
							</button>
							
							<!-- <button @click="telaMapa = false, telaSalaSituacao = true, telaConflitos = false" type="button" :class="[telaSalaSituacao ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']"><i class="fas fa-chart-bar"></i> Sala de situação</button> -->
							<button v-if="tipo == 'Conflito Aberto'" @click="showTela('Faixas')" type="button" :class="[telaFaixas ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']">
								<i class="fas fa-table"></i> 
								Faixas para Publicação
							</button>
							
							<div class="float-right" v-if="tipo != 'Conflito Aberto'">
								<input type="checkbox" @click="tramitadoPraMim = !tramitadoPraMim"> Tramitados Pra Mim
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="<?=base_url('Home')?>">Home</a></li>
							<li class="breadcrumb-item active">{{titulo}}</li>
						</ol>
					</div>
				</div>
			</div>
		</section>

		<section class="content-body">
			<div class="row">
				<div :class="[params.CodigoConflito != '' && params.CodigoConflito != null ? 'col-md-9':'col-md-12']">
					<div class="card-body ">
						
						<!-- Mapa -->
						<div v-show="telaMapa" class="row" style="top:-50px;">

							<div :class="[(params.CodigoConflito != '' && existeArquivo ) || existeArquivoFaixa ? 'col-md-6':'col-md-12']">
								<h4 v-if="existeArquivo || existeArquivoFaixa">Dados Atuais</h4>
								<mapa-conflito-faixa ref="mapaConflito"
									:tipo="tipo"
									id="mapaConflito"
									@emituf="recebeuf"
									@emitloaded="componentMapaLoaded"
								></mapa-conflito-faixa>
							</div>

							<div v-if="existeArquivo" class='col-md-6'>
								<h3 v-if="existeArquivo">Proposta para Resolução</h3>
								<mapa-from-kml ref="mapaProposta"
									:conflito="listaConflito[0]"
								></mapa-from-kml>
							</div>
							
							<div v-if="existeArquivoFaixa" class='col-md-6'>
								<h3 v-if="existeArquivoFaixa">Proposta para Resolução</h3>
								<mapa-faixa-from-kml ref="mapaPropostaFaixa"
									:codigofaixaapublicar="CodigoFaixaApublicar"
									:kml="KmlTrecho"
								></mapa-faixa-from-kml>
							</div>
						</div>		

						<!-- Conflitos -->
						<div v-if="telaConflitos">
							<?php $this->load->view("componentes/tabela") ?>
						</div>

						<!-- Faixa -->
						<div v-if="telaFaixas">
							<?php $this->load->view("componentes/tabelaFaixas") ?>
						</div>
					</div>
				</div>
				<!-- Detalhe do conflito  -->
				<div class="card col-md-3" v-show="params.CodigoConflito != '' && params.CodigoConflito != null">
					<?php $this->load->view("componentes/detalheConflito/index") ?>
				</div>

				<?php $this->load->view("componentes/modalImovel") ?>
			</div>
		</section>
	</div>
</template>

<script>

var gestaoConflitoFaixa = Vue.component('gestao-conflito-faixa',{
    template: '#template-gestao-conflito-faixa',
	props: {
		tipo: String,
	},
	data() {
		return {
			mymap: null,
			tabela: [],

			params: {
				uf: '',
				br: '',
				Municipio: '',
				CodigoConflito: '',
				CodigoFaixaDominio: '',
			},

			tramitadosParaMim: {},

			listaImovel: null,
			listaFaixa: null,

			listaConflito: null,
			listaFaixasPublicacao: null,

			primeiraListaConflito: null,

			detalheImovel: [],
			acaoModel: {},
			listaAcao: null,
			historico: {},
			
			telaConflitos: false,
			telaMapa: true,
			telaFaixas: false,
	
			dadosConflitoDetalhe: true,
			timeLineDetalhe: false,
			detalheAcao: false,
			tramitadoPraMim: false,

			loading: true,

			existeArquivo: false,
			existeArquivoFaixa: false,

			CodigoFaixaApublicar: null, 
			KmlTrecho: null, 

			historicoTramitacao: [],

			showDetalheConflito: true,
			showDetalheImovel: true,
			showDetalheFaixa: true, 
		}
	},
	computed: {
		titulo: function(){
			switch(this.tipo){
				case 'Conflito Aberto':
					return 'Faixas de Domínio';
					break;
				case 'Em Andamento':
					return 'Conflitos em Análise';
					break;
				case 'Em Homologação':
					return 'Conflitos em Homologação';
					break;
			}
		},
	},
	watch: {
		telaConflitos() {
			this.listaConflito = this.$refs.mapaConflito.listaConflito
			this.refreshTabela("#tableConflitos");
		},
		telaFaixas() {
			
			this.refreshTabela("#tableFaixas", [1,2,6], [5]);
			
		},
		async tramitadoPraMim(val){
			if(val === true){
				await this.$refs.mapaConflito.filtrarTramitadoPraMim();

				this.listaImovel = this.$refs.mapaConflito.listaImovel
				this.listaFaixa = this.$refs.mapaConflito.listaFaixa
				this.listaConflito = this.$refs.mapaConflito.listaConflito

				if(this.telaConflitos === true)
					this.refreshTabela('#tableConflitos');
				
			} else {
				location.reload()
			}

		}
	},
	async created(){
		vmGlobal.showLoading();
	},
	async mounted(){
		vmGlobal.closeLoading();
	},
	methods: {
		showTela(selecao){
			switch(selecao){
				case 'Mapa':
					this.telaConflitos = false;
					this.telaFaixas = false;
					this.telaMapa = true;
					break;
				case 'Conflitos':
					this.telaMapa = false;
					this.telaFaixas = false;
					this.telaConflitos = true;
					break;
				case 'Faixas':
					this.telaMapa = false;
					this.telaConflitos = false;
					this.telaFaixas = true;
					break;
			}
		},
		componentMapaLoaded(){
			
			this.primeiraListaConflito = this.$refs.mapaConflito.listaConflito;

			this.listaConflito = this.$refs.mapaConflito.listaConflito
			this.listaImovel = this.$refs.mapaConflito.listaImovel
			this.listaFaixa = this.$refs.mapaConflito.listaFaixa
			this.refreshTabela('#tableConflitos')

			if(this.tipo !== 'Conflito Aberto'){
				this.tramitadosParaMim = this.$refs.mapaConflito.tramitadosParaMim
			}

			if(this.tipo === 'Em Andamento' || this.tipo === 'Em Homologação'){
				this.dadosConflitoDetalhe = false;
				
				this.telaConflitos = true;
				this.telaMapa = false;
			
				// detalhe mostra historico prieiro
				this.timeLineDetalhe = true;	
			}

			this.getListaFaixasPublicacao();
			this.existeArquivo = false;

			vmGlobal.closeLoading();
		},
		async getListaFaixasPublicacao(){

			const controller = 'GestaoConflitosFaixa/getListaFaixasPublicacao';
			
			const result = await vmGlobal.getFromController(controller, this.params);

			this.listaFaixasPublicacao = result;
			this.$refs.mapaConflito.listaFaixasPublicacao = result;

			this.$refs.mapaConflito.plotLayerPublicar();
			this.$refs.mapaConflito.plotLayerSugestaoFaixaAprovada();
			this.$refs.mapaConflito.plotLayerSugestaoFaixaHomolog();
		},
		async reloadMapa(codigoConflito,codigoFaixaDominio){

			this.existeArquivo = false;
			this.params.ConflitosIn = '';
			this.params.FaixasIn = '';

			this.params.CodigoConflito = codigoConflito;
			this.params.CodigoFaixaDominio = codigoFaixaDominio;

			this.$refs.mapaConflito.params = this.params

			await this.$refs.mapaConflito.consultaLayers();

			this.listaConflito = this.$refs.mapaConflito.listaConflito
			this.listaImovel = this.$refs.mapaConflito.listaImovel
			this.listaFaixa = this.$refs.mapaConflito.listaFaixa

			this.getListaFaixasPublicacao();

			this.refreshTabela('#tableConflitos')
			this.refreshTabela('#tableFaixas', [1,2,6], [5])
		},
		recebeuf(val){
			vmFiltros.params.uf = val;
		},
		async getDetalhesConflito(codigoConflito, codigoFaixaDominio){
			
			await this.reloadMapa(codigoConflito, codigoFaixaDominio);

			if(this.tipo !== 'Conflito Aberto')
			await this.getHistorico(codigoConflito);
			
			if(this.telaConflitos == true)
			this.refreshTabela('#tableConflitos')
			
			await this.getListaAcao();
			
			this.verificaSeExisteArquivo();
			
		},
		async fechaDetalhe(){
			$('.leaflet-draw').hide();

			await this.reloadMapa('','');
			this.existeArquivo = false;
		},
		async pointMap(codigoConflito, codigoFaixaDominio) {
			
			this.getDetalhesConflito(codigoConflito, codigoFaixaDominio);
		
			this.telaConflitos = false;
			this.telaFaixas = false;
			this.telaMapa = true;

		},
		async getHistorico(codigoConflito){

			var paramsHistorico = {
				table: 'ConflitosAcaoTramitada',
				join: {
					table: 'ConflitoAcao',
					on: 'CodigoAcao'
				},
				whereCast:[{
					index: 'tblProFaixaConflitosAcaoTramitada.CodigoConflito',
					value: codigoConflito
				}],
				orderBy: ['Data DESC']
			};
			var dataHistorico = await vmGlobal.getFromAPI(paramsHistorico, 'cgdr');
			this.historico = dataHistorico;

			this.historicoTramitacao =  dataHistorico.map(x=>x.TramitadoPor).filter(function(elem, index, self) {
				return index === self.indexOf(elem);
			});
		},
		async getListaAcao(){
			
			var params = {
				table: 'ConflitoAcao', 
				where: {StatusConflito: this.tipo}
			};
			var data = await vmGlobal.getFromAPI(params, 'cgdr');
			this.listaAcao = data;
		},
		async abrirDadosImovel(fonte, codigo) {

			var controller = 'GestaoConflitosFaixa/abrirDadosImovel';
			var params = {Fonte: fonte, Codigo: codigo};
			var data = await vmGlobal.getFromController(controller, params);

			this.detalheImovel = data;
			delete this.detalheImovel[0]['Coordenada']
			
			$("#modalImovel").modal()
		},
		async salvarAcao(){
			
			var verificaArquivo = await this.verificaArquivos();

			if(verificaArquivo) {

				this.acaoModel.TramitadoPor = localStorage.getItem('email');
				this.acaoModel.CodigoConflito = this.listaConflito[0].CodigoConflito;
				this.acaoModel.CodigoFaixa = this.listaFaixa[0].CodigoFaixaDominio;
				this.acaoModel.CodigoImovel = this.listaConflito[0].CodigoEmpreendimento;
				this.acaoModel.Data = moment(String(new Date())).format('YYYY-MM-DD HH:mm:ss');

				// se aprovar homologacao, salva historico com coordenadas, e atualiza coord
				if(this.acaoModel.CodigoAcao == 8){
					this.acaoModel.CoordenadaAntigaConflito = this.listaConflito[0].Coordenada;
					this.acaoModel.CoordenadaAntigaFaixa = this.listaFaixa[0].Coordenada;
					this.acaoModel.CoordenadaAntigaImovel = this.listaImovel[0].Coordenada;

					var controller = 'GestaoConflitosFaixa/atualizaCoordenadas';
					var params = {CodigoConflito: this.params.CodigoConflito};
					await vmGlobal.getFromController(controller, params);					
				}

				var files = this.setFiles();
				
				var paramsAcao = {
					table: 'ConflitosAcaoTramitada',
					data: this.acaoModel,
				}
				var response = await vmGlobal.insertFromAPI(paramsAcao, files, 'CGDR');			

				// atualiza status conflito
				if(response == true){

					var status = '';
					switch(this.acaoModel.CodigoAcao){
						case 7:
							status = 'Em Homologação';
						break;

						case 8:
							status = 'Concluído';
						break;
						
						default: 
							status = 'Em Andamento';
						break;
					}

					this.acaoModel = {};

					var paramConflito = {
						table: 'Conflitos',
						data: {StatusConflito: status},
						where: {CodigoConflito: this.params.CodigoConflito}
					}
					// console.log('paramConflito',paramConflito);
					var response2 = await vmGlobal.updateFromAPI(paramConflito, null, 'CGDR');

					if(response2 == true)
						this.reloadMapa('','');
					
				}
			}
		},
		async verificaArquivos(){
			if(this.$refs.ArquivoConflito){
				if(this.$refs.ArquivoConflito.files[0] != undefined || this.$refs.ArquivoFaixa.files[0] != undefined || this.$refs.ArquivoImovel.files[0] != undefined){

					this.acaoModel.FilesRules = {
						allowed_types: 'kml|xml',
						upload_path: 'arquivos_dpp/ProFaixa/conflitosAtualizados',
					};

					var msg ='';
					if(this.$refs.ArquivoConflito.files[0] != undefined){
						msg += '<b>Conflito</b>: ' + this.$refs.ArquivoConflito.files[0].name + '<br>';
					}

					if(this.$refs.ArquivoFaixa.files[0] != undefined){
						msg += '<b>Faixa</b>: ' + this.$refs.ArquivoFaixa.files[0].name + '<br>';
					}

					if(this.$refs.ArquivoImovel.files[0] != undefined){
						msg += '<b>Imovel</b>: ' + this.$refs.ArquivoImovel.files[0].name + '<br>';
					}

					return Swal.fire({
						title: '<h4>Você confirma os arquivos enviados?</h4>',
						html: msg,
						icon: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#1f7f35',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Sim',
						cancelButtonText: 'Não',
					}).then((result) => {
						return result.value;
					})
				} else {
					return true;
				}
			} else {
				return true;
			}
		},
		verificaSeExisteArquivo(){
			if(this.listaConflito[0].ArquivoConflito != null || this.listaConflito[0].ArquivoFaixa != null || this.listaConflito[0].ArquivoImovel != null){
				this.existeArquivo = true;
			}
		},
		setFiles(){
			var files = [];

			if(this.$refs.ArquivoConflito && this.$refs.ArquivoConflito.files[0] != undefined){
					files.push(
						this.setFileOnModel(this.$refs.ArquivoConflito, 'ArquivoConflito', 'NomeArquivoConflito')
					);
				}
				
				if(this.$refs.ArquivoFaixa && this.$refs.ArquivoFaixa.files[0] != undefined){
					files.push(
						this.setFileOnModel(this.$refs.ArquivoFaixa, 'ArquivoFaixa', 'NomeArquivoFaixa')
					);
				}

				if(this.$refs.ArquivoImovel && this.$refs.ArquivoImovel.files[0] != undefined){
					files.push(
						this.setFileOnModel(this.$refs.ArquivoImovel, 'ArquivoImovel', 'NomeArquivoImovel')
					);
				}
			return files;
		},
		setFileOnModel(inputFile, campoArquivo, campoNomeArquivo){
				
			this.acaoModel[campoNomeArquivo] = inputFile.files[0].name;
			
			return {
				name: campoArquivo,
				value: inputFile.files[0]
			}

		},
		formatNumeric(value, tofix) {
			let val = (value / 1).toFixed(tofix)
			let val2 = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
			return val2.toString().replace(".", ",")
		},
		async getEmails(){
			if (this.acaoModel.TramitadoPara && this.acaoModel.TramitadoPara.length > 2) {
				
				var params = {
					table: 'acessos',
					columns: 'DISTINCT (Email) as Email',
					like: {Email: this.acaoModel.TramitadoPara}
				};
				var data = await vmGlobal.getFromAPI(params);
				$('#listaEmails').empty();
				for (var i = 0; i < data.length; i++) {

					var item = data[i];
					
					$("#listaEmails").append('<option value="' + item.Email + '">' + item.Email +"</option>");
				}
			
			}
		},
		refreshTabela(id, filters, sum){
			$(id).DataTable().destroy();
			this.$nextTick(function() {
				vmGlobal.montaDatatable(id, filters, sum);
			});
		},
		async getKmlConflito(id){
			
			var controller = 'GestaoConflitosFaixa/getKmlConflito';
			var params = {CodigoConflito: id};
			var data = await vmGlobal.getFromController(controller, params);
			this.forceFileDownload(data, 'Conflito', id);
		},
		async getKmlImovel(id){
			
			var controller = 'GestaoConflitosFaixa/getKmlImovel';
			var params = {CodigoEmpreendimento: id};
			var data = await vmGlobal.getFromController(controller, params);
			this.forceFileDownload(data, 'Imovel', id);
		},
		async getKmlFaixa(id){
			
			var controller = 'GestaoConflitosFaixa/getKmlFaixa';
			var params = {CodigoFaixaDominio: id};
			var data = await vmGlobal.getFromController(controller, params);
			this.forceFileDownload(data, 'FaixaDominio', id);
		},
		async getKmlSegmento(id){
			
			var controller = 'GestaoConflitosFaixa/getKmlSegmento';
			var params = {CodigoFaixaApublicar: id};
			var data = await vmGlobal.getFromController(controller, params);
			this.forceFileDownload(data, 'SegmentoFaixaDominio', id);
		},
		forceFileDownload(data,tipo,id){
			const url = window.URL.createObjectURL(new Blob([data]))
			const link = document.createElement('a')
			link.href = url
			link.setAttribute('download', tipo + '_' + id +'.kml') //or any other extension
			document.body.appendChild(link)
			link.click()
		},
	},
});

</script>

<style scoped>
    .card-body {
        min-height: 750px;
    }

    .timelines {
        max-height: 700px;
        overflow: auto
    }

    .acao {
        color: red
    }

    .flex-column {
        border: 1px solid silver !important;
    }
	
	[v-cloak] {
		display: none;
	}

	.content-header {
		padding: 3px .5rem;
	}
	.card-body {
		padding: 0rem;
	}
	
	.leaflet-popup-content{
		z-index: 9999 !important;
	}
</style>
