<table id="tableConflitos" class="dataTables_wrapper dt-bootstrap4 no-footer" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Abrir mapa</th>
            <th>Conflito</th>
            <th>Km Inicial</th>
            <th>Km Final</th>
            <th>Extensao</th>
            <th>Área</th>
            <th>Perímetro</th>
            <th>Imóvel</th>
            <th>Status</th>
            <th v-if="tipo != 'Conflito Aberto'">Tramitado Por</th>
            <th v-if="tipo != 'Conflito Aberto'">Tramitado Para</th>
            <th v-if="tipo != 'Conflito Aberto'">Ação</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="i in listaConflito">
			<td style="text-align: center;">
				<a style="cursor: pointer;" 
					@click="pointMap(i.CodigoConflito,i.CodigoFaixaDominio)">
					<i class="fas fa-map-marker-alt"></i>
				</a>
			</td>
            <td>{{i.CodigoConflito}}</td>
            <td>{{i.KmInicialConflito}}</td>
            <td>{{i.KmFinalConflito}}</td>
            <td>{{formatNumeric(i.ExtensaoConflito)}}</td>
            <td>{{formatNumeric(i.AreaConflitoKm2)}}</td>
            <td>{{i.PerimetroConflitoKm}}</td>
            <td>{{i.CodigoEmpreendimento}}</td>
            <td>
				<a v-if="tipo !== 'Conflito Aberto'" 
					@click="getDetalhesConflito(i.CodigoConflito, i.CodigoFaixaDominio)"
					style="cursor: pointer; text-decoration: underline"
				>
					{{i.StatusConflito}}
				</a>
				<span v-else>{{i.StatusConflito}}</span>
			</td>
            <td v-if="tipo !== 'Conflito Aberto'">{{i.TramitadoPor}}</td>
            <td v-if="tipo !== 'Conflito Aberto'">{{i.TramitadoPara}}</td>
            <td v-if="tipo !== 'Conflito Aberto'">{{i.NomeAcao}}</td>
        </tr>
    </tbody>
</table>
