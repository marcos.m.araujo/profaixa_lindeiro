<template id="template-mapa-conflito-faixa">
	
	<!--End input search-->
	<div class="row">
		<div id="mapContainer" style="text-align: center; align-items: center;" class="col-md-12">
			<div id="mapid" class="map-height">
				<!-- Pesquisa por municipio -->
				<div id="buscaM" class="buscaM">
					<div class="col-lg-4 col-10" id="pesquisaMunicipio">
						<div class="input-group md-form form-sm form-2 pl-0">
							<!-- campo de pesquisa -->
							<input class="form-control" type="text" size="140"
								placeholder="Digite o nome do Município" id="inputMunic" list="inptmun" name="inptmun"
								@keypress="getMunicipio()">
							<!-- lista com municipios pesquisados -->
							<datalist id="inptmun">
								
							</datalist>
							<!-- botoes -->
							<div class="input-group-append">
								<!-- incluir municipio -->
								<button class="btn btn-info btn-sm btnBuscarM" @click="getMunicipioSelect()" title="Consultar município">
									<i class="fas fa-search text-grey" aria-hidden="true"></i>
								</button>
								<!-- excluir municipio -->
								<button title="Remover município do mapa" @click="clearMunicipioSelect()"
									style='display: none' class="btn btn-sm btn-danger btnClearBuscarM">
									<i class="fas fa-trash-alt"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
				<!-- FIM Pesquisa por municipio -->
				
				<!-- ZOOM Brasil -->
				<button @click="zoomHome()" class="btnMapa btnHome"><i class="fas fa-home"></i>
				</button>

				<!-- Imagem satelite -->
				<button @click="showImagemSatelite = !showImagemSatelite" 
					:style="[showImagemSatelite ? {'font-weight': 'bold', 'background-color': '#fff'} : {'font-weight': 'bold', 'background-color': '#BDBDBD'}]" 
					class="btnMapa baseMapa" title="Imagem de Satélite"
				>
					<i class="fas fa-map"></i>
				</button>

				<!-- layer SNV -->
				<button @click="showLayerSNV = !showLayerSNV" title="Rodovias" id="baseRodovia" 
					:style="[showLayerSNV ? {'background-color':'#fff','font-weight':'bold'} : {'background-color':'#BDBDBD','font-weight':'bold'}]" 
					class="btnMapa baseRodovia" title="SNV"
				>
					SNV
				</button>
				

				<button @click="showLayerConflito = !showLayerConflito" 
					:style="[showLayerConflito ? {'background-color':'#fff','font-weight':'bold'} : {'background-color':'#BDBDBD','font-weight':'normal'}]" 
					class="btnMapa baseConflito" title="Conflitos"
				>
					<i style="color: red" class="fab fa-confluence"></i>
				</button>
				<button @click="showLayerFaixa = !showLayerFaixa" 
					:style="[showLayerFaixa ? {'background-color':'#fff','font-weight':'bold'} : {'background-color':'#BDBDBD','font-weight':'normal'}]" 
					class="btnMapa baseFaixa" title="Faixas de Domínio"
				>
					<i style="color: blue" class="fas fa-road"></i>
				</button>
				<button @click="showLayerImovel = !showLayerImovel" 
					:style="[showLayerImovel ? {'background-color':'#fff','font-weight':'bold'} : {'background-color':'#BDBDBD','font-weight':'normal'}]" 
					class="btnMapa baseImovel" title="Imóveis"
				>
					<i style="color: green" class="fas fa-home"></i>
				</button>

				
				<!-- layer Publicação de faixa -->
				<button @click="showPublicarFaixa = !showPublicarFaixa" v-if="tipo == 'Conflito Aberto'"
					:style="[showPublicarFaixa ? {'background-color':'#fff','font-weight':'bold'} : {'background-color':'#BDBDBD','font-weight':'normal'}]" 
					class="btnMapa basePublicaFaixa" title="Segmentos de Faixas Para Aprovação"
				>
					<i style="color: #ff7700" class="fa fa-stamp"></i>
				</button>

				
				<!-- layer Sugestao de publicacao aprovada -->
				<button @click="showSugestaoAprovada = !showSugestaoAprovada"  v-if="tipo == 'Conflito Aberto'"
					:style="[showSugestaoAprovada ? {'background-color':'#fff','font-weight':'bold'} : {'background-color':'#BDBDBD','font-weight':'normal'}]" 
					class="btnMapa baseSugestaoPublicarAprovada" title="Sugestões de Segmentos Aprovadas"
				>
					<i style="color: #aa00ff" class="fa fa-stamp"></i>
				</button>
				
				<!-- layer Sugestao de publicacao em homologacao -->
				<button @click="showSugestaoFaixaHomolog = !showSugestaoFaixaHomolog"  v-if="tipo == 'Conflito Aberto'"
					:style="[showSugestaoFaixaHomolog ? {'background-color':'#fff','font-weight':'bold'} : {'background-color':'#BDBDBD','font-weight':'normal'}]" 
					class="btnMapa baseSugestaoPublicarHomolog" title="Sugestões de Segmentos em Homologação"
				>
					<i style="color: #00d9ff" class="fa fa-stamp"></i>
				</button>
			</div>
		</div>
	</div>
</template>

<?php $this->load->view('template_adminlte3/script_leaflet'); ?>

<script>

var faixaApublicar = [];

var mapaConflitoFaixa = Vue.component('mapa-conflito-faixa',{
	template: '#template-mapa-conflito-faixa',
	props: {
		tipo: String,
	},
	data() {
		return {
			
			params: {
				uf: '',
				br: '', 
				kminicial: '', 
				kmfinal: '',
				CodigoConflito: '',
				CodigoFaixaDominio: '',
				CodigoImovel: '',
				ConflitosIn: null,
				FaixasIn: null,
			},
			
			tramitadosParaMim: {
				Conflitos: null,
				Faixas: null
			},
			
			mymap: null,
			
			layerEstados: {},
			layerMunicipios: {},
			
			showImagemSatelite: false,
			layerSNV: {},
			showLayerSNV: false,
			
			layerAlterarCor: '',
			botaoAlteraCor: '',
			carregando: null, 
			
			imoveis: null,
			showLayerConflito: true,
			layerConflito: {},
			
			showLayerFaixa: true,
			layerFaixa: {},
			
			showLayerImovel: true,
			layerImovel:{},
			
			listaImovel: null,
			listaFaixa: null,
			listaConflito: null,
			
			coordenadasUFs: {
				AC: [ -8.77, -70.55]
				, AL: [ -9.71, -35.73]
				, AM: [ -3.07, -61.66]
				, AP: [  1.41, -51.77]
				, BA: [-12.96, -38.51]
				, CE: [ -3.71, -38.54]
				, DF: [-15.83, -47.86]
				, ES: [-19.19, -40.34]
				, GO: [-16.64, -49.31]
				, MA: [ -2.55, -44.30]
				, MT: [-12.64, -55.42]
				, MS: [-20.51, -54.54]
				, MG: [-18.10, -44.38]
				, PA: [ -5.53, -52.29]
				, PB: [ -7.06, -35.55]
				, PR: [-24.89, -51.55]
				, PE: [ -8.28, -35.07]
				, PI: [ -8.28, -43.68]
				, RJ: [-22.84, -43.15]
				, RN: [ -5.22, -36.52]
				, RO: [-11.22, -62.80]
				, RS: [-30.01, -51.22]
				, RR: [  1.89, -61.22]
				, SC: [-27.33, -49.44]
				, SE: [-10.90, -37.07]
				, SP: [-23.55, -46.64]
				, TO: [-10.25, -48.25]
				, Brasil: [-10.6007767, -63.6037797]
			},
			
			acesso: 0,

			showPublicarFaixa: false,

			layerEditavel:{},
			layerPublicar:{},
			layerDrawControl:{},

			publicacao: {
				CodigoFaixaDominio: ''
			},

			listaFaixasPublicacao: null,

			showSugestaoAprovada: false,
			showSugestaoFaixaHomolog: false,

			layerSugestaoAprovada: {},
			layerSugestaoHomolog: {},
			
			faixaApublicarSelecionada: false,

			listaFaixaPublicada: null,
			layerFaixasPublicadas: null,
		}
	},
	computed: {
		isMobile: function(){
			var check = false;
			(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
			return check;
		},
		listaFaixasApublicar:function(){
			return this.listaFaixasPublicacao.filter(function(item){
				return item.Status == "A Publicar";
			});
		},
		listaFaixasApublicarAprovadas: function(){
			return this.listaFaixasPublicacao.filter(function(item){
				return item.Status == "Sugestão Aprovada";
			});
		},
		listaFaixasApublicarHomolog: function(){
			return this.listaFaixasPublicacao.filter(function(item){
				return item.Status == "Sugestão em Homologação";
			});
		}
	},
	watch:{
		showImagemSatelite: function(val){
			this.changeViewSatelite(val);
		},
		showLayerSNV: function(val){
			this.plotLayerSNV();				
		},
		showLayerConflito: async function(val){
			await this.getConflitos();
			await this.plotLayerConflito();
		},
		showLayerFaixa: async function(val){
			await this.plotLayerFaixa();
		},
		showLayerImovel: async function(val){
			await this.plotLayerImovel();
		},
		showPublicarFaixa: async function(val){
			await this.plotLayerPublicar();
		},
		showSugestaoAprovada: async function(val){
			await this.plotLayerSugestaoFaixaAprovada();
		},
		showSugestaoFaixaHomolog: async function(val){
			await this.plotLayerSugestaoFaixaHomolog();
		},
	},
	async mounted(){
		
		vmGlobal.showLoading();
		
		await this.gerarMapa('mapid');
		
		$('.modal-dialog').draggable({" handle": ".modal-header"});

		if(this.tipo != 'Conflito Aberto')
		await this.getTramitadosParaMim();

		
	},
    methods: {
		// Gerador de mapa
		async gerarMapa(idMapa) {
			
			vmGlobal.showLoading();

			this.mymap = await leafletVue.gerarMapa(idMapa, true);
			
			$('#polyline-measure-control').parent().css('top','375px');
			
			// cria os layers necessarios
			this.layerEstados = L.geoJSON().addTo(this.mymap);
			this.layerMunicipios = L.geoJSON().addTo(this.mymap);
			this.layerSNV = L.geoJSON().addTo(this.mymap);
			
			this.layerConflito = L.geoJSON().addTo(this.mymap);
			this.layerFaixa = L.geoJSON().addTo(this.mymap);
			this.layerImovel = L.geoJSON().addTo(this.mymap);
			
			this.layerFaixasPublicadas = L.geoJSON().addTo(this.mymap);
			this.layerPublicar = L.geoJSON().addTo(this.mymap);
			this.layerSugestaoAprovada = L.geoJSON().addTo(this.mymap);
			this.layerSugestaoHomolog = L.geoJSON().addTo(this.mymap);

			// Initialise the FeatureGroup to store editable layers
			this.layerEditavel = new L.FeatureGroup();
			this.mymap.addLayer(this.layerEditavel);
			
			await this.consultaLayers();
		},
		// Botões de cálculos de metros no mapa
		controlPolyMeasure() {
			
			L.control.polylineMeasure({
				position: 'topleft',
				unit: 'metres',
				showBearings: true,
				clearMeasurementsOnStop: true,
				showClearControl: true,
				showUnitControl: true
			}).addTo(this.mymap);
			
		},
		addControlDraw(){
			this.faixaApublicarSelecionada = true;

			var iconPoly = new L.DivIcon({
				iconSize: new L.Point(16, 16),
				className: 'fa fa-stamp'
			})

			var drawPluginOptions = {
				position: 'topright',
				draw: {
					// polygon: {
					// 	allowIntersection: true,
					// 	drawError: {
					// 		color: '#e1e100', // Color the shape will turn when intersects
					// 		message: '<strong>Oh snap!<strong> you can\'t draw that!' // Message that will show when intersect
					// 	},
					// 	shapeOptions: {
					// 		color: '#00a6ff'
					// 	}, 
					// 	icon: iconPoly
					// },
					polygon: false,
					// disable toolbar item by setting it to false
					polyline: {
						allowIntersection: true,
						drawError: {
							color: '#e1e100', // Color the shape will turn when intersects
							message: '<strong>Oh snap!<strong> you can\'t draw that!' // Message that will show when intersect
						},
						shapeOptions: {
							color: '#ffff00',
							opacity: 1,
							fill: false,
							stroke: true,

							weight: 20,
						}, 
						icon: iconPoly
					},
					circle: false, // Turns off this drawing tool
					rectangle: false,
					marker: false,
				},
				edit: false
			};
								
			// Initialise the draw control and pass it the FeatureGroup of editable layers
			drawControl = new L.Control.Draw(drawPluginOptions);
			this.mymap.addControl(drawControl);

			var layerPublicar = new L.FeatureGroup();
			this.mymap.addLayer(layerPublicar);

			this.mymap.on('draw:created', (e) => {
				var type = e.layerType,
					layer = e.layer;
					
				var poly = layer.toGeoJSON();

				this.publicacao.poly = poly.geometry;

				if (type === 'polygon' || type === 'polyline' ) {
					layer.bindPopup('<div><form id="form-publicar" onsubmit="enviarPublicacaoFaixa(event)">' +
					'<h6>Trecho para Aprovação</h6>' +
					'<div class="form-group">' +
					'<label>Observações</label>' +
					'<textarea name="obsPublicarFaixa" id="obsPublicarFaixa" class="form-control" rows="6" cols="25" placeholder="Escreva uma observação ..."></textarea>'+
					'</div>' +
					// '<div class="form-group">' +
					// '<label class="mr-2">Anexos</label>' +
					// '<input type="file" name="anexosPublicarFaixa" multiple>'+
					// '</div>' +
					'<button class="btn bg-info" type="submit"><i class="fa fa-stamp mr-2"></i>Enviar</button><form></div>');
				}

				layerPublicar.addLayer(layer);
			});

			
		},
		// recupera dados e monta layer selecionados
		async consultaLayers() {
			
			vmGlobal.showLoading();

			var controller = 'Municipio/getCoordsLayerEstados';
			var data = await vmGlobal.getFromController(controller, {});

			this.layerEstados.clearLayers()
			
			for (var i = 0; i < data.length; i++) {
				var cord = data[i];
				
				if(cord.UF == this.params.uf || this.params.uf == '')
				this.addLayerEstados(cord.coordenada, cord.UF, .0, this.layerEstados, 'black')
				else 
				this.addLayerEstados(cord.coordenada, cord.UF, .1, this.layerEstados, 'trasparente')
			}
			
			await this.plotLayerSNV();
				
			if(this.params.CodigoConflito == null){
				// filtro por Faixa, para publicacao
				this.params.FaixasIn = [this.params.CodigoFaixaDominio];
				
				if(this.faixaApublicarSelecionada == false){
					this.addControlDraw();
				}
			}

			await this.getConflitos();
			
			this.params.ConflitosIn = this.listaConflito.map(x=>x.CodigoConflito).filter(function(elem, index, self) {
				return index === self.indexOf(elem);
			});
			
			this.params.FaixasIn = this.listaConflito.map(x=>x.CodigoFaixaDominio).filter(function(elem, index, self) {
				return index === self.indexOf(elem);
			});
			
			await this.plotLayerImovel();
			await this.plotLayerFaixa();
			await this.plotLayerConflito()
			
			if(this.params.uf != ''){
				this.zoomUF(this.params.uf);
			}

			this.$nextTick(function(){
				this.$emit('emitloaded');
			});
			
			vmGlobal.closeLoading();
		},
		// monta layer dos estados
		addLayerEstados(cord, uf, fillOpacity, layer, color) {
			
			var array = {
				"type": "FeatureCollection",
				"features": [{
					"type": "Feature",
					"geometry": JSON.parse(cord),
					"properties": {
						"OBJECTID": uf,
						"DADOS" : 'TEste'
					}
				}]
			}
			
			L.geoJson(array, {
				style: function (feature) {
					return {
						stroke: true,
						color: color,
						weight: 1,
						fillOpacity: fillOpacity,
					};
				},
				/**
				 * todo -> precisa emitir UF para filtro
				 */
				onEachFeature: (feature, l) => {
					var vm = this;
					l.on('contextmenu', function (e) {
						vm.$emit('emituf', feature.properties.OBJECTID);
					});
					l.on("dblclick", function (e) {
						vm.$emit('emituf', '');							
					});
				}
			}).addTo(layer)
			
		},	
        zoomUF(UF){  
			
			if(UF == '') {
				UF = 'Brasil';
            }
			
            var escalaMenor = ['Brasil'];
            var escalaMedia = ['AM','PA','MT','BA','MG','PI','MA']; //7
            var escalaMediaMaior = ['SC', 'RO', 'RR', 'SP', 'PR', 'RS','AC','RS','MS','CE','AP'];//11
            var escalaMaior = ['RN', 'PB', 'AL', 'SE', 'ES', 'RJ','AP','DF'];//8
			
            var zoom = 5;
            if (escalaMenor.includes(UF)){
				zoom = 4.1;
            } else if (escalaMedia.includes(UF)){
				zoom = 6.1;
            }else if (escalaMediaMaior.includes(UF)){
				zoom = 7.1;
            }else if (escalaMaior.includes(UF)){
				zoom = 8.1;
            }
			this.mymap.setView(this.coordenadasUFs[UF], zoom);
		},
		async plotLayerSNV() {
			
			this.layerSNV.clearLayers()

			if(this.showLayerSNV){
				
				var controller = 'MalhaRodoviaria/getMalhaRodovia';
				var data = await vmGlobal.getFromController(controller, this.params);
				
				for (var i = 0; i < data.length; i++) {
					var cord = data[i];
					if (cord.DescricaoSuperficie == 'PLA') {
						leafletVue.setMapRodovia(cord.Coordenada, cord.Concatenado, '#38f404', this.layerSNV, '3,20', 'round')
					} else {
						leafletVue.setMap(cord.Coordenada, cord.Concatenado, '#38f404', this.layerSNV)
					}
				}
			}
		},	
		//volta o zoom para visao nacional
		zoomHome() {
			this.mymap.setView(['-10.6007767', '-63.6037797'], 4.6);
		},            
		// altera a visualizacao do mapa para estilo satelite
		changeViewSatelite(showImagemSatelite) {
			if (!showImagemSatelite) {
				L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
					maxZoom: 18,
					attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
					'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ',
					id: 'mapbox.streets'
				}).addTo(this.mymap);
				
			} else {
				L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
					maxZoom: 20,
					subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
				}).addTo(this.mymap);
			}
		}, 
		// busca municipio pelo nome, monta lista com nomes parecidos            
		async getMunicipio() {
			
			var hsp = $("#inputMunic").val();
			if (hsp.length > 2) {
				var controller = 'Municipio/getMunicipiosGeo';
				var params = {municipio: hsp};
				var data = await vmGlobal.getFromController(controller, params);

				$('#inptmun').empty();
				for (var i = 0; i < data.length; i++) {
					var hosp = data[i];
					$("#inptmun").append('<option value="' + hosp.MunicipioEstado + '">' + hosp.MunicipioEstado +"</option>");
				}
			}
			
		},
		// adiciona marcador em cima do municipio selecionado
		async getMunicipioSelect() {
			var m = $("#inputMunic").val();
			if (m != "") {
				
				var controller = 'Municipio/getMunicipiosCord';
				var params = {municipio: m};
				var data = await vmGlobal.getFromController(controller, params);
				
				leafletVue.addLayer(data[0].Coordenada, m, '',this.layerMunicipios);
				$(".btnClearBuscarM").css('display', 'block')
				
				return this.layerMunicipios;
			}
		},
		// retira do mapa municipios selecionados
		clearMunicipioSelect(input, clearClass) {
			this.layerMunicipios.clearLayers()
			$("#inputMunic").val('')
			$(".btnClearBuscarM").css('display', 'none')
		},
		formatNumeric(value, tofix) {
			let val = (value / 1).toFixed(tofix)
			let val2 = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
			return val2.toString().replace(".", ",")
		},
		async getConflitos(){
			vmGlobal.showLoading();
			
			var controller = 'GestaoConflitosFaixa/getFaixaConflitos';
			var params = this.params;
			params.tipo = this.tipo;
			
			var data = await vmGlobal.getFromController(controller, params);
			
			this.listaConflito = data; 
			vmGlobal.closeLoading();

		},
		async plotLayerConflito(){
			
			vmGlobal.showLoading();

			this.layerConflito.clearLayers();
			
			if(this.showLayerConflito){	
				for (i = 0; i < this.listaConflito.length; i++) {
					
					var extensaoConflito = this.formatNumeric(this.listaConflito[i].ExtensaoConflito, 1);
					var areaConflitoKm2 = this.formatNumeric(this.listaConflito[i].AreaConflitoKm2, 2);
					var codigoConflito = this.listaConflito[i].CodigoConflito
					var codigoFaixaDominio = this.listaConflito[i].CodigoFaixaDominio
					var concatenado = '<div><strong>INFORMAÇÕES DO CONFLITO</strong> <br>' +
					'<br> <strong>Código conflito:</strong> ' + this.listaConflito[i].CodigoConflito +
					'<br> <strong>Extensão aproximada do conflito:</strong> ' + extensaoConflito + ' km' +
					'<br> <strong>Área do conflito:</strong> ' + areaConflitoKm2 + ' km²' +
					'<br> <strong>Situação do conflito:</strong> ' + this.listaConflito[i].StatusConflito +
						'<br> <strong>Fonte:</strong> ' + this.listaConflito[i].Fonte +
						"<br><br><br> <button type='button' onclick='pointMap(" + codigoConflito + "," + codigoFaixaDominio + ")' class='btn btn-block btn-outline-danger btn-sm'><i class='fas fa-filter'></i> Filtrar este conflito</button>" +
						'</div>';
					leafletVue.addLayer(this.listaConflito[i].Coordenada, concatenado, 'red', this.layerConflito);
					
				}
				
				if(this.listaConflito.length > 0) {
					this.mymap.setView(this.layerConflito.getBounds().getCenter())
					this.mymap.fitBounds(this.layerConflito.getBounds())
				}
			}

			vmGlobal.closeLoading();
		},
		async plotLayerFaixa(){
			
			vmGlobal.showLoading();
			
			this.layerFaixa.clearLayers();
			
			if(this.listaConflito.length>0 && this.showLayerFaixa){	
				
				var controller = 'GestaoConflitosFaixa/getFaixaDominioOperacional';
				var data = await vmGlobal.getFromController(controller, this.params);
				this.listaFaixa = data;
				
				if(data.length > 0){
					
					for (i = 0; i < data.length; i++) {
						var concatenado = '<div><strong>INFORMAÇÕES DA FAIXA DE DOMÍNIO</strong> <br>' +
							'<br> <strong>BR-' + data[i].FaixaDominioBR + '/' + data[i].UF + '</strong>' +
							'<br> <strong>Trecho:</strong> ' + data[i].KmInicial + ' ao ' + data[i].KmFinal +
							'<br> <strong>Extensão:</strong> ' + data[i].ExtensaoKm + ' km <br>' +
							'<br> <strong>Largura da faixa lado direito:</strong> ' + data[i].FaixaDominioLadoDireito + ' m' +
							'<br> <strong>Largura da faixa lado esquerdo:</strong> ' + data[i].FaixaDominioLadoEsquerdo + ' m' +
							'<br> <strong>Largura da faixa:</strong> ' + data[i].FaixaDominioLargura + ' m <br>' +
							'<br> <strong>Checagem:</strong> ' + data[i].Checagem +
							'<br> <strong>Fonte:</strong> ' + data[i].Fonte +
							'<br><br><strong>Total de Conflitos:</strong> ' + data[i].total +
							'<br><br> Selecionar para publicação<button title="Publicação de Faixa" class="ml-2" style="background-color: rgb(255,255,255);' +
							' font-weight: normal; outline: none;" onclick="selecionarFaixaParaPublicacao('+data[i].CodigoFaixaDominio+',\''+data[i].BR2+ '\',\''+ 
							data[i].UF +'\')"><i class="fa fa-stamp" style="color: rgb(0, 166, 255);"></i></button>' +
							'</div>';
							leafletVue.addLayer(data[i].Coordenada, concatenado, '#0000FF', this.layerFaixa);
					}
					// this.mymap.setView(this.layerFaixa.getBounds().getCenter())
					// this.mymap.fitBounds(this.layerFaixa.getBounds())
				}
			}


			this.plotLayerFaixasPublicadas();

			vmGlobal.closeLoading();
		},
		async plotLayerImovel(){

			vmGlobal.showLoading();

			this.layerImovel.clearLayers();
			
			if(this.listaConflito.length>0 && this.showLayerImovel){	
				
				var controller = 'GestaoConflitosFaixa/getFaixaImoveis';
				var data = await vmGlobal.getFromController(controller, this.params);
				
				this.listaImovel = data;
				
				if(data.length > 0){					
					for (i = 0; i < data.length; i++) {
						var concatenado = '<div><strong>INFORMAÇÕES DO IMÓVEL</strong> <br>' +
							'<br> <strong>Codigo imóvel:</strong> ' + data[i].CodigoEmpreendimento +
							'<br> <strong>Área do imóvel:</strong> ' + this.formatNumeric(data[i].AreaKm2, 2) + ' km²' +
							'<br> <strong>Perimêtro do imóvel:</strong> ' + this.formatNumeric(data[i].PerimetroKm, 2) + ' km' +
							'<br> <strong>Fonte:</strong> ' + data[i].Fonte +
							"<br><br><br> <button type='button' onclick='abrirDadosImovel(" + JSON.stringify(data[i].Fonte) + "," + data[i].CodigoTabelaFonte + ")' class='btn btn-block btn-outline-success btn-sm'><i class='fas fa-home'></i> Detalhes Imóvel</button>" +
							'</div>';
							leafletVue.addLayer(data[i].Coordenada, concatenado, 'green', this.layerImovel);
						}
						// this.mymap.setView(this.layerImovel.getBounds().getCenter())
						// this.mymap.fitBounds(this.layerImovel.getBounds())
				}
			}
			vmGlobal.closeLoading();
		},
		async plotLayerPublicar() {
			
			vmGlobal.showLoading();
			
			this.layerPublicar.clearLayers();

			if(this.showPublicarFaixa){
			
				if(this.listaFaixasApublicar.length > 0){
				
					for (i = 0; i < this.listaFaixasApublicar.length; i++) {
						var concatenado = '<div><h6>Trecho A Publicar</h6>' +
							'<br> <strong>BR-' + this.listaFaixasApublicar[i].FaixaDominioBR + '/' + this.listaFaixasApublicar[i].UF + '</strong>' +
							'<br> <strong>Trecho:</strong> ' + this.listaFaixasApublicar[i].KmInicialTrechoPublicacao + ' ao ' + this.listaFaixasApublicar[i].KmFinalTrechoPublicacao +
							'<br> <strong>Largura da faixa lado direito:</strong> ' + this.listaFaixasApublicar[i].FaixaDominioLadoDireito + ' m' +
							'<br> <strong>Largura da faixa lado esquerdo:</strong> ' + this.listaFaixasApublicar[i].FaixaDominioLadoEsquerdo + ' m' +
							'<br> <strong>Largura da faixa:</strong> ' + this.listaFaixasApublicar[i].FaixaDominioLargura + ' m <br>' +
							'<br> <strong>Tramitado por:</strong> ' + this.listaFaixasApublicar[i].TramitadoPor + '<br>' +
							'<br> <div class="row">'+
							'<div class="col-md-4"><button title="Aprovar Sugestão"  class="btn btn-sm btn-block btn-success  ml-2" onclick="aprovarSugestaoPublicacao('+ this.listaFaixasApublicar[i].CodigoFaixaApublicar + ')">' +
							'Aprovar</button></div>' +
							'<div class="col-md-4"><button title="Recusar Sugestão"  class="btn btn-sm btn-block btn-danger ml-2" onclick="deletaSugestaoPublicacao('+ this.listaFaixasApublicar[i].CodigoFaixaApublicar + ')">' +
							'Recusar</button></div></div>';
							
						leafletVue.addLayer(this.listaFaixasApublicar[i].CoordenadaTrechoPublicacao, concatenado, '#ff7700', this.layerPublicar);
			
						var array = {
							"type": "FeatureCollection",
							"features": [{
								"type": "Feature",
								"geometry": JSON.parse(this.listaFaixasApublicar[i].CoordenadaTrechoPublicacao),
			
							}]
						}

						L.geoJson(array, {
							style: function(feature) {
								return {
									stroke: true,
									color: '#ff7700',
									weight: 5
								};
							},
						
						});
					}
				}
			}
			
			vmGlobal.closeLoading();
		},
		async plotLayerFaixasPublicadas(){

			vmGlobal.showLoading();

			
			this.layerFaixasPublicadas.clearLayers();

			if(this.showLayerFaixa){

				const params = {
					table: 'FaixasPublicadas'
				};
				var data = await vmGlobal.getFromAPI(params, 'CGDR');
				this.listaFaixaPublicada = data;
				
				if(data.length > 0){
					
					for (i = 0; i < data.length; i++) {
						var concatenado = '<div><strong>INFORMAÇÕES DA FAIXA DE DOMÍNIO PUBLICADA</strong> <br>' +
						'<br> <strong>' + data[i].BR + '/' + data[i].UF + '</strong>' +
						'<br> <strong>Trecho:</strong> ' + data[i].KmInicial + ' ao ' + data[i].KmFinal +
						'</div>';
						leafletVue.addLayer(data[i].Coordenada, concatenado, '#41d6f0', this.layerFaixasPublicadas);
					}
					// this.mymap.setView(this.layerFaixasPublicadas.getBounds().getCenter())
					// this.mymap.fitBounds(this.layerFaixasPublicadas.getBounds())
				}
			}
				
			vmGlobal.closeLoading();

		},
		async plotLayerSugestaoFaixaAprovada() {
			
			vmGlobal.showLoading();

			this.layerSugestaoAprovada.clearLayers();

			if(this.showSugestaoAprovada){
				if(this.listaFaixasApublicarAprovadas.length > 0){
				
					for (i = 0; i < this.listaFaixasApublicarAprovadas.length; i++) {
						var concatenado = '<div><h6>Enviar para Homologação</h6>' +
							
							'<strong>BR-' + this.listaFaixasApublicarAprovadas[i].FaixaDominioBR + '/' + this.listaFaixasApublicarAprovadas[i].UF + '</strong>' +
							'<br> <strong>Trecho:</strong> ' + this.listaFaixasApublicarAprovadas[i].KmInicialTrechoPublicacao + ' ao ' + this.listaFaixasApublicarAprovadas[i].KmFinalTrechoPublicacao +
							'<br> <strong>Tramitado por:</strong> ' + this.listaFaixasApublicarAprovadas[i].TramitadoPor + ' m <br>' +
							
							'<br><div class="row">'+
							'<div class="col-md-5"><button title="KML da Faixa"  class="btn btn-sm btn-block btn-default  ml-2" onclick="getKmlFaixa('+ this.listaFaixasApublicarAprovadas[i].CodigoFaixa + ')">' +
							'<i class="fa fa-download" style="color: rgb(0,0,0);"></i>&nbsp;KML da faixa</button>' +
							'</div>' +
							
							'<div class="col-md-5"><button title="KML do Trecho"  class="btn btn-sm btn-block btn-default  ml-2" onclick="getKmlSegmento('+ this.listaFaixasApublicarAprovadas[i].CodigoFaixaApublicar + ')">' +
							'<i class="fa fa-download" style="color: rgb(0,0,0);"></i>&nbsp;KML do Trecho</button></div>' +
							'</div>' +
							
							'<form onsubmit="enviaTrechoHomolog(event)">' + 
							
							'<div class="form-group required">'  +
								'<label class="mt-2">Homologador</label>'  +
								'<input type="text" placeholder="Email" list="fxlistaEmails" name="fxTramitadoPara" id="fxTramitadoPara" class="form-control" required onkeypress="findEmail(this.value, \'#fxlistaEmails\')">'  +
								'<datalist id="fxlistaEmails"></datalist>' +
							'</div>'+

							'<input type="hidden" id="CodigoFaixaApublicar" name="CodigoFaixaApublicar" value="'+ this.listaFaixasApublicarAprovadas[i].CodigoFaixaApublicar +'">' + 
							
							'<div class="form-group mt-2 ">' + 
							'<label>Observações</label>' +
							'<textarea name="obsPublicarFaixa" id="obsPublicarFaixa" class="form-control" rows="6" cols="25" placeholder="Escreva uma observação ..." ></textarea>' + 
							'</div>' +

							'<div class="form-group required" id="infoTrecho">'  +
								'<label class="mt-2 ">Trecho de Faixa A Publicar</label>'  +
								'<span class="float-right">'  +
									'<input type="file" name="" id="pubArquivoTrecho" accept=".kml" required>'  +
								'</span>'  +
							'</div>'+

							/*'<div class="form-group mt-2 " id="infoFaixa">' + 
								'<label class="mt-2">Faixa de Domínio Remanescente</label>' +
								'<span class="float-right">' +
									'<input type="file" name="" id="pubArquivoFaixa" accept=".kml" >' +
								'</span>' +
							'</div>'  +
							*/

							'<br><br>' +
							'<button class="btn bg-info" type="submit"><i class="fa fa-stamp mr-2"></i>Enviar</button></div></form>';
							
						leafletVue.addLayer(this.listaFaixasApublicarAprovadas[i].CoordenadaTrechoPublicacao, concatenado, '#aa00ff', this.layerSugestaoAprovada);
			
						var array = {
							"type": "FeatureCollection",
							"features": [{
								"type": "Feature",
								"geometry": JSON.parse(this.listaFaixasApublicarAprovadas[i].CoordenadaTrechoPublicacao),
			
							}]
						}

						L.geoJson(array, {
							style: function(feature) {
								return {
									stroke: true,
									color: '#aa00ff',
									weight: 5
								};
							},
								// onEachFeature: (feature, l) => {
					
								// 		l.on('click', (e)=> {
								// 			console.log(this.acesso)		 
								// 				if(this.acesso == 0){
								// 						this.layerPublicar.addLayer(L.marker(e.latlng));
								// 						this.publicarKmInicial = e.latlng;
								// 						console.log('this.publicarKmInicial',this.publicarKmInicial);
								// 					} else if(this.acesso == 1){
								// 							this.layerPublicar.addLayer(L.marker(e.latlng));
								// 							this.publicarKmFinal = e.latlng;
								// 							console.log('this.publicarKmFinal',this.publicarKmFinal);
								
								// 						}
								
								// 						this.acesso++;
								// 					});
								
								// 				}
								// 			}).addTo(this.layerPublicar)
								
								// 		}
						
						});
					}
				}
			}
			vmGlobal.closeLoading();
		},	
		async plotLayerSugestaoFaixaHomolog() {
			
			vmGlobal.showLoading();
			
			this.layerSugestaoHomolog.clearLayers();

			if(this.showSugestaoFaixaHomolog){
				if(this.listaFaixasApublicarHomolog.length > 0){
				
					for (i = 0; i < this.listaFaixasApublicarHomolog.length; i++) {
						var concatenado = '<div><h6>Homologação do Trecho a Publicar</h6> ' +
							'<br> <strong>BR-' + this.listaFaixasApublicarHomolog[i].FaixaDominioBR + '/' + this.listaFaixasApublicarHomolog[i].UF + '</strong>' +
							'<br> <strong>Trecho:</strong> ' + this.listaFaixasApublicarHomolog[i].KmInicialTrechoPublicacao + ' ao ' + this.listaFaixasApublicarHomolog[i].KmFinalTrechoPublicacao +
							'<br><br> <strong>Tramitado por:</strong> ' + this.listaFaixasApublicarHomolog[i].TramitadoPor  +
							'<br> <strong>Tramitado para:</strong> ' + this.listaFaixasApublicarHomolog[i].TramitadoPara + '<br>' +

							'<br><strong>Largura da faixa lado direito:</strong> ' + this.listaFaixasApublicarHomolog[i].FaixaDominioLadoDireito + ' m' +
							'<br> <strong>Largura da faixa lado esquerdo:</strong> ' + this.listaFaixasApublicarHomolog[i].FaixaDominioLadoEsquerdo + ' m' +
							'<br> <strong>Largura da faixa:</strong> ' + this.listaFaixasApublicarHomolog[i].FaixaDominioLargura + ' m <br>' ;

							if(this.listaFaixasApublicarHomolog[i].ArquivoTrecho != null){
								concatenado += '<br><a style="cursor: pointer;" onclick="visualizarHomologFaixa('+ this.listaFaixasApublicarHomolog[i].CodigoFaixaApublicar + ',\'' + this.listaFaixasApublicarHomolog[i].ArquivoTrecho +'\')">Visualizar KML</a><br>';

							}
							
							

						if(this.listaFaixasApublicarHomolog[i].TramitadoPara == localStorage.getItem('email')){
							concatenado += '<br><div class="row"><div class="col-md-4"><button title="Aprovar Publicação"  class="btn btn-sm btn-block btn-success  ml-2" onclick="aprovaPublicacao('+ this.listaFaixasApublicarHomolog[i].CodigoFaixaApublicar + ')">' +
							'Aprovar</button></div>' +
							'<div class="col-md-4"><button title="Recusar Publicação"  class="btn btn-sm btn-block btn-danger ml-2" onclick="showFormRecusa()">' +
							'Recusar</button></div></div>' +

							'<div id="formRecusa" style="display: none;">' + 

							'<form onsubmit="recusaHomologacao(event)">' + 
							
							'<div class="form-group required">'  +
								'<label class="mt-2">Tramitar Para</label>'  +
								'<input type="text" placeholder="Email" list="fxHomlistaEmails" name="fxHomTramitadoPara" id="fxHomTramitadoPara" value="' + this.listaFaixasApublicarHomolog[i].TramitadoPor + '" class="form-control" required onkeypress="findEmail(this.value, \'#fxHomlistaEmails\')">'  +
								'<datalist id="fxHomlistaEmails"></datalist>' +
							'</div>'+

							'<input type="hidden" id="CodigoFaixaApublicarHom" name="CodigoFaixaApublicar" value="'+ this.listaFaixasApublicarHomolog[i].CodigoFaixaApublicar +'">' + 
						
							'<div class="form-group mt-2 required">' + 
							'<label>Observações</label>' +
							'<textarea name="obsHomologPublicarFaixa" id="obsHomologPublicarFaixa" class="form-control" rows="6" cols="25" placeholder="Escreva uma observação ..." required></textarea>' + 
							'</div>' +
							
							'<div class="col-md-4"><button type="submit" title="Recusar Publicação"  class="btn btn-sm btn-block btn-danger ml-2" ">' +
							'Enviar</button></div></div>' +
							'</form>';
						}	
						
						concatenado += '</div>';
							
							
						leafletVue.addLayer(this.listaFaixasApublicarHomolog[i].CoordenadaTrechoPublicacao, concatenado, '#00d9ff', this.layerSugestaoHomolog);
			
						var array = {
							"type": "FeatureCollection",
							"features": [{
								"type": "Feature",
								"geometry": JSON.parse(this.listaFaixasApublicarHomolog[i].CoordenadaTrechoPublicacao),
			
							}]
						}

						L.geoJson(array, {
							style: function(feature) {
								return {
									stroke: true,
									color: '#00d9ff',
									weight: 5
								};
							},
								// onEachFeature: (feature, l) => {
					
								// 		l.on('click', (e)=> {
								// 			console.log(this.acesso)		 
								// 				if(this.acesso == 0){
								// 						this.layerPublicar.addLayer(L.marker(e.latlng));
								// 						this.publicarKmInicial = e.latlng;
								// 						console.log('this.publicarKmInicial',this.publicarKmInicial);
								// 					} else if(this.acesso == 1){
								// 							this.layerPublicar.addLayer(L.marker(e.latlng));
								// 							this.publicarKmFinal = e.latlng;
								// 							console.log('this.publicarKmFinal',this.publicarKmFinal);
								
								// 						}
								
								// 						this.acesso++;
								// 					});
								
								// 				}
								// 			}).addTo(this.layerPublicar)
								
								// 		}
						
						});
					}
				}
			}
			vmGlobal.closeLoading();
		},	
		async getTramitadosParaMim(){
			
			vmGlobal.showLoading();
			
			var controller = "GestaoConflitosFaixa/getTramitadosPara";
			var params = {email: localStorage.getItem('email')}
			var data = await vmGlobal.getFromController(controller, params);
			
			this.tramitadosParaMim.Conflitos = data.map(x=>x.CodigoConflito);
			this.tramitadosParaMim.Faixas = data.map(x=>x.CodigoFaixa);
			
		},
		async filtrarTramitadoPraMim(){
			
			this.params.ConflitosIn = this.tramitadosParaMim.Conflitos
			this.params.FaixasIn = this.tramitadosParaMim.Faixas
			
			await this.consultaLayers();
		}
    }
});

// $(window).on("resize", function() {
//     $("#mapid").height($(window).height()).width($(window).width());
//     map.invalidateSize();
// }).trigger("resize");		
</script>


<style scoped>
	.map-height {
        width: 98%;
        height: 752px; 
        z-index: 1;
		border: 1px solid #dee2e6;
		left: 8px;
    }

	.card-body {
		min-height: 500px !important;
	}
	
	.btnMapa {
		position: absolute;
		left: 10px;
		padding: 8px;
		text-align: center;
		z-index: 1000;
		width: 34px;
		background-color: #FFFF;
		border-radius: 4px;
		cursor: pointer;
	}
	.btnHome {
		top: 85px;
		left: 10px;
	}
	
	.baseMapa {
		top: 121px;
		left: 10px;
		height: 35px;
	}

	.baseRodovia {
		top: 155px;
		left: 10px;		
		padding: 0 !important;
		width: 34px;
		height: 36px;
		font-size: 9px !important;
	}

    .baseConflito {
        top: 209px;
        /* top: 244px;  */
    }

    .baseFaixa {
        top: 240px;
        /* top: 275px;  */
    }

    .baseImovel {
        top: 270px;
        /* top: 310px;  */
    }


	.basePublicaFaixa{
		top: 330px;
	}

	
	.baseSugestaoPublicarAprovada{
		top: 365px;
	}

	.baseSugestaoPublicarHomolog{
		top: 400px;
	}
    .buscaM {
        position: absolute;
        top: 4px;
        left: 60px;
        padding: 8px;
        text-align: center;
        z-index: 1000;
    }

    .buscaM input {
        width: 350px;
        height: 30px;
        border-radius: 4px;
        z-index: 1000;
    }

    .buscaM .btnBuscarM {
        width: 40px;
        height: 30px;
        border-radius: 4px;
        z-index: 1000;
    }

	
    .buscaM .btnClearBuscarM {
        width: 40px;
        height: 30px;
        border-radius: 4px;
        z-index: 1000;
    }


	#preloader {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(255, 255, 255, 0.5);
        /* cor do background que vai ocupar o body */
        z-index: 999;
        /* z-index para jogar para frente e sobrepor tudo */
    }
 
    #preloader .inner {
        position: absolute;
        top: 50%;
        /* centralizar a parte interna do preload (onde fica a animação)*/
        left: 50%;
        transform: translate(-50%, -50%);
    }
</style>

<link rel="stylesheet" href="<?= base_url('template_lte3/plugins/leaflet/leaflet.css') ?>">
<link rel="stylesheet" href="https://ppete2.github.io/Leaflet.PolylineMeasure/Leaflet.PolylineMeasure.css" />
<link rel="stylesheet" href="<?= base_url('template_lte3/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') ?>">


