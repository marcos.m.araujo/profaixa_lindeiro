<div class="card-header">
	<div class="mailbox-controls">
		
		<button @click="dadosConflitoDetalhe = true, detalheAcao = false, timeLineDetalhe = false" 
			type="button" :class="[dadosConflitoDetalhe ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']"
		>
			<i class="fab fa-confluence"></i> Dados
		</button>
		
		<button v-if="params.CodigoConflito != '' && params.CodigoConflito != null && (tipo == 'Conflito Aberto' || tipo == 'Em Homologação' || ( tramitadosParaMim.Conflitos && tramitadosParaMim.Conflitos.includes(params.CodigoConflito)))" @click="detalheAcao = true, dadosConflitoDetalhe = false, timeLineDetalhe = false" 
			type="button" :class="[detalheAcao ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']"
		>
			<i class="fas fa-cogs"></i> Ações
		</button>
		
		<button v-if="tipo != 'Conflito Aberto'" @click="timeLineDetalhe = true, detalheAcao = false, dadosConflitoDetalhe = false " 
			type="button" :class="[timeLineDetalhe ? 'btn btn-success btn-sm': 'btn btn-default btn-sm']"
		>
			<i class="fas fa-history"></i> Histórico
		</button>
		
		<div class="float-right col-sm-1">
			<a @click="fechaDetalhe()" style="cursor:pointer;"><i class="fa fa-times" aria-hidden="true"></i></a>
		</div>

	</div>
</div>
<div style="background-color: #F7F7F7!important;" class="card-body ">
	<div v-show="dadosConflitoDetalhe">
		<?php $this->load->view("componentes/detalheConflito/dados") ?>
	</div>
	<div v-show="detalheAcao">
		<?php $this->load->view("componentes/detalheConflito/acao") ?>
	</div>

	<div class="timelines" v-show="timeLineDetalhe">	
		<?php $this->load->view("componentes/detalheConflito/timeline") ?>
	</div>
</div>
