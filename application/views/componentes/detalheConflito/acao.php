<div class="card-body p-0" style="margin-top: 10px;">
	<form v-on:submit.prevent="salvarAcao">
	<div class="form-group required">
		<label>Ação</label>
		<select v-model="acaoModel.CodigoAcao" class="form-control" required>
			<option v-for="i in listaAcao" :value="i.CodigoAcao">{{i.NomeAcao}}</option>
		</select>
	</div>
	
	<div class="form-group required" v-if="tipo != 'Em Andamento' && acaoModel.CodigoAcao != 8" class="mb-2">
		<label for="exampleInputEmail1">Tramitar para</label>
		<div v-if="acaoModel.CodigoAcao == 9">
			<select v-model="acaoModel.TramitadoPara" class="form-control" required>
				<option v-for="i in historicoTramitacao" :value="i">{{i}}</option>
			</select>
		</div>
		<div v-else>
			<input type="email" v-model="acaoModel.TramitadoPara" class="form-control"  
				placeholder="Email"	@keypress="getEmails()"  list="listaEmails"
				required
				>
			<!-- lista com Emails pesquisados -->
			<datalist id="listaEmails">
				
			</datalist>
		</div>

	</div>

	

	<div class="form-group required">
		<label>Observações</label>
		<textarea v-model="acaoModel.ObservacaoTramitacao" class="form-control" rows="10" 
			placeholder="Escreva uma observação ..." required
		>
		</textarea>
	</div>
	
	<div v-if="tipo == 'Em Andamento'">	
		<h4 class="mb-4">Arquivos KML</h4>
		<div class="form-group" id="infoConflito">
			<label>Conflito</label>
			<span class="float-right">
				<input type="file" name="" id="ArquivoConflito" ref="ArquivoConflito" accept=".kml">
			</span>
		</div>
		<div class="form-group" id="infoFaixa">
			<label>Faixa de Domínio</label>
			<span class="float-right">
				<input type="file" name="" id="ArquivoFaixa" ref="ArquivoFaixa" accept=".kml">
			</span>
		</div>
		<div class="form-group" id="infoImovel">
			<label>Imóvel</label>
			<span class="float-right">
				<input type="file" name="" id="ArquivoImovel" ref="ArquivoImovel" accept=".kml">
			</span>
		</div>
	</div>
	<div class="card-footer">
		<button class="btn btn-info float-right">Salvar</button>
	</div>
	</form>
</div>
