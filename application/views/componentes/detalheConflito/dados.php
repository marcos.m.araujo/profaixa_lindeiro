<div class="card-footer p-0" v-show="showDetalheConflito">
	<i style="margin-left: 10px; color: red" class="fab fa-confluence"></i> Conflito
	<ul v-for="i in listaConflito" class="nav flex-column">
		<li class="nav-item">
			<span class="nav-link">
				Situação do conflito <span class="float-right badge bg-danger">{{i.StatusConflito}}</span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Área do conflito <span class="float-right">{{formatNumeric(i.AreaConflitoKm2,2)}} km²</span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Km inicial <span class="float-right">{{formatNumeric(i.KmInicialConflito, 1)}}</span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Km final <span class="float-right">{{formatNumeric(i.KmFinalConflito, 1)}}</span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Extensão aproximada do conflito <span class="float-right">{{formatNumeric(i.ExtensaoConflito,1)}} km</span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Arquivo KML 
				<span class="float-right">
					<a style="cursor: pointer;" @click="getKmlConflito(i.CodigoConflito)">
						<i class="fa fa-download" aria-hidden="true"></i>
					</a>
				</span>
			</span>
		</li>
	</ul>
</div>

<div style="margin-top: 10px" class="card-footer p-0" v-show="showDetalheImovel">
	<i style="margin-left: 10px; color: green" class="fas fa-home"></i> Imóvel
	<ul v-for="i in listaImovel" class="nav flex-column">
		<li class="nav-item">
			<span class="nav-link">
				Munícipio do imóvel <span class="float-right">{{i.Municipio}}</span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Área do imóvel <span class="float-right">{{formatNumeric(i.AreaKm2,2)}} km²</span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Perimêtro do imóvel <span class="float-right">{{formatNumeric(i.PerimetroKm,2)}} km</span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Ver mais detalhes do imóvel <span class="float-right"><button type="button" @click="abrirDadosImovel(i.Fonte,i.CodigoTabelaFonte)" class="btn btn-block btn-success btn-sm">Abrir </button></span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Fonte <span class="float-right">{{i.Fonte}} </span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Arquivo KML 
				<span class="float-right">
					<a style="cursor: pointer;" @click="getKmlImovel(i.CodigoEmpreendimento)">
						<i class="fa fa-download" aria-hidden="true"></i>
					</a>
				</span>
			</span>
		</li>
	</ul>
</div>

<div style="margin-top: 10px" class="card-footer p-0"  v-show="showDetalheFaixa">
	<i style="margin-left: 10px;color: blue" class="fas fa-road"></i> Faixa
	<ul v-for="i in listaFaixa" class="nav flex-column">
		<li class="nav-item">
			<span class="nav-link">
				Trecho <span class="float-right ">{{i.KmInicial}} ao {{i.KmFinal}}</span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Extensão <span class="float-right">{{i.ExtensaoKm}} km</span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Largura da faixa lado direito <span class="float-right">{{i.FaixaDominioLadoDireito}} m</span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Largura da faixa lado esquerdo <span class="float-right">{{i.FaixaDominioLadoEsquerdo}} m</span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Largura da faixa <span class="float-right">{{i.FaixaDominioLargura}} m</span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Checagem <span class="float-right badge">{{i.Checagem}} </span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Fonte <span class="float-right badge">{{i.Fonte}}</span>
			</span>
		</li>
		<li class="nav-item">
			<span class="nav-link">
				Arquivo KML 
				<span class="float-right">
					<a style="cursor: pointer;" @click="getKmlFaixa(i.CodigoFaixaDominio)">
						<i class="fa fa-download" aria-hidden="true"></i>
					</a>
				</span>
			</span>
		</li>
	</ul>
</div>
