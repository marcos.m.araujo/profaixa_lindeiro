<div v-for="i in historico" class="callout callout-success">
	<h5><i class="far fa-clock"></i> {{moment(String(i.Data)).format('DD/MM/YYYY HH:mm:ss')}}</h5>
	<strong>De: </strong>{{i.TramitadoPor}}<br>
	<strong>Para: </strong>{{i.TramitadoPara}}<br>
	<strong class="acao">{{i.NomeAcao}}</strong>

	<p>{{i.ObservacaoTramitacao}}</p>
</div>
