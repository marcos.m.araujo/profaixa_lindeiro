<template id="template-mapa-faixa-from-kml">
	<!--End input search-->
	<div class="row">
		<div style="text-align: center; align-items: center;" class="col-md-12">
			<div id="mapkmlfaixaid" class="map-height">
				<!-- Pesquisa por municipio -->
				<div id="buscaM" class="buscaM">
					<div class="col-lg-4 col-10" id="pesquisaMunicipio">
						<div class="input-group md-form form-sm form-2 pl-0">
							<!-- campo de pesquisa -->
							<input class="form-control" type="text" size="140"
								placeholder="Digite o nome do Município" id="inputMunicKml" list="inptmunkml" name="inptmunkml"
								@keypress="getMunicipio()">
							<!-- lista com municipios pesquisados -->
							<datalist id="inptmunkml">
								
							</datalist>
							<!-- botoes -->
							<div class="input-group-append">
								<!-- incluir municipio -->
								<button class="btn btn-info btn-sm btnBuscarM" @click="getMunicipioSelect()" title="Consultar município">
									<i class="fas fa-search text-grey" aria-hidden="true"></i>
								</button>
								<!-- excluir municipio -->
								<button title="Remover município do mapa" @click="clearMunicipioSelect()"
									style='display: none' class="btn btn-sm btn-danger btnClearBuscarMkml">
									<i class="fas fa-trash-alt"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
				<!-- FIM Pesquisa por municipio -->
				
				<button @click="leafletVue.zoomHome()" class="btnMapa btnHome"><i class="fas fa-home"></i>
				</button>

				<button @click="showImagemSatelite = !showImagemSatelite" 
					:style="[showImagemSatelite ? {'font-weight': 'bold', 'background-color': '#fff'} : {'font-weight': 'bold', 'background-color': '#BDBDBD'}]" 
					class="btnMapa baseMapa"
				>
					<i class="fas fa-map"></i>
				</button>

				<button @click="showLayerSNV = !showLayerSNV" title="Rodovias" id="baseRodovia" 
					:style="[showLayerSNV ? {'background-color':'#fff','font-weight':'bold'} : {'background-color':'#BDBDBD','font-weight':'normal'}]" 
					class="btnMapa baseRodovia"
				>
					SNV
				</button>
				
			</div>
		</div>
	</div>
</template>

<?php $this->load->view('template_adminlte3/script_leaflet'); ?>

<script>
var mapaFromKml = Vue.component('mapa-faixa-from-kml',{
    template: '#template-mapa-faixa-from-kml',
	props: {
		codigofaixaapublicar: Number,
		kml: String,
	},
	data() {
		return {
			mymap: null,
			
			layerEstados: {},
			layerMunicipios: {},
			
			showImagemSatelite: false,
			layerSNV: {},
			showLayerSNV: false,

			layerFaixa: {},
			listaFaixa: null,
			showLayerFaixa: true,	

			loading: true,
		}
	},
	computed: {
		isMobile: function(){
			var check = false;
			(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
			return check;
		},
	},
	watch:{
		loading(val){
			if(val === true) {
				$('#preloader .inner').fadeOut();
				$('#preloader').delay(350).fadeOut('slow');
				$('body').delay(350).css({
					'overflow': 'visible'
				});
			} else {
				$('#preloader').hide();
			}
		},
		showImagemSatelite: function(val){
			leafletVue.changeViewSatelite(val);
		},
		showLayerSNV: function(val){
			this.layerSNV = leafletVue.plotLayerSNV(this.params, val, '#38f404');				
		},
		showLayerFaixa: async function(val){
			await this.plotLayerFaixaFromKml()		
		},
	},
	async mounted(){
		await this.gerarMapa('mapkmlfaixaid');
	},
    methods: {
		// Gerador de mapa
		async gerarMapa(idMapa) {
			
			this.loading = true;

			this.mymap = await leafletVue.gerarMapa(idMapa, true);
			
			// $('.polyline-measure-unicode-icon').parent().css('top','230px');
			
			// cria os layers necessarios
			this.layerMunicipios = L.geoJSON().addTo(this.mymap);
			this.layerEstados = L.geoJSON().addTo(this.mymap);
			this.layerSNV = L.geoJSON().addTo(this.mymap);

			this.layerFaixa = L.geoJSON().addTo(this.mymap);
			
			this.loading = false;
			
			await this.consultaLayers();
		},
		// Botões de cálculos de metros no mapa
		controlPolyMeasure() {
				
			L.control.polylineMeasure({
				position: 'bottomleft',
				unit: 'metres',
				showBearings: true,
				clearMeasurementsOnStop: true,
				showClearControl: true,
				showUnitControl: true
			}).addTo(this.mymap);
			
		},
		
		// busca municipio pelo nome, monta lista com nomes parecidos            
		async getMunicipio() {

			var hsp = $("#inputMunicKml").val();
			if (hsp.length > 2) {
				var controller = 'Municipio/getMunicipiosGeo';
				var params = {municipio: hsp};
				var data = await vmGlobal.getFromController(controller, params);

				$('#inptmunkml').empty();
				for (var i = 0; i < data.length; i++) {
					var hosp = data[i];
					$("#inptmunkml").append('<option value="' + hosp.MunicipioEstado + '">' + hosp.MunicipioEstado +"</option>");
				}
			}

		},
		// adiciona marcador em cima do municipio selecionado
		async getMunicipioSelect() {
		var m = $("#inputMunicKml").val();
		if (m != "") {
			
			var controller = 'Municipio/getMunicipiosCord';
			var params = {municipio: m};
			var data = await vmGlobal.getFromController(controller, params);

			leafletVue.addLayer(data[0].Coordenada, m, '',this.layerMunicipios);
			$(".btnClearBuscarMkml").css('display', 'block')
			
			return this.layerMunicipios;
		}
		},
		// retira do mapa municipios selecionados
		clearMunicipioSelect(input, clearClass) {
			this.layerMunicipios.clearLayers()
			$("#inputMunicKml").val('')
			$(".btnClearBuscarMkml").css('display', 'none')
		},
		// recupera dados e monta layer selecionados
		async consultaLayers() {
			
			this.loading = true;
			
			var controller = 'Municipio/getCoordsLayerEstados';
			var data = await vmGlobal.getFromController(controller, {});

			this.layerEstados.clearLayers()

			for (var i = 0; i < data.length; i++) {
				var cord = data[i];
				this.addLayerEstados(cord.coordenada, cord.UF, .0, this.layerEstados, 'black')
			}
			
			// adiciona os dados dos layers
			leafletVue.plotLayerSNV(this.params, this.layerSNV, this.showLayerSNV, '#38f404');
			
			await this.plotLayerFaixaFromKml();
			
			this.loading = false;			
		},
		// monta layer dos estados
		addLayerEstados(cord, uf, fillOpacity, layer, color) {
			
			var array = {
				"type": "FeatureCollection",
				"features": [{
					"type": "Feature",
					"geometry": JSON.parse(cord),
					"properties": {
						"OBJECTID": uf,
						"DADOS" : 'TEste'
					}
				}]
			}

			L.geoJson(array, {
				style: function (feature) {
					return {
						stroke: true,
						color: color,
						weight: 1,
						fillOpacity: fillOpacity,
					};
				},
				/**
				 * todo -> precisa emitir UF para filtro
				 */
				onEachFeature: (feature, l) => {
					var vm = this;
					l.on('contextmenu', function (e) {
						vm.$emit('emituf', feature.properties.OBJECTID);
					});
					l.on("dblclick", function (e) {
						vm.$emit('emituf', '');							
					});
				}
			}).addTo(layer)
			
		},		
		async plotLayerFaixaFromKml(){
			
			this.layerFaixa.clearLayers();
			
			if(this.showLayerFaixa){	
				
				var controller = 'GestaoConflitosFaixa/getJsonFromKml';
				var data = await vmGlobal.getFromController(controller, {Arquivo: this.kml});

				this.listaFaixa = data;
				
				var concatenado = '<div><strong>TRECHO DA FAIXA DE DOMÍNIO A SER PUBLICADO</strong> <br>';
				leafletVue.addLayer(data, concatenado, '#00a6ff', this.layerFaixa);
				
				this.mymap.setView(this.layerFaixa.getBounds().getCenter())
				this.mymap.fitBounds(this.layerFaixa.getBounds())
				
			}
		},
    }
});
</script>


<style scoped>
	.map-height {
        width: 100%;
        height: 700px;
        z-index: 1
    }
	
	.btnMapa {
		position: absolute;
		left: 10px;
		padding: 8px;
		text-align: center;
		z-index: 1000;
		width: 34px;
		background-color: #FFFF;
		border-radius: 4px;
		cursor: pointer;
	}
	.btnHome {
		top: 85px;
		left: 10px;
	}
	
	.baseMapa {
		top: 121px;
		left: 10px;
		height: 35px;
	}

	.baseRodovia {
		font-size: 5pt;
		top: 155px;
		left: 10px;
		height: 33px;

	}

    .baseConflito {
        top: 197px;
    }

    .baseFaixa {
        top: 233px;
    }

    .baseImovel {
        top: 269px;
    }

	
    .buscaM {
        position: absolute;
        top: 4px;
        left: 60px;
        padding: 8px;
        text-align: center;
        z-index: 1000;
    }

    .buscaM input {
        width: 350px;
        height: 30px;
        border-radius: 4px;
        z-index: 1000;
    }

    .buscaM .btnBuscarM {
        width: 40px;
        height: 30px;
        border-radius: 4px;
        z-index: 1000;
    }

	
    .buscaM .btnClearBuscarMkml {
        width: 40px;
        height: 30px;
        border-radius: 4px;
        z-index: 1000;
    }


	#preloader {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(255, 255, 255, 0.5);
        /* cor do background que vai ocupar o body */
        z-index: 999;
        /* z-index para jogar para frente e sobrepor tudo */
    }
 
    #preloader .inner {
        position: absolute;
        top: 50%;
        /* centralizar a parte interna do preload (onde fica a animação)*/
        left: 50%;
        transform: translate(-50%, -50%);
    }
</style>

<link rel="stylesheet" href="<?= base_url('template_lte3/plugins/leaflet/leaflet.css') ?>">
<link rel="stylesheet" href="<?= base_url('template_lte3/plugins/leaflet/draw/src/leaflet.draw.css') ?>">
<link rel="stylesheet" href="https://ppete2.github.io/Leaflet.PolylineMeasure/Leaflet.PolylineMeasure.css" />
<link rel="stylesheet" href="<?= base_url('template_lte3/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') ?>">
