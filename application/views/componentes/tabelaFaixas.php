<table id="tableFaixas" class="dataTables_wrapper dt-bootstrap4 no-footer" cellspacing="0" width="100%">
    <thead>
		<!-- <tr>
			<td colspan="8" style="text-align: center;">Extensão Total: {{somatorioFxApublicarExtensao}} kms</td>
		</tr> -->
        <!-- <tr>
            <th></th>
            <th>UF</th>
            <th>BR</th>
            <th></th>
			<th></th>
			<th></th>
            <th>Status</th>
            <th>Tramitado Por</th>
		</tr> -->
		<tr>
            <th>Abrir Mapa</th>
            <th>UF</th>
            <th>BR</th>
            <th>Km Inicial</th>
			<th>Km Final</th>
			<th>Extensão</th>
            <th>Status</th>
            <th>Tramitado Por</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="i in listaFaixasPublicacao">
			<td style="text-align: center;">
				<a style="cursor: pointer;" 
					@click="pointMap(null,i.CodigoFaixa)">
					<i class="fas fa-map-marker-alt"></i>
				</a>
			</td>
            <td>{{i.UF}}</td>
            <td>{{i.BR}}</td>
            <td>{{i.KmInicialTrechoPublicacao}}</td>
			<td>{{i.KmFinalTrechoPublicacao}}</td>
			<td>{{(i.KmFinalTrechoPublicacao > i.KmInicialTrechoPublicacao) ? i.KmFinalTrechoPublicacao - i.KmInicialTrechoPublicacao : i.KmInicialTrechoPublicacao - i.KmFinalTrechoPublicacao}}</td>
            <td>{{(i.Status == null) ? 'Para Aprovação' : i.Status}}</td>
            <td>{{i.TramitadoPor}}</td>
		</tr>
	</tbody>
	<tfoot align="right">
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</tfoot>
</table>
