
<section class="content-header">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-sm-6">
                <h3></h3>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section id="conflitos" class="content">
    <div class="container-fluid">
		
		<div class="row">
			<div class="col-md-12 d-flex justify-content-center" style="top: 50px">
				<img src="<?=base_url('img_dpp/profaixa-logo2.png')?>" alt="Programa de Faixa de Domínio" width="220px">
			</div>
			<div class="col-md-12 d-flex justify-content-center" style="top: 50px"	>
				<h3>Programa Federal de Faixas de Domínio</h3>
			</div>

			<div class="col-md-12 d-flex justify-content-center mb-2" style="top: 100px">
				
				<h2>Lindeiro</h2>
			</div>

			
			<div class="col-md-12 d-flex justify-content-center mb-2" style="top: 150px">
				<div class="col-md-3">
					<a href="<?=base_url('Lindeiro/consulta')?>">
						<button type="button" class="btn btn-block btn-default">Consultar Situação do Imóvel</button>
					</a>
				</div>
				<div class="col-md-3">
					<a href="<?=base_url('Lindeiro/cadastro')?>">
						<button type="button" class="btn btn-block btn-default">Cadastro do Imóvel</button>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
