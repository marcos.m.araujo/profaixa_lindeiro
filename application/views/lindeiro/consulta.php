<style>
	.control-label {
		text-align: right;
	}
</style>

<section class="content-header">
	<div class="container-fluid">
		<div class="row ">
			<div class="col-sm-6">
				<h3></h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="<?= base_url('Lindeiro/index') ?>">Home</a></li>
					<li class="breadcrumb-item">Consulta</li>
				</ol>
			</div>
		</div>
	</div>
</section>

<section id="conflitos" class="content">
	<div class="container-fluid">

		<div class="row">
			<div class="col-md-12 d-flex justify-content-center" style="top: 50px">
				<div class="col-md-6">
					<!-- general form elements -->
					<div class="card card-default">
						<div class="card-header">
							<h3 class="card-title">Situação do Imóvel</h3>
						</div>
						<!-- /.card-header -->
						<!-- form start -->
						<div class="card-body">
							<form class="form-horizontal">

								<div class="form-group row">
									<label for="cpf" class="col-sm-3 control-label">CPF</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" id="cpf" placeholder="000.000.000-00">
									</div>
								</div>
								<div class="form-group row">
									<label for="email" class="col-sm-3 control-label">Email</label>
									<div class="col-sm-5">
										<input type="email" class="form-control" id="email" placeholder="Email">
									</div>
								</div>
								<!-- /.card-body -->
							</form>
						</div>
						<!-- /.card -->
						
					<div class="card-footer">
						<button type="submit" class="btn btn-info float-right">Entrar</button>
					</div>
					<!-- /.card-footer -->
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
