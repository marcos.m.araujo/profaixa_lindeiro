<style>
	.control-label {
		text-align: right;
	}
</style>

<section class="content-header">
	<div class="container-fluid">
		<div class="row ">
			<div class="col-sm-6">
				<h3></h3>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="<?= base_url('Lindeiro/index') ?>">Home</a></li>
					<li class="breadcrumb-item">Cadastro</li>
				</ol>
			</div>
		</div>
	</div>
</section>

<section class="content">
	<div class="container-fluid">
		<div class="row d-flex justify-content-center">
			<div class="col-md-5">
				<!-- general form elements -->
				<div class="card card-default">
					<div class="card-header">
						<h3 class="card-title">Cadastro do Imóvel</h3>
					</div>
					<!-- /.card-header -->
					<!-- form start -->
					<div class="card-body">
						<form class="form-horizontal">

							<div class="form-group row  required">
								<label for="nome" class="col-sm-3 control-label">Nome Completo</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" v-model="NomeCompleto" placeholder="Nome Completo" required>
								</div>
							</div>

							<div class="form-group row required">
								<label for="cpf" class="col-sm-3 control-label">CPF</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" id="CPF" v-model="CPF" placeholder="000.000.000-00" required>
								</div>
							</div>

							<div class="form-group row required">
								<label for="email" class="col-sm-3 control-label">Email</label>
								<div class="col-sm-9">
									<input type="email" class="form-control" v-model="Email" placeholder="Email" required>
								</div>
							</div>

							<div class="form-group row required">
								<label for="cep" class="col-sm-3 control-label">CEP</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" v-model="CEP" id="CEP" placeholder="00000-000" required>
								</div>
							</div>

							<div class="form-group row required">
								<label for="estado" class="col-sm-3 control-label">Estado</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" v-model="Estado" required>
								</div>
							</div>

							<div class="form-group row required">
								<label for="municipio" class="col-sm-3 control-label">Município</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" v-model="Municipio" required>
								</div>
							</div>

							<div class="form-group row required">
								<label for="endereco" class="col-sm-3 control-label">Endereço</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" v-model="Endereco" required>
								</div>
							</div>

							<div class="form-group row required">
								<label for="numero" class="col-sm-3 control-label">Número</label>
								<div class="col-sm-3">
									<input type="text" class="form-control" v-model="numero" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="complemento" class="col-sm-3 control-label">Complemento</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" v-model="Complemento" required>
								</div>
							</div>

							<div class="form-group row required">
								<label for="inscricaoImovel" class="col-sm-3 control-label">N° de Inscrição do Imóvel</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" v-model="NumeroInscricaoImovel" required>
								</div>
							</div>

							<div class="form-group row required">
								<label for="arquivo" class="col-sm-3 control-label">KML Imóvel</label>
								<div class="col-sm-9">
									<input type="file" name="" id="Arquivo" ref="Arquivo" accept=".kml" required>
								</div>
							</div>

						</form>
					</div>
					<!-- /.card-body -->
					<div class="card-footer">
						<button type="submit" class="btn btn-info float-right">Enviar</button>
					</div>
					<!-- /.card-footer -->
				</div>
			</div>
			<!-- /.card -->
		
		</div>
	</div>
</section>
<script>
$('#CPF').mask("000.000.000-00", { placeholder: "000.000.000-00" });
$('#CEP').mask("00000-000", { placeholder: "00000-000" });
</script>
