<?php

class GestaoConflitosModel extends CI_Model
{

    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('DPP', true);
        $this->db3 = $this->load->database('CGDR', true);
    }


    public function getFaixaDominioOperacional($dados)
    {

		$sql = "SELECT * 
		FROM (
			SELECT * FROM 
			tblProfaixaFaixaDominioOperacional 
			WHERE 1=1";

			if (!empty($dados['CodigoFaixaDominio']))
				$sql .= ' AND CodigoFaixaDominio = ' . $dados['CodigoFaixaDominio'];
			if (!empty($dados['uf']))
				$sql .= ' AND UF = ' . $dados['uf'];
			if (!empty($dados['br']))
				$sql .= ' AND FaixaDominioBR = \''. str_replace('BR-','', $dados['br']).'\'';

			if(isset($dados['FaixasIn']) && $dados['FaixasIn'] != '') 
				$sql .= " AND CodigoFaixaDominio in (". implode(',', $dados['FaixasIn']) .")";
			
		$sql .= ") as  faixas
		CROSS APPLY (
			SELECT COUNT(CodigoConflito) as total FROM tblProfaixaConflitos WHERE CodigoFaixaDominio = faixas.CodigoFaixaDominio
		) totalConflitos";

      
		$query = $this->db3->query($sql);
        return $query->result_array();
	}

	public function getListaFaixasPublicacao($dados)
	{
		
		$SQL = "SELECT * FROM (
			SELECT MAX(CodigoFaixaApublicar) as CodigoFaixaApublicar  FROM  [tblProfaixaFaixaDominioAcaoTramitada] group by CodigoFaixa, KmInicialTrechoPublicacao, KmFinalTrechoPublicacao
		) as faixas
		OUTER APPLY (
		SELECT
			[CodigoFaixaApublicar]
			,[CodigoFaixa]
			,[Data]
			,[TramitadoPor]
			,[TramitadoPara]
			, CAST([ObservacaoTramitacao] AS VARCHAR(MAX)) AS [ObservacaoTramitacao]
			,[NomeArquivoFaixa]
			,[ArquivoFaixa]
			, CAST([CoordenadaAntigaFaixa] AS VARCHAR(MAX)) AS [CoordenadaAntigaFaixa]
			, CAST([CoordenadaTrechoPublicacao] AS VARCHAR(MAX)) AS [CoordenadaTrechoPublicacao]
			,[Status]
			,[ArquivoTrecho]
			,[NomeArquivoTrecho]
			,[KmFinalTrechoPublicacao]
			,[KmInicialTrechoPublicacao]
			,UF
			,BR
			,FaixaDominioBR
			,FaixaDominioLadoDireito
			,FaixaDominioLadoEsquerdo
			,FaixaDominioLargura
		FROM [tblProfaixaFaixaDominioAcaoTramitada]
		LEFT JOIN [tblProfaixaFaixaDominioOperacional]  ON
		[tblProfaixaFaixaDominioAcaoTramitada].CodigoFaixa = [tblProfaixaFaixaDominioOperacional].CodigoFaixaDominio
		
		WHERE [tblProfaixaFaixaDominioAcaoTramitada].CodigoFaixaApublicar = faixas.CodigoFaixaApublicar
		) as detalhe WHERE 1=1";
		
		
		if (!empty($dados['CodigoFaixaDominio']))
			$SQL .= ' AND CodigoFaixa = ' . $dados['CodigoFaixaDominio'];
		if (!empty($dados['uf']))
			$SQL .= ' AND UF = \'' . $dados['uf'] . '\'';
		if (!empty($dados['br']))
			$SQL .= ' AND BR = \''. $dados['br'].'\'';

		if(isset($dados['FaixasIn']) && $dados['FaixasIn'] != '') 
			$SQL .= " AND CodigoFaixa in (". implode(',', $dados['FaixasIn']) .")";
			
		$query = $this->db3->query($SQL);
        return $query->result_array();
	}

	public function setAndamentoPublicacaoFaixa($dados, $cord = null){
		

		$this->db3->select("*", false);
		$this->db3->from('tblProfaixaFaixaDominioAcaoTramitada');
		$this->db3->join('tblProfaixaFaixaDominioOperacional', 'tblProfaixaFaixaDominioOperacional.CodigoFaixaDominio = tblProfaixaFaixaDominioAcaoTramitada.CodigoFaixa');
		$this->db3->where(['CodigoFaixaApublicar' => $dados['CodigoFaixaApublicar']]);
		$query1 = $this->db3->get();
		$result = $query1->result_array();
		
		$data = [];
		$data["CodigoFaixa"] = $result[0]["CodigoFaixa"]; 
		$data["KmInicialTrechoPublicacao"] = $result[0]["KmInicialTrechoPublicacao"]; 
		$data["KmFinalTrechoPublicacao"] = $result[0]["KmFinalTrechoPublicacao"]; 
		$data["NomeArquivoFaixa"] = $result[0]["NomeArquivoFaixa"]; 
		$data["ArquivoFaixa"] = $result[0]["ArquivoFaixa"]; 
		$data["CoordenadaAntigaFaixa"] = $result[0]["CoordenadaAntigaFaixa"]; 
		$data["CoordenadaTrechoPublicacao"] = $result[0]["CoordenadaTrechoPublicacao"]; 
		$data["ArquivoTrecho"] = $result[0]["ArquivoTrecho"]; 
		$data["NomeArquivoTrecho"] = $result[0]["NomeArquivoTrecho"];

		$data['Status'] = $dados['Status'];
		$data["TramitadoPor"] = $dados["TramitadoPor"]; 
		$data['Data'] = date('Y-m-d H:m:s');

		if(isset($dados['TramitadoPara']))
			$data['TramitadoPara'] = $dados['TramitadoPara'];
			
		if(isset($dados["ObservacaoTramitacao"]))
			$data['ObservacaoTramitacao'] = $dados['ObservacaoTramitacao'];
			
		if($dados['Status'] == 'Sugestão em Homologação'){
			if(isset($dados['NomeArquivoTrecho']))
				$data['NomeArquivoTrecho'] = $dados['NomeArquivoTrecho'];

			if(isset($dados['NomeArquivoFaixa']))	
				$data['NomeArquivoFaixa'] = $dados['NomeArquivoFaixa'];
			
			if(isset($dados['ArquivoFaixa']))
				$data['ArquivoFaixa'] = $dados['ArquivoFaixa'];

			if(isset($dados['ArquivoTrecho']))				
				$data['ArquivoTrecho'] = $dados['ArquivoTrecho'];
		}
		
		if($this->db3->insert('tblProfaixaFaixaDominioAcaoTramitada', $data)){

			
			if($dados['Status'] == 'Sugestão Homologada'){

				$kml = file_get_contents($data['ArquivoTrecho']);
				$geom = geoPHP::load($kml, 'kml');
				$cord = $geom->out('json');

				$dadosHomologados = [
					'CodigoFaixaApublicar' => $dados['CodigoFaixaApublicar'],
					'CodigoFaixaDominio' => $data['CodigoFaixa'],
					'UF' => $result[0]['UF'],
					'BR' => $result[0]['BR'],
					'KmInicial' => $data['KmInicialTrechoPublicacao'],
					'KmFinal' => $data['KmFinalTrechoPublicacao'],
					'Coordenada' => $cord
				];
				$this->db3->insert('tblProfaixaFaixaDominioPublicada', $dadosHomologados);
			}


			return [
				'status' => true,
				'message' => 'dados inseridos com sucesso'
			];

		} else {
			$db_error = $this->db3->error();
			return [
				'status' => false,
				'message' => 'Erro ao tentar inserir dados. Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']
			];
		}
	}
	
	public function getFaixaConflitos($dados)
    {
		$SQL = "SELECT conflitos.*, ultimaAcao.TramitadoPor, ultimaAcao.TramitadoPara, 
		ultimaAcao.NomeAcao, ultimaAcao.Data, ultimaAcao.ArquivoFaixa, ultimaAcao.ArquivoConflito, 
		ultimaAcao.ArquivoImovel, ultimaAcao.CodigoAcaoTramitada 
		FROM
		(
			SELECT tblProfaixaConflitos.*, tblProfaixaFaixaDominioOperacional.UF FROM BD_CGDR.dbo.tblProfaixaConflitos
			
			LEFT JOIN BD_CGDR.dbo.tblProfaixaFaixaDominioOperacional
			on tblProfaixaConflitos.CodigoFaixaDominio = tblProfaixaFaixaDominioOperacional.CodigoFaixaDominio
			
			LEFT JOIN [BD_CGDR].[dbo].[tblProfaixaImoveisConsolidado]
			ON [tblProfaixaConflitos].CodigoEmpreendimento = [tblProfaixaImoveisConsolidado].CodigoEmpreendimento

			WHERE 1=1 ";
		if(isset($dados['tipo'])){
			$SQL .= " AND cast(tblProfaixaConflitos.StatusConflito as varchar) = '".$dados['tipo']."'";
		}
		if(isset($dados['CodigoConflito']) && $dados['CodigoConflito'] != ''){
			$SQL .= " AND tblProfaixaConflitos.CodigoConflito = " . $dados['CodigoConflito'];
		}
			
		if(isset($dados['ConflitosIn']) && $dados['ConflitosIn'] != ''){
			$SQL .= " AND tblProfaixaConflitos.CodigoConflito IN (" . implode(',',$dados['ConflitosIn']) .')';
		}
			
		if(isset($dados['FaixasIn']) && $dados['FaixasIn'] != ''){
			$SQL .= " AND tblProfaixaConflitos.CodigoFaixaDominio IN (" . implode(',',$dados['FaixasIn']) .')';
		}
		if(isset($dados['uf']) && $dados['uf'] != ''){
			$SQL .= " AND tblProfaixaFaixaDominioOperacional.UF = '" . $dados['uf'] . "'";
		}
		if (!empty($dados['br'])){
			$SQL .= " AND tblProfaixaFaixaDominioOperacional.BR = '" . $dados['br'] . "'";
		}
		if (!empty($dados['Municipio'])){
			$SQL .= " AND [tblProfaixaImoveisConsolidado].Municipio = '" . $dados['Municipio'] . "'";
		}
		
		$SQL .= "
		) as conflitos
		
		OUTER APPLY (
			SELECT TOP(1) tblProFaixaConflitosAcaoTramitada.*, tblProFaixaConflitosAcao.NomeAcao
			from BD_CGDR.dbo.tblProFaixaConflitosAcaoTramitada
			left join BD_CGDR.dbo.tblProFaixaConflitosAcao
			on tblProFaixaConflitosAcaoTramitada.CodigoAcao = tblProFaixaConflitosAcao.CodigoAcao
			where CodigoConflito = conflitos.CodigoConflito
			ORDER BY Data DESC
		) as ultimaAcao
		ORDER BY conflitos.CodigoConflito";
		
		$query = $this->db2->query($SQL);
        return $query->result_array();
    }
    public function getFaixaImoveis($dados)
    {
        $this->db2->select("tblProfaixaImoveisConsolidado.*", false);
		$this->db2->from('BD_CGDR.dbo.tblProfaixaImoveisConsolidado');
		$this->db2->join('BD_CGDR.dbo.tblProfaixaConflitos', 'tblProfaixaImoveisConsolidado.CodigoConflito = tblProfaixaConflitos.CodigoConflito');
		$this->db2->join('BD_CGDR.dbo.tblProfaixaFaixaDominioOperacional', 'tblProfaixaConflitos.CodigoFaixaDominio = tblProfaixaFaixaDominioOperacional.CodigoFaixaDominio');

		
		if (!empty($dados['uf']))
			$this->db2->where('tblProfaixaFaixaDominioOperacional.UF', $dados['uf']);
		
		if (!empty($dados['br']))
			$this->db2->like('tblProfaixaFaixaDominioOperacional.FaixaDominioBR', str_replace('BR-','', $dados['br']));
			
		if(!empty($dados['FaixasIn'])){
			$this->db2->where_in('tblProfaixaFaixaDominioOperacional.CodigoFaixaDominio', $dados['FaixasIn']);
		}
        $query = $this->db2->get();
        return $query->result_array();
    }


    public function abrirDadosImovel($dados)
    {
        $this->db2->select("*");
        $this->db2->from('BD_CGDR.dbo.' . $dados['Fonte']);
        $this->db2->where('Codigo', $dados['Codigo']);
        $query = $this->db2->get();
        return $query->result_array();
	}
	
	public function getQuantitativo(){
		$sql ="SELECT CAST(StatusConflito as varchar) as status, COUNT(*) as qtd
		FROM [BD_CGDR].[dbo].[tblProfaixaConflitos]
		GROUP BY CAST(StatusConflito as varchar) ";
        $query = $this->db2->query($sql);
        return $query->result_array();
	}

	public function getTramitadosPara($email){
		$sql = "Select * from [BD_CGDR].[dbo].[tblProfaixaConflitosAcaoTramitada]
		LEFT JOIN [BD_CGDR].[dbo].[tblProfaixaConflitos] ON [tblProfaixaConflitosAcaoTramitada].CodigoConflito = [tblProfaixaConflitos].CodigoConflito
		LEFT JOIN [BD_CGDR].[dbo].tblProfaixaFaixaDominioOperacional ON [tblProfaixaConflitosAcaoTramitada].CodigoFaixa = tblProfaixaFaixaDominioOperacional.CodigoFaixaDominio
				where CodigoAcaoTramitada IN 
				(
					SELECT MAX([CodigoAcaoTramitada])
				  FROM [BD_CGDR].[dbo].[tblProfaixaConflitosAcaoTramitada]
				  group by CodigoConflito
				)
		AND TramitadoPara = '".$email."'";
        $query = $this->db2->query($sql);
        return $query->result_array();
	}

	
	public function getUltimaAcao($codigoConflito){

		// busca ultima acao tramitada e os arquivos 
		$sql1 = "SELECT TOP 1 CodigoAcaoTramitada
					,[CodigoConflito]
					,[CodigoFaixa]
					,[CodigoImovel]
					,[ArquivoConflito]
					,[ArquivoFaixa]
					,[ArquivoImovel]
					
				FROM [BD_CGDR].[dbo].[tblProfaixaConflitosAcaoTramitada]
			
				where CodigoConflito = $codigoConflito
			
				ORDER BY CodigoAcaoTramitada DESC";
		
        $query = $this->db2->query($sql1);
        return $query->result_array();
	}
	
	public function getConflitosAcaoTramitada($dados)
	{

        $this->db3->select("*", false);
		$this->db3->from('tblProfaixaConflitosAcaoTramitada');
		$this->db3->join('tblProfaixaFaixaDominioOperacional', 'tblProfaixaConflitosAcaoTramitada.CodigoFaixa = tblProfaixaFaixaDominioOperacional.CodigoFaixaDominio');
		$this->db3->where('CodigoAcaoTramitada', $dados['CodigoAcaoTramitada']);
		
        $query = $this->db3->get();
        return $query->result_array();
	}

	
	public function getFaixasAcaoTramitada($dados)
	{

        $this->db3->select("*", false);
		$this->db3->from('tblProfaixaFaixaDominioAcaoTramitada');
		$this->db3->join('tblProfaixaFaixaDominioOperacional', 'tblProfaixaFaixaDominioAcaoTramitada.CodigoFaixa = tblProfaixaFaixaDominioOperacional.CodigoFaixaDominio');
		$this->db3->where('CodigoFaixaApublicar', $dados['CodigoFaixaApublicar']);
		
        $query = $this->db3->get();
        return $query->result_array();
	}

	public function atualizaCoordFaixa($json, $cod){
		$this->db2->set('Coordenada', $json);
		$this->db2->where('CodigoFaixaDominio', $cod);
		$this->db2->update('BD_CGDR.dbo.tblProfaixaFaixaDominioOperacional');
		echo ' arquivo da faixa atualizado com sucesso. ';
	}

	
	public function atualizaCoordConflito($json, $cod){
		$this->db2->set('Coordenada', $json);
		$this->db2->where('CodigoConflito', $cod);
		$this->db2->update('BD_CGDR.dbo.tblProfaixaConflitos');
		echo 'arquivo do conflito atualizado com sucesso. ';
	}
	
	public function atualizaCoordImovel($json, $cod){
		$this->db2->set('Coordenada', $json);
		$this->db2->where('CodigoEmpreendimento', $cod);
		$this->db2->update('BD_CGDR.dbo.tblProfaixaImoveisConsolidado');
		echo 'arquivo do imovel atualizado com sucesso.';
	}

	public function insertAcaoFaixa($dados){
		$dados['CoordenadaTrechoPublicacao'] = json_encode($dados['CoordenadaTrechoPublicacao']);

		if( $this->db3->insert('tblProfaixaFaixaDominioAcaoTramitada', $dados) )
			return [
				'status' => true,
				'message' => 'dados inseridos com sucesso'
			];
		else 
		return [
			'status' => false,
			'message' => 'erro ao tentar inserir dados'
		];
	}
}
