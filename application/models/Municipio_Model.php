<?php

class Municipio_Model extends CI_Model
{

    private $db2;

    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('DPP', true);
	}
	
    public function getCoordsLayerEstados($uf)
    {
        $this->db2->select('coordenada, UF');
        $this->db2->from('Estados_GeoAlteryx');
        if (!empty($uf)) {
            $this->db2->where("UF", $uf);
        }
        $query = $this->db2->get();
        return $query->result_array();
    }
 
 	public function getMunicipiosGeo($dados)
    {
        $this->db2->select('distinct MunicipioEstado', false);
        $this->db2->from('MunicipiosGeoAlteryx');     
        $this->db2->like("MunicipioEstado", $dados['municipio']);
        $this->db2->limit(10);  
        $this->db2->order_by('MunicipioEstado asc'); 
        $query = $this->db2->get();
        return $query->result_array();
    }

    public function getMunicipiosCord($dados)
    {
        $this->db2->select('Coordenada');
        $this->db2->from('MunicipiosGeoAlteryx');     
        $this->db2->where("MunicipioEstado", $dados['municipio']); 
        $query = $this->db2->get();
        return $query->result_array();
	}
	
}
