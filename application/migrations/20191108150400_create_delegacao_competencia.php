<?php 
// Perceba os maiúsculos em Migration e na primeira letra do restante
class Migration_Create_delegacao_competencia extends CI_Migration
{
    public function up()
    {
        $sql = "CREATE TABLE tblDelegacaoCompetencia(
            RecordID [int] NOT NULL,
            CodigoDelegacao [int] NULL,
            UF [varchar](255) NULL,
            Contrato [varchar](255) NULL,
            Lote [varchar](255) NULL,
            TipoDelegacao [varchar](255) NULL,
            Objeto [varchar](999) NULL,
            NumeroPortariaDelegacao [varchar](255) NULL,
            NumeroBoletimAdministrativo [varchar](255) NULL,
            NumeroSei [varchar](255) NULL,
            DataRecebimento [date] NULL,
            DataConclusao [date] NULL,
            StatusDelegacao [int] NULL,
            ArquivoInformacaoSalvo [int] NULL,
            Empreendimento [varchar](999) NULL,
            SituacaoDelegacao [varchar](255) NULL,
            ExtensaoTotalPortaria [decimal](23, 1) NULL,
            DelegacaoLicitacao [varchar](255) NULL,
        PRIMARY KEY CLUSTERED 
        (
            [RecordID] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]";
        
        $this->db->query($sql);

        
        if(ENVIRONMENT != 'production'){
            $sql = "INSERT tblDelegacaoCompetencia (RecordID, CodigoDelegacao, UF, Contrato, Lote, TipoDelegacao, Objeto, NumeroPortariaDelegacao, NumeroBoletimAdministrativo, NumeroSei, DataRecebimento, DataConclusao, StatusDelegacao, ArquivoInformacaoSalvo, Empreendimento, SituacaoDelegacao, ExtensaoTotalPortaria, DelegacaoLicitacao)
                VALUES
                (1, 1, N'SC', N'16 00192/2019', N'Único', N'PROJETO', N'Delegação de Competência para análise e Aceitação dos projetos básico e executivo - obras de adequação para ampliação de capacidade, restauração e eliminação de pontos críticos na rodovia BR-163/SC.', N'', N'', N'50616.002802/2019-11', CAST(N'2019-07-15' AS Date), NULL, 1, 0, N'BR-163/SC - Contratação Integrada dos Serviços de Elaboração dos Projetos Básico/Executivo e Construção das Obras de Adequação para Ampliação da Capacidade, Restauração e Retirada de Pontos Críticos.', N'Delegação de Competência Indeferida', CAST(47.6 AS Decimal(23, 1)), N'Não'),
                (2, 2, N'RS', N'10 00253/2018', N'Único', N'ANTEPROJETO', N'Delegação de Competência Plena e as Responsabilidades Decorrentes para ANÁLISE E ACEITAÇÃO DE ANTEPROJETO DE ENGENHARIA visando à Contratação Integrada de Empresa para Elaboração do Projeto de Engenharia e Execução das Obras de Restauração Rodoviária na BR-158/RS.', N'', N'', N'50610.003116/2019-17', CAST(N'2019-05-14' AS Date), NULL, 1, 1, N'Contratação Integrada de Empresa para Elaboração do Projeto de Engenharia e Execução das Obras de Restauração Rodoviária na BR-158/RS', N'Não Conforme', CAST(42.1 AS Decimal(23, 1)), N'Não'),
                (3, 3, N'RS', N'10 00029/2017', N'Único', N'PROJETO', N'Delegação de Competência Plena e as Responsabilidades Decorrentes para ANÁLISE E ACEITAÇÃO DE PROJETO EXECUTIVO DE ENGENHARIA visando à Contratação de Empresa para Execução das Obras de Restauração da Avenida Santa Tecla na BR-293/RS', N'PORTARIA Nº 5987', N'BA Nº 170 de 03/09/2019', N'50610.004791/2019-55', CAST(N'2019-07-15' AS Date), CAST(N'2019-09-03' AS Date), 2, 1, N'PROJETO EXECUTIVO DE ENGENHARIA visando à Contratação de Empresa para Execução das Obras de Restauração da Avenida Santa Tecla na BR-293/RS', N'Delegação de Competência Deferida', CAST(4.3 AS Decimal(23, 1)), N'Não'),
                (4, 4, N'RN', N'A LICITAR', N'Único', N'PROJETO', N'Delegação de Competência Plena Elaboração de Projeto Executivo de Engenharia para Construção de Rede de Ciclovias em Travessias Urbanas da BR-101, BR-226 e BR-406 na Região Metropolitana de Natal.', N'PORTARIA Nº 6623', N'BA Nº 192 de 03/10/2019', N'50614.001789/2019-94', CAST(N'2019-09-02' AS Date), CAST(N'2019-10-01' AS Date), 2, 1, N'Elaboração de Projeto Executivo de Engenharia para Construção de Passarelas de Pedestres na BR-304/RN e rojeto Executivo de Engenharia para Execução das Obras Remanescentes e Complementares de Adequação de Capacidade, Melhoramentos, Segurança de Tráfego, Eliminação de Pontos Críticos, Ampliação de Vias Marginais e Construção de Obras de Arte Especiais e Passarelas da BR-101/RN.', N'Delegação de Competência Deferida', CAST(83.8 AS Decimal(23, 1)), N'Sim'),
                (5, 5, N'PE', N'A LICITAR', N'Único', N'PROJETO', N'Delegação de competência para análise dos projetos básico e executivo de engenharia para a execução da obra de duplicação para adequação da capacidade da BR-428/PE km 183,00 ao km 188,00 na travessia urbana do município de Petrolina /PE,', N'PORTARIA Nº 6835', N'BA_Nº 198 de 11/10/2019', N'50604.007554/2019-25', CAST(N'2019-09-25' AS Date), CAST(N'2019-10-09' AS Date), 2, 1, N'Duplicação da BR-423/PE lotes 1 e 2 e Contratação Integrada de Empresa para desenvolvimento de Projetos Básico e Executivo para Implantação de Viadutos na BR-428, trecho travessia urbana de Petrolina', N'Delegação de Competência Deferida', CAST(5.0 AS Decimal(23, 1)), N'Sim'),
                (6, 6, N'MS', N'19 00207/2019', N'Único', N'PROJETO', N'Delegação de Competência Plena para a elaboração, análise e aprovação do Projeto Básico/Executivo, bem como licitar, homologar, adjudicar e representar esta Autarquia na lavratura e assinatura do contrato e termos aditivos, apostilamento, referente às obras de Melhorias do Segmento Crítico na Travessia Urbana de Dourados/MS, na BR- 463/MS.', N'', N'', N'50619.000289/2018-12', CAST(N'2019-07-16' AS Date), NULL, 1, 1, N'BR-463/MS - Projeto Básico/Executivo, bem como licitar, homologar, adjudicar e representar esta Autarquia na lavratura e assinatura do contrato e termos aditivos, apostilamento, referente às obras de Melhorias do Segmento Crítico na Travessia Urbana de Dourados/MS', N'Em Análise pela DPP', CAST(7.4 AS Decimal(23, 1)), N'Não'),
                (7, 7, N'MS', N'19 00206/2019', N'Único', N'PROJETO', N'Delegação de Competência para elaboração, análise e aprovação do Projeto Básico/Executivo, bem como licitar, homologar, adjudicar e representar esta Autarquia na lavratura e assinatura do contrato e termos aditivos, apostilamento, referente às obras de Restauração com Melhorias na Rodovia BR-158/MS.', N'PORTARIA Nº 5913', N'BA Nº 166 de 28/08/2019', N'50619.001333/2019-84', CAST(N'2019-07-24' AS Date), CAST(N'2019-08-28' AS Date), 2, 2, N'BR-158/MS - Projeto Básico/Executivo, bem como licitar, homologar, adjudicar e representar esta Autarquia na lavratura e assinatura do contrato e termos aditivos, apostilamento, referente às obras de Restauração com Melhorias.', N'Delegação de Competência Deferida', CAST(200.1 AS Decimal(23, 1)), N'Não'),
                (8, 8, N'MS', N'19 00206/2019', N'Único', N'PROJETO', N'Delegação de Competência Plena e as Responsabilidades Decorrentes ao Superintendente Regional do DNIT no Estado de Mato Grosso do Sul, para elaboração, análise e aprovação do Projeto Básico/Executivo, bem como licitar na modalidade Pregão Eletrônico, homologar, adjudicar e representar esta Autarquia na lavratura e assinatura do contrato e termos aditivos, apostilamentos, referente às obras de Restauração com Melhorias na Rodovia BR-267/MS', N'Portaria nº 5.926', N'BA nº 168 de 30/08/2019', N'50619.001369/2019-68', CAST(N'2019-07-29' AS Date), CAST(N'2019-08-30' AS Date), 2, 1, N'BR-267/MS - Projeto Básico/Executivo, bem como licitar na modalidade Pregão Eletrônico, homologar, adjudicar e representar esta Autarquia na lavratura e assinatura do contrato e termos aditivos, apostilamentos, referente às obras de Restauração com Melhorias', N'Delegação de Competência Deferida', CAST(209.6 AS Decimal(23, 1)), N'Não'),
                (9, 9, N'MG', N'A LICITAR', N'Único', N'PROJETO', N'Delegação de Competência para licitação dos serviços de \"Atualização e Complementação do Projeto Básico e elaboração do Projeto Executivo de Engenharia para Implantação e Pavimentação na Rodovia BR- 367/MG - Trecho: Divisa BA/MG (Salto da Divisa) - Entr. BR-259 (Gouveia); Subtrecho: Entr. MG-114(A)(Virgem da Lapa) - ENTR MG-114(p/ Catutiba); segmento 120,0km.', N'PORTARIA Nº 6624', N'BA Nº 192 de 03/10/2019', N'50606.003546/2019-90', CAST(N'2019-06-28' AS Date), CAST(N'2019-10-01' AS Date), 2, 1, N'Projeto Básico/Executivo de Engenharia para Implantação, Pavimentação, incluindo Melhoramentos e Adequação da Capacidade de segmento rodoviário e Implantação do Contorno Rodoviário de Minas Novas na BR-367/MG - Trecho: Divisa BA/MG (Salto da Divisa) - Entr. BR-259 (Gouveia); Subtrecho: Entr. MG-114 (A) (Virgem da Lapa) - Entr. MG-114 (p/Catutiba). Segmento: km 336,7 a km 456,7; Extensão 120 Km.', N'Delegação de Competência Deferida', CAST(120.0 AS Decimal(23, 1)), N'Sim'),
                (10, 10, N'MG', N'00 00940/2014', N'67', N'EVTEA', N'Delegação de Competência Plena para acompanhamento, análise e aprovação dos Estudos da rodovia BR-265/MG. Para a Implantação, Pavimentação, Adequação de Capacidade com Melhoria de Segurança e Eliminação de Pontos Críticos da rodovia', N'', N'', N'50600.015437/2019-75', CAST(N'2019-08-07' AS Date), NULL, 1, 1, N'Análise e aprovação do EVTEA da rodovia BR-265/MG, Segmento: km 0 - km 371,4, Extensão: 371,4 km - Lote 67', N'Em Análise pela CGPLAN', CAST(371.4 AS Decimal(23, 1)), N'Não'),
                (11, 11, N'BA', N'A LICITAR', N'Único', N'PROJETO', N'Delegação de Competêcuia para Licitar e contratar os serviços de elaboração de projeto básico e executivo e execução das obras de reabilitação de 03 pontes pontes localizadas nas rodovias BR-324/BA e 407/BA.', N'', N'', N'50600.003942/2018-96', CAST(N'2019-09-06' AS Date), NULL, 1, 0, N'', N'Em Análise pela DIREX / Colegiada', CAST(0.0 AS Decimal(23, 1)), N'Sim'),
                (12, 12, N'BA', N'A LICITAR', N'Único', N'PROJETO', N'Delegação de Competêcuia para Licitar e contratar os serviços de elaboração de projeto básico e executivo e execução das obras de reabilitação de 03 pontes pontes localizadas nas rodovias BR-324/BA e 407/BA.', N'', N'', N'50600.003942/2018-96', CAST(N'2019-09-06' AS Date), NULL, 1, 0, N'', N'Em Análise pela DIREX / Colegiada', CAST(0.0 AS Decimal(23, 1)), N'Sim'),
                (13, 13, N'PI', N'A LICITAR', N'Único', N'ANTEPROJETO', N'Delegação para análise e aprovação do Anteprojeto de Reabilitação da Ponte Juscelino Kubitschek sobre o Rio Poti localizada na rodovia BR-343/PI', N'Portaria nº 6853/2019', N'BA nº 198 de 11/10/2019', N'50600.025594/2019-99', CAST(N'2019-09-13' AS Date), CAST(N'2019-10-11' AS Date), 1, 0, N'', N'Delegação de Competência Deferida', CAST(4.9 AS Decimal(23, 1)), N'Sim'),
                (14, 14, N'RS', N'A LICITAR', N'Único', N'PROJETO', N'Delegação de Competência para contratação de empresa visando a elaboração de projeto básico e executivo de engenharia para melhoramentos físicos e de segurança de tráfego com ampliação da capacidade, incluindo duplicação e restauração do segmento entre Passo Fundo e Pontão, na BR-285/RS', N'', N'', N'50610.006345/2019-85', CAST(N'2019-09-25' AS Date), NULL, 1, 0, N'', N'Em Análise pela DIREX / Colegiada', CAST(11.4 AS Decimal(23, 1)), N'Sim'),
                (15, 15, N'MT', N'A LICITAR', N'Único', N'PROJETO', N'Delegação de Competência para Elaboração de Estudos, Projetos Básicos e Executivos de Engenharia para a Execução de Obras de Adequação de Capacidade e Melhoramentos, incluindo Duplicação, Implantação, Pavimentação e Obras de Artes Especiais, da Rodovia BR-070/MT.', N'', N'', N'50611.003862/2019-92', CAST(N'2019-11-06' AS Date), NULL, 1, 0, N'', N'Não Conforme', CAST(20.7 AS Decimal(23, 1)), N'Sim')
                ";
            $this->db->query($sql);
        }
        
    }

    public function down()
    {
        $this->dbforge->drop_table('tblDelegacaoCompetencia');
       
    }
}