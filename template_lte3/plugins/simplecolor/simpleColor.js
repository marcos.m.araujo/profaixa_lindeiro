/*
*简易颜色选取器插件
*/
(function(window, document, $, undefined) {
    $.fn.simpleColor = function(_config){
        var defaults = {
            id:"selectColors",    //id
            bgOutput:"", //颜色显示div ID
            hexOutput:"" //颜色值显示input ID
        };
        var _this = this;
        this.config = $.extend({},defaults,_config);
        var hexch = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'],
            ToHex = function(n) {
                var h, l;
                n = Math.round(n);
                l = n % 16;
                h = Math.floor((n / 16)) % 16;
                return (hexch[h] + hexch[l]);
            },
            getPosition = function(obj) {
                return {
                    x : $(obj).offset().left,
                    y : $(obj).offset().top,
                    w : $(obj).width(),
                    h : $(obj).height()
                };
            },
            setPosition = function(obj){
                var ox = getPosition($(_this)).x,
                    oy = getPosition($(_this)).y,
                    ow = getPosition($(_this)).w,
                    oh = getPosition($(_this)).h;
                $(obj).css({
                    position : "absolute",
                    top : (oy + oh + 10) +"px",
                    left : ox +"px",
                    zIndex : 9999
                });    
            },
            eventfun = function(obj,color,event){
                $(obj).css("background-color","#"+color);
                $(obj).on("mouseover",function(){
                    $(obj).css("background-color","#FFF");
                }).on("mouseout",function(){
                    $(obj).css("background-color","#"+color);
                }).on("click",function(){
                    /* 点击选择颜色时在div @bgOutput中显示选中的颜色和显示色值的input @hexOutput*/
                    $(obj).css("background-color","#000");
                    $(_this.config.bgOutput).css("background-color","#"+color);
                    $(_this.config.hexOutput).val("#"+color.toLowerCase());
                    event.stopPropagation();
                })
            },
            /* 生成色彩区域 */
            drawColor = function(r, g, b, n) {
                r = ((r * 16 + r) * 3 * (15 - n) + 0x80 * n) / 15;
                g = ((g * 16 + g) * 3 * (15 - n) + 0x80 * n) / 15;
                b = ((b * 16 + b) * 3 * (15 - n) + 0x80 * n) / 15;
                var sFinalHex = ToHex(r) + ToHex(g) + ToHex(b);
                var td = document.createElement("td");
                eventfun(td,sFinalHex);
                return td;
            },
            init = function() {
                var div = document.createElement("div");
                var table = document.createElement("table");
                div.id = _this.config.id;
                var cnum = [1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0];
                if(!document.getElementById(div.id)){
                    for (var i = 0; i < 16; i++) {
                        var tr = document.createElement("tr");
                        var tdE = "";
                        for (var j = 0; j < 30; j++) {
                            var n1 = j % 5,
                                n2 = Math.floor(j / 5) * 3,
                                n3 = n2 + 3;
                            tdE = drawColor((cnum[n3] * n1 + cnum[n2] * (5 - n1)), (cnum[n3 + 1] * n1 + cnum[n2 + 1] * (5 - n1)), (cnum[n3 + 2] * n1 + cnum[n2 + 2] * (5 - n1)), i);
                            tr.appendChild(tdE);
                        }
                        table.appendChild(tr);
                    }
                    div.appendChild(table);
                    setPosition($(div));
                    document.body.appendChild(div);
                }
            };
        this.close = function(callback){
            var id = _this.config.id;
            $("#"+id).remove();
            if(typeof callback == "function"){
                callback();
            }
        };
        init();
        return this;  
    }
    
})(window, document, jQuery);
